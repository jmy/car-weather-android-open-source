SOLIPASTE CAR WEATHER
=====================

A shallow clone of latest non-published version Solipaste Car Weather aka heathelper. Now with
new Material Design look.

![Screenshot](docs/screenshot.png)

License
=======

The code in this project is licensed under the Apache Software License 2.0, per the terms of the included LICENSE file.