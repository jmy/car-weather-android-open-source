/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import android.test.InstrumentationTestCase;
import android.util.SparseArray;

import com.solipaste.carweather.domain.Location.Country;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

public class LocationsTest extends InstrumentationTestCase {

    private static final int INDEX_LAST = 89;
    Reader r;
    Locations l;
    private SparseArray<String> countryNames;

    @Override
    public void setUp() throws Exception {
        r = new InputStreamReader(getInstrumentation().getContext().getAssets()
                .open("test_locations.csv"), "UTF-8");
        l = new Locations();

        countryNames = new SparseArray<String>();
        countryNames.put(Country.FI.getCode(), "Finland");
        countryNames.put(Country.SE.getCode(), "Sweden");
        countryNames.put(Country.NO.getCode(), "Norway");

    }

    @SuppressWarnings("deprecation")
    public void testLocations_Constructor_EmptyLocs() throws Exception {
        assertTrue(l != null);
        assertTrue(l.getLocations() != null);
        assertEquals(0, l.getLocations().size());
    }

    @SuppressWarnings("deprecation")
    public void testInitialize() throws Exception {
        assertEquals(0, l.getLocations().size());
        l.Initialize(r, false);
        assertEquals(90, l.getLocations().size());
    }

    public void testInitialize_Error() throws Exception {
        try {
            l.Initialize(null, false);
            fail("Should have thrown");
        } catch (Exception e) {
            // OK
        }
    }

    public void testGetLocations_First_Alaharma() throws Exception {
        l.Initialize(r, false); // 661601,Alahärmä,PPLA3,FI,15,14,233
        Location loc = l.getLocation("alahärmä");
        assertTrue(loc != null);
        assertEquals("PPLA3", loc.getClassCode());
        assertEquals(Location.Country.FI, loc.getCountry());
        assertEquals(15, loc.getAdmin1());
        assertEquals(14, loc.getAdmin2());
        assertEquals(233, loc.getAdmin3());
    }

    public void testGetLocations_Last_Aanekoski() throws Exception {
        l.Initialize(r, false); // 662095,Äänekoski,PPLA3,FI,15,13,992
        Location loc = l.getLocation("Äänekoski");
        assertTrue(loc != null);
        assertEquals("PPLA3", loc.getClassCode());
        assertEquals(Location.Country.FI, loc.getCountry());
        assertEquals(15, loc.getAdmin1());
        assertEquals(13, loc.getAdmin2());
        assertEquals(992, loc.getAdmin3());
    }

    public void testGetRegionName_FI_Lappland() throws Exception {
        Location loc = new Location("dummy", "dummy", "FI", "6", "666",
                "666");
        assertEquals("Lappland", loc.getRegionName());
    }

    public void testCountryName_Invalid_Null() throws Exception {
        Location loc = new Location("dummy", "dummy", "dummy", "123", "666",
                "666");
        assertNull(loc.getCountryName());
    }

    public void testGetCountry_FI() throws Exception {
        Location loc = new Location("dummy", "dummy", "FI", "6", "666",
                "666");
        assertEquals(Location.Country.FI, loc.getCountry());
    }
    
    public void testGetCountry_SE() throws Exception {
        Location loc = new Location("dummy", "dummy", "SE", "6", "666",
                "666");
        assertEquals(Location.Country.SE, loc.getCountry());
    }
    
    public void testGetCountry_NO() throws Exception {
        Location loc = new Location("dummy", "dummy", "NO", "6", "666",
                "666");
        assertEquals(Location.Country.NO, loc.getCountry());
    }
    
    public void testGetCountryName_FI() throws Exception {
        Location loc = new Location("dummy", "dummy", "FI", "6", "666",
                "666");
        assertEquals("Finland", loc.getCountryName());
    }
    
    public void testGetCountryName_SE() throws Exception {
        Location loc = new Location("dummy", "dummy", "SE", "6", "666",
                "666");
        assertEquals("Sweden", loc.getCountryName());
    }
    
    public void testGetCountryName_NO() throws Exception {
        Location loc = new Location("dummy", "dummy", "NO", "6", "666",
                "666");
        assertEquals("Norway", loc.getCountryName());
    }
    
    public void testGetRegionName_Invalid_Null() throws Exception {
        Location loc = new Location("dummy", "dummy", "dummy", "123", "666",
                "666");
        assertNull(loc.getRegionName());
        assertNull(loc.getRegion2Name());
    }

    public void testGetAllNames() throws Exception {
        l.Initialize(r, false);
        List<String> names = l.getNameList();
        assertEquals("Alahärmä", names.get(0));
        assertEquals("Äänekoski", names.get(INDEX_LAST));
    }

    public void testGetLocationList() throws Exception {
        l.Initialize(r, false);
        List<Location> names = l.getLocationList();
        assertEquals("Alahärmä", names.get(0).getName());
        assertEquals("Äänekoski", names.get(INDEX_LAST).getName());
    }
    
    public void testGetRegionName_FI() throws Exception {
        // 661352,Alavus,PPLA3,FI,15,14,010
        Location loc = new Location("Alavus", "PPL3", "FI", "15", "14", "010");
        assertEquals("Western Finland", loc.getRegionName());
        assertNull(loc.getRegion2Name());
    }
    
    
    public void testGetRegionName_SE() throws Exception {
        // 2726026,Anderstorp,PPL,SE,28,1447
        Location loc = new Location("Anderstorp", "PPL", "SE", "28", "1447");
        assertEquals("Västra Götaland", loc.getRegionName());
        assertNull(loc.getRegion2Name());
    }
    
    public void testGetRegionName_NO() throws Exception {
        // 3161456,Bjørkvang,PPL,NO,09,1805
        Location loc = new Location("Bjørkvang", "PPL", "NO", "09", "1805");
        assertEquals("Nordland", loc.getRegionName());
        assertEquals("Narvik", loc.getRegion2Name());
    }
    
    public void testGetLongName_FI() throws Exception {
        // 661352,Alavus,PPLA3,FI,15,14,010
        Location loc = new Location("Alavus", "PPL3", "FI", "15", "14", "010");
        assertEquals("Finland/Western_Finland/Alavus", loc.getLongName());
    }

    public void testGetLongName_SE() throws Exception {
        // 2726026,Anderstorp,PPL,SE,28,1447
        Location loc = new Location("Anderstorp", "PPL", "SE", "28", "1447");
        assertEquals("Sweden/Västra_Götaland/Anderstorp", loc.getLongName());
    }

    public void testGetLongName_NO() throws Exception {
        // 3161456,Bjørkvang,PPL,NO,09,1805
        Location loc = new Location("Bjørkvang", "PPL", "NO", "09", "1805");
        assertEquals("Norway/Nordland/Narvik/Bjørkvang", loc.getLongName());
    }
    
    public void testGetLongRegionUIName_FI() throws Exception {
        // 661352,Alavus,PPLA3,FI,15,14,010
        Location loc = new Location("Alavus", "PPL3", "FI", "15", "14", "010");
        assertEquals("Finland", loc.getLongRegionUIName(countryNames));
    }

    public void testGetLongRegionUIName_SE() throws Exception {
        // 2726026,Anderstorp,PPL,SE,28,1447
        Location loc = new Location("Anderstorp", "PPL", "SE", "28", "1447");
        assertEquals("Sweden, Västra Götaland", loc.getLongRegionUIName(countryNames));
    }

    public void testGetLongRegionUIName_NO() throws Exception {
        // 3161456,Bjørkvang,PPL,NO,09,1805
        Location loc = new Location("Bjørkvang", "PPL", "NO", "09", "1805");
        assertEquals("Norway, Nordland, Narvik", loc.getLongRegionUIName(countryNames));
    }

    public void testGetLongRegionUIName_NO_DuplicateRegions() throws Exception {
        // 3143244,Oslo,PPLC,NO,12,0301
        Location loc = new Location("Oslo", "PPLC", "NO", "12", "0301");
        assertEquals("Norway", loc.getLongRegionUIName(countryNames));
    }
    
}
