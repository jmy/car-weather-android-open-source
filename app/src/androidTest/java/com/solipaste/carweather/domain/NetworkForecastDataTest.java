/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import android.test.InstrumentationTestCase;

public class NetworkForecastDataTest extends InstrumentationTestCase {

    private static final String FINLAND_WESTERN_FINLAND_TAMPERE = "Finland/Western_Finland/Tampere";

    final private String TAG = "NetworkWeatherTest";

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        // manually refresh data using mock reader
        InputStreamReader inputStreamReader = new InputStreamReader(
                new BufferedInputStream(getInstrumentation().getContext()
                        .getAssets().open("forecast_hour_by_hour.xml")),
                "UTF-8");
    }

    public void testParseXML_InvalidTemperatureValue_Throws() throws Exception {
            Reader reader = new InputStreamReader(new BufferedInputStream(
                    getInstrumentation().getContext().getAssets()
                            .open("invalidtemperaturevalue.xml")), "UTF-8");
            assertNotNull(reader);
            NetworkForecastData temps = NetworkForecastData
                    .parseXML(reader);
            assertNotNull(temps);
            assertNull(temps.getTemperatureMap());
        }

    public void testParseXML_InvalidXml_Throws() throws Exception {
            Reader reader = new InputStreamReader(new BufferedInputStream(
                    getInstrumentation().getContext().getAssets()
                            .open("invalidxml.xml")), "UTF-8");
            assertNotNull(reader);
            NetworkForecastData temps = NetworkForecastData
                    .parseXML(reader);
            assertNotNull(temps);
            assertNull(temps.getTemperatureMap());
        }

    public void testParseXML_InvalidDate_Throws() throws Exception {
            Reader reader = new InputStreamReader(new BufferedInputStream(
                    getInstrumentation().getContext().getAssets()
                            .open("invaliddate.xml")), "UTF-8");
            assertNotNull(reader);
            NetworkForecastData temps = NetworkForecastData
                    .parseXML(reader);
            assertNotNull(temps);
            assertNull(temps.getTemperatureMap());
        }

}
