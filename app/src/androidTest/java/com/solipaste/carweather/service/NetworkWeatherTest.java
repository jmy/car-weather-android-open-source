/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.service;

import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.util.Calendar;

import android.test.InstrumentationTestCase;

import com.solipaste.carweather.domain.NetworkForecastData;
import com.solipaste.carweather.domain.Temperature;

public class NetworkWeatherTest extends InstrumentationTestCase {

    /**
     * 
     */
    private static final String FINLAND_WESTERN_FINLAND_TAMPERE = "Finland/Western_Finland/Tampere";

    final private String TAG = "NetworkWeatherTest";

    private NetworkWeather weather;

    public NetworkWeatherTest() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.test.AndroidTestCase#setUp()
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        weather = new NetworkWeather(null, FINLAND_WESTERN_FINLAND_TAMPERE, false, false);
        InputStreamReader inputStreamReader = new InputStreamReader(
                                                                    new BufferedInputStream(getInstrumentation().getContext()
                                                                            .getAssets().open("forecast_hour_by_hour.xml")),
                                                                    "UTF-8");
        NetworkForecastData testData = NetworkForecastData.parseXML(inputStreamReader);
        testData.resetTimeStamp();
        weather.setTestData(testData);
    }

    public void testGetLocation()
     throws Exception {
        assertTrue(weather.getLongLocation().equals(FINLAND_WESTERN_FINLAND_TAMPERE));
    }

    public void testGetTemperature_TestDataBeforePeriod_14C() throws Exception {
        Calendar earlyCal = Calendar.getInstance();
        earlyCal.set(2012, 6, 23, 12, 55);
        Temperature t = weather.getTemperature(earlyCal.getTime());
        assertTrue(new Temperature("14.0").equals(t));
    }

    public void testGetTemperature_TestData1stPeriod_14C() throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.set(2012, 6, 23, 13, 59);
        Temperature t = weather.getTemperature(cal.getTime());
        assertTrue(new Temperature("14.0").equals(t));
    }

    public void testGetTemperature_TestData2ndPerios_7C() throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.set(2012, 6, 23, 14, 10);
        Temperature t = weather.getTemperature(cal.getTime());
        assertTrue(new Temperature("7.0").equals(t));
    }

    public void testGetTemperature_TestData3ndPerios_5C() throws Exception {
        Calendar cal = Calendar.getInstance();
        cal.set(2012, 6, 23, 15, 00);
        Temperature t = weather.getTemperature(cal.getTime());
        assertTrue(new Temperature("5.0").equals(t));
    }


}
