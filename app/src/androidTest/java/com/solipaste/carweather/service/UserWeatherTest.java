/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.service;

import java.util.Date;

import android.test.AndroidTestCase;

import com.solipaste.carweather.domain.Temperature;

public class UserWeatherTest extends AndroidTestCase {

    /**
     * @throws java.lang.Exception
     */
    @Override
    public void setUp() throws Exception {
    }

    /**
     * Test method for
     * {@link com.solipaste.carweather.service.domaindomain.UserWeather#UserWeather(float)}
     * .
     */
    public void testUserWeather() {
        final Temperature t = new Temperature(0);
        UserWeather w = new UserWeather(t);
        assertNotNull(w);
    }

    /**
     * Test method for
     * {com.solipaste.carweather.domainather.domain.UserWeather#getTemperature
     * (java.util.Date)}.
     */
    public void testGetTemperatureDate_0F_OK() {
        final Temperature t = new Temperature(0);
        UserWeather w = new UserWeather(t);
        assertTrue(t.equals(w.getTemperature(new Date())));
    }

    /**
     * Test methodcom.solipaste.carweather.domain.carweather.domain.UserWeather#
     * getTemperature()}.
     */
    public void testGetTemperature() {
        final Temperature t = new Temperature(0);
        UserWeather w = new UserWeather(t);
        assertTrue(t.equals(w.getTemperature()));
    }

    /**
     * Test com.solipaste.carweather.domainipaste.carweather.domain.UserWeather#
     * refreshWeatherData()}.
     */
    public void testRefreshWeatherData() {
        final Temperature t = new Temperature(10);
        UserWeather w = new UserWeather(t);
        assertTrue(t.equals(w.getTemperature()));
        w.refreshWeatherData(null);
        assertTrue(t.equals(w.getTemperature()));

    }

    public void getLongLocation() throws Exception {
        final Temperature t = new Temperature(10);
        UserWeather w = new UserWeather(t);
        assertTrue("test".equals(w.getLongLocation()));

    }

}
