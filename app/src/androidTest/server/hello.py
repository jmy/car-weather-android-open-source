import cherrypy

import time

cherrypy.server.socket_host = '192.168.255.11'

class HelloWorld(object):
    def index(self):
        return "Hello World!"
    index.exposed = True

class Timeout(object):
    def index(self):
   		time.sleep(20)
		return str(cherrypy.server.httpserver.timeout)
    index.exposed = True

cherrypy.quickstart(Timeout())