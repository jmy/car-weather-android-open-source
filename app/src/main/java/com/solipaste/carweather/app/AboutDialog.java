/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.format.DateFormat;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

public class AboutDialog extends DialogFragment {
    private final static String TAG = AboutDialog.class.getName();

    public interface AboutDialogListener {

        public void onAboutDialogClose();

        public void onAboutDialogCloseWithShowHelp();

    }

    public static final String FRAGMENT_TAG_ABOUTDIALOG = "com.solipaste.carweather.aboutdialog";

    private static final String BUILD_DATE_STRING = "yyyy-MM-dd";
    private static final String CHARSET = "UTF-8";
    private static final String ASSET_PATH_FORMAT = "%s/%s";
    private static final String ASSET_PATH_FORMAT_WITH_LANG = "%s-%s/%s";
    private static final String ASSET_FOLDER = "html";

    public static final String ASSET_FILE_ABOUT = "about.html";

    private String mText;
    private final String mContentFile = ASSET_FILE_ABOUT;

    private AboutDialogListener mCallback;

    public AboutDialog() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate");

        super.onCreate(savedInstanceState);

        StringBuilder text = new StringBuilder();

        try {
            String langCode = Locale.getDefault().getLanguage();

            String path = String.format(ASSET_PATH_FORMAT_WITH_LANG,
                                        ASSET_FOLDER, langCode, mContentFile);
            InputStream is = null;
            try {
                is = getActivity().getAssets().open(path);
            } catch (IOException e) {
                path = String.format(ASSET_PATH_FORMAT, ASSET_FOLDER,
                                     mContentFile);
                is = getActivity().getAssets().open(path);
            }
            InputStreamReader reader = new InputStreamReader(is, CHARSET);
            BufferedReader br = new BufferedReader(reader);
            char[] cbuf = new char[1024];
            int count = 0;
            while (count != -1) {
                count = br.read(cbuf);
                if (count > 0)
                    text.append(cbuf, 0, count);
            }

            // Append detailed version data
            text.append(getString(R.string.about_dialog_header_application_details,
                                  getVersionName(), getBuildDateString()));

        } catch (IOException e) {
            Log.e(TAG, "Reading text for dialog failed");
            e.printStackTrace();
        }

        mText = text.toString();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.v(TAG, "onCreateDialog");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Dialog has no parent, hence null
        @SuppressLint("InflateParams") final View view = inflater.inflate(R.layout.dialog_about, null);
        TextView textView = (TextView) view.findViewById(R.id.about_text);
        ScrollView scrollView = (ScrollView) view
                .findViewById(R.id.about_dialog_scrollview);
        ImageView imageView = (ImageView) view.findViewById(R.id.about_image);

        textView.setMovementMethod(LinkMovementMethod.getInstance());
        scrollView.setScrollbarFadingEnabled(false);

        imageView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Go to Google play when image is clicked

                FragmentActivity a = getActivity();
                if (a != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(a
                            .getString(R.string.market_google_play)));
                    startActivity(i);
                }
            }
        });

        // Set text to dialog
        if (mText != null)
            textView.setText(Html.fromHtml(mText));

        // Use the Builder class for convenient dialog construction
        builder.setView(view)
                .setTitle(getString(R.string.about_dialog_title_with_version_format,
                                    getVersionName()))
                .setNeutralButton(R.string.dialog_close_button_title,
                                  new DialogInterface.OnClickListener() {
                                      @Override
                                      public void onClick(
                                              DialogInterface dialog, int which) {
                                          if (mCallback != null)
                                              mCallback.onAboutDialogClose();
                                          dismiss();
                                      }
                                  })
                .setPositiveButton(R.string.about_dialog_help_button,
                                   new DialogInterface.OnClickListener() {

                                       @Override
                                       public void onClick(
                                               DialogInterface dialog, int which) {
                                           if (mCallback != null)
                                               mCallback
                                                       .onAboutDialogCloseWithShowHelp();
                                           dismiss();
                                       }
                                   });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        try {
            mCallback = (AboutDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement AboutDialogListener");
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        FragmentActivity activity = getActivity();
        if (activity != null)
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

    }

    public int getVersion() {
        int v = 0;
        try {
            v = getActivity().getPackageManager()
                    .getPackageInfo(getActivity().getPackageName(), 0).versionCode;
        } catch (NameNotFoundException ignored) {
        }
        return v;
    }

    public String getVersionName() {
        String v = "";
        try {
            v = getActivity().getPackageManager()
                    .getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (NameNotFoundException ignored) {
        }
        return v;
    }

    public String getBuildDateString() {
        CharSequence date = "";
        ZipFile zf = null;
        try {
            ApplicationInfo ai = getActivity().getPackageManager()
                    .getApplicationInfo(getActivity().getPackageName(), 0);
            zf = new ZipFile(ai.sourceDir);
            ZipEntry ze = zf.getEntry("classes.dex");
            if (ze != null) {
                long time = ze.getTime();
                date = DateFormat.format(BUILD_DATE_STRING, new java.util.Date(
                        time));
            }
            zf.close();

        } catch (Exception e) {
            try {
                if (zf != null)
                    zf.close();
            } catch (IOException e1) {
                // ignore
            }
        }
        return date.toString();
    }
}
