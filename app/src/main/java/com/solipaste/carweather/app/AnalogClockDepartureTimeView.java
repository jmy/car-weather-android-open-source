/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Matrix.ScaleToFit;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.solipaste.carweather.domain.SimpleTime;
import com.solipaste.carweather.domain.TimerData;

public class AnalogClockDepartureTimeView extends View implements DepartureTimeView, ClockView {
    private final static String TAG = AnalogClockDepartureTimeView.class.getName();

    private static final int PAINT_COLOR_CIRCLE = Color.argb(0xff, 0x73, 0x76,
                                                             0x73);
    private static final int PAINT_COLOR_HOUR_HAND = 0xFF2A2A50;
    private static final int TEXT_COLOR_HOUR = Color.rgb(0x33, 0x33, 0x33);
    private static final int SHADOW_COLOR = 0x7f000000;
    private static final int SHADOW_COLOR_RIM = 0x3f000000;

    private static final float USER_HAND_SHADOW_SHIFT_Y = 0.01f;
    private static final float CENTER_SCALED = 0.5f;
    private static final float CLOCK_OUTER_MARGIN = 0.02f;
    private static final float RIM_WIDTH = 0.15f;

    private static final int USER_HAND_ANIMATION_DURATION_MS = 300;
    private static final float USER_HAND_MIN_ANIMATED_HOUR_DELTA = 1f;
    private static final int USER_HAND_TOUCH_ANGLE_MAX_THRESHOLD = 20;
    private static final double USER_HAND_TOUCH_CENTER_DISTANCE_MIN_THRESHOLD = 0.3;
    private static final double USER_HAND_TOUCH_CENTER_DISTANCE_MAX_THRESHOLD = 1.0;
    private static final int ROUND_TOUCH_MINUTE = 5;

    public enum UserHandType {
        START_TIME, STOP_TIME
    }

    private final Rect mCoordinateRect = new Rect(0, 0, 1, 1);

    private final static int UNITS = 24;
    private float mCenterRawX;
    private float mCenterRawY;

    private final Paint mHourHandPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint mHeatingArcPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint mIdleHeatingArcPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint mHourPointPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint mHourTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint mRimCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint mRimCircleBackgroundPaint = new Paint(
            Paint.ANTI_ALIAS_FLAG);
    private final Paint mRimShadowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint mUserHandPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private RectF mRimRect;
    private RectF mRimMiddleRect;
    private RectF mFaceRect;

    private RectF mUserHandRect;
    private RectF mUserHandRectTarget;
    private Path mHandPath;
    private float mHOffset;
    private float mWOffset;
    private float mScale;

    private Bitmap mUserHandleBitmapUnselected;
    private Bitmap mUserHandleBitmapSelected;
    private Bitmap mUserHandleBitmapShadow;

    private OnActionListener onActionListener;
    private TimerData mUserTimeData;

    private boolean mIsUserHandSelected = false;
    private float mUserHandCurrentDeg;

    private boolean mIsLiveUpdate = true;

    private final Handler mHandler = new Handler();
    private boolean mIsAnimating;
    private float mAnimatingUserHandHourF;

    private final boolean mIsAnimationEnabled = true;

    private Matrix mUserHandMatrixTemplate;
    private Matrix mRotatedUserHandMatrix;
    private Matrix mRotatedUserHandShadowMatrix;
    private Canvas mClockFaceCanvas;
    private Bitmap mClockFaceBitmap;

    public AnalogClockDepartureTimeView(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
    }

    public AnalogClockDepartureTimeView(Context context, int centerX, int centerY) {
        super(context);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        init();
    }

    @Override
    public void setOnActionListener(OnActionListener l) {
        onActionListener = l;
    }

    /**
     * Initialize the object data
     * 
     */
    private void init() {

        // Init timer data
        mUserTimeData = new TimerData();

        // Init measurements
        // Scaled sizes: 0...1

        // User handle bitmaps
        Options opts = new Options();
        opts.inPreferredConfig = Config.ARGB_8888;
        opts.inDither = false;
        mUserHandleBitmapUnselected = BitmapFactory
                .decodeResource(getResources(),
                                R.drawable.user_handle_unselected, opts);
        mUserHandleBitmapSelected = BitmapFactory
                .decodeResource(getResources(),
                                R.drawable.user_handle_selected, opts);
        mUserHandleBitmapShadow = BitmapFactory
                .decodeResource(getResources(), R.drawable.user_handle_shadow,
                                opts);

        // Using this paint ensures AA
        mUserHandPaint.setAntiAlias(true);
        mUserHandPaint.setFilterBitmap(true);
        mUserHandPaint.setDither(true);

        // Source rect wide enough so that apect ratio stays intact
        mUserHandRect = new RectF(0, 0,
                mUserHandleBitmapUnselected.getWidth() * 2,
                mUserHandleBitmapUnselected.getHeight());

        final float userHandRectSize = 0.4f;
        // target rect
        mUserHandRectTarget = new RectF(0, 0, userHandRectSize,
                userHandRectSize);

        // Create matrices for user hand and its shadow
        mUserHandMatrixTemplate = new Matrix();
        mRotatedUserHandMatrix = new Matrix();
        mRotatedUserHandShadowMatrix = new Matrix();
        mUserHandMatrixTemplate.setRectToRect(mUserHandRect,
                                              mUserHandRectTarget,
                                              ScaleToFit.START);
        // Move center to the center of handle
        mUserHandMatrixTemplate.preTranslate(-mUserHandleBitmapUnselected
                .getWidth() / 2, 0);

        // Adjust top of handle to the inside of the rim
        mUserHandMatrixTemplate.postTranslate(CENTER_SCALED, RIM_WIDTH - 0.01f);

        mRimRect = new RectF(0 + CLOCK_OUTER_MARGIN, 0 + CLOCK_OUTER_MARGIN,
                1 - CLOCK_OUTER_MARGIN, 1 - CLOCK_OUTER_MARGIN);

        mRimMiddleRect = new RectF(mRimRect.left + RIM_WIDTH / 2, mRimRect.top
                + RIM_WIDTH / 2, mRimRect.right - RIM_WIDTH / 2,
                mRimRect.bottom - RIM_WIDTH / 2);

        mFaceRect = new RectF(mRimRect.left + RIM_WIDTH, mRimRect.top
                + RIM_WIDTH, mRimRect.right - RIM_WIDTH, mRimRect.bottom
                - RIM_WIDTH);

        // rim background paint
        mRimCircleBackgroundPaint.setStyle(Paint.Style.STROKE);
        mRimCircleBackgroundPaint.setColor(Color.WHITE);
        mRimCircleBackgroundPaint.setStrokeWidth(RIM_WIDTH);

        // rim circles
        mRimCirclePaint.setStyle(Paint.Style.STROKE);
        mRimCirclePaint.setColor(PAINT_COLOR_CIRCLE);
        mRimCirclePaint.setStrokeWidth(0.005f);

        // rim shadow
        mRimShadowPaint.setShadowLayer(0.01f, 0f, 0.01f, SHADOW_COLOR_RIM);
        mRimShadowPaint.setStyle(Paint.Style.STROKE);
        mRimShadowPaint.setColor(Color.TRANSPARENT);
        mRimShadowPaint.setStrokeWidth(RIM_WIDTH);

        // hour point
        mHourPointPaint.setStyle(Paint.Style.STROKE);
        mHourPointPaint.setColor(Color.LTGRAY);
        mHourPointPaint.setStrokeWidth(0.005f);

        // hour point text
        mHourTextPaint.setColor(TEXT_COLOR_HOUR);
        mHourTextPaint.setTextSize(0.06f);
        mHourTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
        mHourTextPaint.setTextAlign(Paint.Align.CENTER);

        // hour triangle
        mHourHandPaint.setColor(PAINT_COLOR_HOUR_HAND);
        mHourHandPaint.setStrokeWidth(0.02f);
        mHourHandPaint.setStyle(Style.FILL);
        mHourHandPaint.setShadowLayer(0.01f, 0f, 0f, SHADOW_COLOR);

        mHandPath = new Path();
        float bottom = mFaceRect.top + RIM_WIDTH + 0.005f;
        mHandPath.moveTo(CENTER_SCALED - 0.05f, bottom);
        mHandPath.lineTo(CENTER_SCALED, bottom - 0.1f);
        mHandPath.lineTo(CENTER_SCALED + 0.05f, bottom);
        mHandPath.close();

        // heat On arc
        mHeatingArcPaint.setStyle(Paint.Style.STROKE);
        mHeatingArcPaint.setStrokeWidth(0.03f);
        mHeatingArcPaint.setStrokeCap(Paint.Cap.BUTT);
        mHeatingArcPaint.setColor(getResources()
                .getColor(R.color.heat_start_color));
        mHeatingArcPaint.setShadowLayer(0.01f, 0f, 0.005f, SHADOW_COLOR);

        // idle heat arc
        mIdleHeatingArcPaint.setStyle(Paint.Style.STROKE);
        mIdleHeatingArcPaint.setStrokeWidth(0.03f);
        mIdleHeatingArcPaint.setStrokeCap(Paint.Cap.BUTT);
        mIdleHeatingArcPaint.setColor(getResources()
                .getColor(R.color.idle_heat));
        mIdleHeatingArcPaint.setShadowLayer(0.01f, 0f, 0.005f, SHADOW_COLOR);

    }

    @Override
    public int getDurationMinutes() {
        return mUserTimeData.getDuration();
    }

    @Override
    public void setDurationMinutes(int d) {
        if (mUserTimeData.setDuration(d))
            postInvalidate();
    }

    @Override
    public void setUserTimeData(TimerData data) {

        boolean settingValidDataForTheFirstTime = !mUserTimeData.isValid();

        float newUserHandHourF = getUserHandHourF(data);
        float oldUserHandHourF = getUserHandHourF(mUserTimeData);
        Log.v(TAG, "setUserTimeData, oldTime: " + oldUserHandHourF
                + ", newTime: " + newUserHandHourF);

        // take a copy of the timer data to allow avoid time updates during or
        // before animations
        mUserTimeData = new TimerData(data);

        // Decide whether to animate or not
        if (!mIsAnimationEnabled // disabled
                || settingValidDataForTheFirstTime // first time to set data
                || Math.abs(newUserHandHourF - oldUserHandHourF) < USER_HAND_MIN_ANIMATED_HOUR_DELTA) {
            // No animation
            postInvalidate();
        } else {
            animateUserHand(oldUserHandHourF, newUserHandHourF,
                            USER_HAND_ANIMATION_DURATION_MS);
        }
    }

    public boolean isLiveUpdate() {
        return mIsLiveUpdate;
    }

    public void setLiveUpdate(boolean isLiveUpdate) {
        mIsLiveUpdate = isLiveUpdate;
    }

    @Override
    public void updateWallTime() {
        // Log.v(TAG, "updateWallTime");
        // Use post so that it can be called from non-UI threads also
        postInvalidate();
    }

    /**
     * set current user hand time. Adjust the other one correctly
     * 
     * @param time
     * @param type
     */
    @Override
    public void setUserHandTime(SimpleTime time, UserHandType type) {
        switch (type) {
        case STOP_TIME:
            mUserTimeData.setReadyTime(time);
            postInvalidate();
            break;
        case START_TIME:
            mUserTimeData.setStartTime(time);
            postInvalidate();
            break;
        default:
            throw new IllegalArgumentException("invalid user hand type");
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int w = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        int h = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);

        // Force clock to circle shape
        w = Math.min(w, h);
        h = Math.min(w, h);

        setMeasuredDimension(w, h);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        Log.v(TAG, "onSizeChanged: " + w + ", " + h);

        // This offset hack is not really needed after onMeasure fix

        // Ensure that clock appears on the center of canvas and is scaled to
        // fit there
        if (h > w) {
            // portrait
            mScale = w;
            mHOffset = (h - mScale) / 2;
        } else {
            // landscape
            mScale = h;
            mWOffset = (w - mScale) / 2;
        }

        // calculate raw center point
        mCenterRawX = w / 2;
        mCenterRawY = h / 2;

        // Draw clock on the backup canvas when size changes
        // use original sizes to avoid scaling issues
        mClockFaceBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mClockFaceCanvas = new Canvas(mClockFaceBitmap);
        mClockFaceCanvas.translate(mWOffset, mHOffset); // move to center
        mClockFaceCanvas.scale(w, h); // scale from 0 ... 1
        drawClockFace(mClockFaceCanvas);

    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
//        Log.v(TAG, "onDraw start");
        
//        if (isInEditMode()) {
//            // Rects with floats don't work in Eclipse
//            mRimRect = new RectF(0, 0, 1f, 1f);
//            mFaceRect = new RectF(0, 0, 1f, 1f);
//        }

        // Set up matrix
        canvas.save(Canvas.MATRIX_SAVE_FLAG);
        canvas.translate(mWOffset, mHOffset); // move to center
        canvas.scale(mScale, mScale); // scale from 0 ... 1

        // Get current position as a floating point value for the user handle
        float userHandHourF = 0;

        if (!mIsAnimating) {
            // Animation not ongoing so therefore draw handle according to timer
            // data
            userHandHourF = getUserHandHourF(mUserTimeData);
        } else {
            // We are animating user handle movement so therefore
            // do not draw handle to "target" position
            // but instead draw it to the current animated location
            userHandHourF = mAnimatingUserHandHourF;
        }

        // Draw arcs under everything else
        drawHeatingArcs(canvas, userHandHourF);

        // Draw clock face from backup canvas
        if (mClockFaceBitmap != null)
            canvas.drawBitmap(mClockFaceBitmap, null, mCoordinateRect, null);

        // Draw hour hand on top of clock
        drawHourHand(canvas);

        if (mUserTimeData.isValid())
            // Draw user handle on top
            drawUserHand(canvas, userHandHourF);
        else
            Log.e(TAG, "time data invalid, cannot draw user hand");

        canvas.restore();
//        Log.v(TAG, "onDraw start");

    }

    /**
     * @param canvas
     */
    private void drawClockFace(Canvas canvas) {

        // draw the rim shadow inside the face and under the rim
        canvas.drawOval(mRimMiddleRect, mRimShadowPaint);

        // draw rim background circle
        canvas.drawOval(mRimMiddleRect, mRimCircleBackgroundPaint);

        // draw outer rim circle
//        canvas.drawOval(mRimRect, mRimCirclePaint);

        // draw the inner rim circle
//        canvas.drawOval(mFaceRect, mRimCirclePaint);

        drawHourPoints(canvas);
    }

    private void drawHourPoints(Canvas canvas) {

        final float hourMarkHeight = 0.03f;
        final float hourTextFromLineDistance = 0.03f;
        final float hourTextXoffset = 0.003f;
        final float y1 = mFaceRect.top;
        final float y2 = y1 - hourMarkHeight;
        final float halfHourInDegrees = 7.5f;
        final float centerX = CENTER_SCALED;
        final float centerY = CENTER_SCALED;

        canvas.save(Canvas.MATRIX_SAVE_FLAG);
        for (int i = 0; i < 48; ++i) {

            canvas.drawLine(centerX, y1, centerX, y2, mHourPointPaint);

            if (i % 2 == 0) {
                String valueString = Integer.toString(i / 2);
                if (true) {
                    float originalTextSize = mHourTextPaint.getTextSize();

                    // https://code.google.com/p/android/issues/detail?id=39755#c10
                    // set a magnification factor
                    final float magnifier = 100f;

                    // Scale the canvas
                    canvas.save();
                    canvas.scale(1f / magnifier, 1f / magnifier);

                    // increase the font size
                    mHourTextPaint.setTextSize(originalTextSize * magnifier);

                    canvas.drawText(valueString, (centerX-hourTextXoffset)*magnifier, (y2
                                    - hourTextFromLineDistance)*magnifier, mHourTextPaint);

                    // bring everything back to normal
                    canvas.restore();
                    mHourTextPaint.setTextSize(originalTextSize);
                    
//                    canvas.drawText(valueString, centerX, y2
//                                    - hourTextFromLineDistance, mHourTextPaint);
                }
            }

            canvas.rotate(halfHourInDegrees, centerX, centerY);
        }
        canvas.restore();
    }

    /**
     * @param canvas
     */
    private void drawHourHand(Canvas canvas) {
        final Calendar cal = Calendar.getInstance();
        final int hours = cal.get(Calendar.HOUR_OF_DAY);
        final int mins = cal.get(Calendar.MINUTE);
        final float hourF = hours + mins / 60f;

        canvas.save(Canvas.MATRIX_SAVE_FLAG);
        float deg = hourF / UNITS * 360;
        canvas.rotate(deg, CENTER_SCALED, CENTER_SCALED); // Will rotate shadow
                                                          // in paint also!
        canvas.drawPath(mHandPath, mHourHandPaint);

        canvas.restore();
    }

    private void drawUserHand(Canvas canvas, float userHandHourF) {

        // store current hand angle for reference to used for touch gesture
        // checking
        mUserHandCurrentDeg = userHandHourF / UNITS * 360;

        // initialize matrices from the template
        mRotatedUserHandMatrix.set(mUserHandMatrixTemplate);
        mRotatedUserHandShadowMatrix.set(mUserHandMatrixTemplate);

        // Translate shadow lower
        mRotatedUserHandShadowMatrix.postTranslate(0, USER_HAND_SHADOW_SHIFT_Y);

        // Rotate both matrices by the current deg
        mRotatedUserHandShadowMatrix
                .postRotate(mUserHandCurrentDeg, CENTER_SCALED, CENTER_SCALED
                        + USER_HAND_SHADOW_SHIFT_Y);
        mRotatedUserHandMatrix.postRotate(mUserHandCurrentDeg, CENTER_SCALED,
                                          CENTER_SCALED);

        Bitmap userHand = null;
        if (mIsUserHandSelected)
            userHand = mUserHandleBitmapSelected;
        else
            userHand = mUserHandleBitmapUnselected;

        // Draw shadown first
        canvas.drawBitmap(mUserHandleBitmapShadow,
                          mRotatedUserHandShadowMatrix, mUserHandPaint);
        canvas.drawBitmap(userHand, mRotatedUserHandMatrix, mUserHandPaint);

    }

    private void animateUserHand(final float oldUserHandHourF,
            float newUserHandHourF, final long durationMs) {

        if (oldUserHandHourF == newUserHandHourF)
            return;

        // Ensure that there are not normal time updates after this
        mAnimatingUserHandHourF = oldUserHandHourF;
        mIsAnimating = true;

        final AccelerateDecelerateInterpolator interpolator = new AccelerateDecelerateInterpolator();
        final long startTime = System.currentTimeMillis();

        final float delta = newUserHandHourF - oldUserHandHourF;

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                long now = System.currentTimeMillis();
                float currentMs = Math.min(durationMs, now - startTime);

                // Log.d(TAG, "currentMs: " + currentMs);
                final float interpolation = interpolator
                        .getInterpolation(currentMs / durationMs);
                // Log.d(TAG, "interpolation: " + interpolation);
                mAnimatingUserHandHourF = oldUserHandHourF + delta
                        * interpolation;
                // Log.d(TAG, "mAnimHourF: " + mAnimatingUserHandHourF);
                // Use post for non-UI thread behavior
                postInvalidate();

                if (currentMs < durationMs)
                    mHandler.post(this);
                else
                    mIsAnimating = false;

            }
        });
    }

    private void drawHeatingArcs(Canvas canvas, float userHandHourF) {

        // offset the time by 1/4 of the UNITS (ie: rotate by 90 degrees) so up
        // is zero
        float theOffset = UNITS / 4;
        float userHandDegStartWithOffset = (userHandHourF - theOffset) / UNITS
                * 360;

        // Idle heating arc, unless unlimited timer period
        if (mUserTimeData.getTimerPeriod() != TimerData.TIMER_PERIOD_UNLIMITED) {
            final float idleDurationF = mUserTimeData.getTimerPeriod()
                    - mUserTimeData.getDuration();
            float heatIdleSweepDeg = Math.abs(idleDurationF / 60F / UNITS
                    * 360F);
            // Draw arc anti-clockwise
            canvas.drawArc(mRimRect, userHandDegStartWithOffset,
                           heatIdleSweepDeg, false, mIdleHeatingArcPaint);
        }

        // heating arc
        final float durationF = mUserTimeData.getDuration();
        float heatOnSweepDeg = Math.abs(durationF / 60F / UNITS * 360F);
        // Draw arc anti-clockwise
        canvas.drawArc(mRimRect, userHandDegStartWithOffset, -heatOnSweepDeg,
                       false, mHeatingArcPaint);
    }

    private static float getUserHandHourF(TimerData data) {
        float userHandHourF = 0;
        if (data.isValid()) {
            userHandHourF = data.getReadyTime().getHour();
            userHandHourF += ((float) data.getReadyTime().getMinute()) / 60;
        }
        return userHandHourF;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getAction();
        switch (action) {
        case MotionEvent.ACTION_UP:

            // Mark handle unselected
            mIsUserHandSelected = false;
            break;

        case MotionEvent.ACTION_DOWN:

            if (isUserHandTouch(event)) {
                // Mark handle selected
                mIsUserHandSelected = true;

                // redraw unselected handle
                invalidate();
                return true;
            } else {
                Log.d(TAG, "Ignored touch");
                // Ignore if touch down point is not close enough to current
                // hand position
                return false;
            }

        case MotionEvent.ACTION_MOVE:
            // Mark handle selected (probably not needed again)
            mIsUserHandSelected = true;
            break;

        default:
            // Ignore other events
            return false;
        }

        // Calculate new angle
        double touchAngle = getAngle(event);

        // Convert touch angle to float time
        float touchHourF = (float) (touchAngle / 360 * 24);
        int touchHours = (int) touchHourF; // cut decimals
        int touchMinutes = (int) ((touchHourF - touchHours) * 60);

        // Round to nearest ROUND_TOUCH_MINUTE
        touchMinutes = (int) Math.rint((double) touchMinutes
                / ROUND_TOUCH_MINUTE)
                * ROUND_TOUCH_MINUTE;
        if (touchMinutes >= 60) {
            // next hour
            touchHours++;
            touchMinutes = 0;
        }

        // Avoid 24:00
        if (touchHours >= 24) {
            touchHours = 0;
            touchMinutes = 0;
        }

        SimpleTime touchTime = new SimpleTime(touchHours, touchMinutes);

        // Set new ready time
        mUserTimeData.setReadyTime(touchTime);
        // Log.d(TAG, "New time from touch: " + touchHours + ":" +
        // touchMinutes);

        // Redraw
        invalidate();

        if (action == MotionEvent.ACTION_UP) {

            // Inform activity
            if (onActionListener != null && mUserTimeData.isValid()) {
                onActionListener.onUserHandReleaseAction(mUserTimeData
                        .getReadyTime());
            }
        } else {
            if (isLiveUpdate())
                // Inform activity
                if (onActionListener != null && mUserTimeData.isValid()) {
                    onActionListener.onUserHandMoveAction(mUserTimeData
                            .getReadyTime());
                }
        }

        return true;
    }

    /**
     * Test if touch event is near enough the current position of user handle
     * 
     * @param event
     * @return true if close and false otherwise
     */
    private boolean isUserHandTouch(MotionEvent event) {
        boolean isValidTouch = false;

        // First check the angle
        // /////////////////////
        boolean isInsideArc = false;

        double touchDeg = getAngle(event);
        Log.d(TAG, "handDeg: " + mUserHandCurrentDeg + ", touchDeg: "
                + touchDeg);

        // Calulate limits for the touch area
        float firstLimitDeg = (mUserHandCurrentDeg + USER_HAND_TOUCH_ANGLE_MAX_THRESHOLD) % 360;
        float secondLimitDeg = (mUserHandCurrentDeg
                - USER_HAND_TOUCH_ANGLE_MAX_THRESHOLD + 360) % 360;

        Log.d(TAG, String.format("Test if angle %.1f is between %.1f and %.1f",
                                 touchDeg, secondLimitDeg, firstLimitDeg));

        // Test if limits are on opposite side of 0 deg
        if (firstLimitDeg > secondLimitDeg)
            isInsideArc = touchDeg < firstLimitDeg && touchDeg > secondLimitDeg;
        else
            isInsideArc = touchDeg < firstLimitDeg || touchDeg > secondLimitDeg;

        // The check the distance from the center
        // ////////////////////////////////
        boolean isOutsideCenter = false;
        double distancePixels = Math
                .hypot(Math.abs(mCenterRawX - event.getX()),
                       Math.abs(mCenterRawY - event.getY()));
        double distanceRelative = distancePixels / mCenterRawX;
        Log.d(TAG, String
                .format("Test if distance %.2f is between %.2f and %.2f",
                        distanceRelative,
                        USER_HAND_TOUCH_CENTER_DISTANCE_MIN_THRESHOLD,
                        USER_HAND_TOUCH_CENTER_DISTANCE_MAX_THRESHOLD));
        if (distanceRelative > USER_HAND_TOUCH_CENTER_DISTANCE_MIN_THRESHOLD
                && distanceRelative < USER_HAND_TOUCH_CENTER_DISTANCE_MAX_THRESHOLD)
            isOutsideCenter = true;

        // Both rules must be valid
        isValidTouch = isInsideArc && isOutsideCenter;

        return isValidTouch;

    }

    private double getAngle(MotionEvent event) {
        float pX = event.getX();
        float pY = event.getY();

        float opposite = Math.abs(mCenterRawX - pX);
        float adjacent = Math.abs(mCenterRawY - pY);

        // never divide by zero
        double userHandAngle = Math.toDegrees(Math.atan(opposite
                / (adjacent != 0 ? adjacent : 1)));
        if (pX > mCenterRawX) {
            if (pY > mCenterRawY) // 2nd quarter
                userHandAngle = 180 - userHandAngle;
        } else { // left half
            if (pY > mCenterRawY) // 3rd quarter
                userHandAngle += 180;
            else
                // 4th quarter
                userHandAngle = 360 - userHandAngle;
        }
        return userHandAngle;
    }
}