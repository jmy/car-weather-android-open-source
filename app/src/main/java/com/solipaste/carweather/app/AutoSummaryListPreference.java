/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import android.content.Context;
import android.preference.ListPreference;
import android.util.AttributeSet;

public class AutoSummaryListPreference extends ListPreference {

    private final String mSummaryFormat;

    public AutoSummaryListPreference(Context context) {
        super(context);
        mSummaryFormat = super.getSummary().toString();
    }

    public AutoSummaryListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        mSummaryFormat = super.getSummary().toString();
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult && mSummaryFormat != null) {
            setSummary(String.format(mSummaryFormat, getEntry()));
        }
    }

    @Override
    public CharSequence getSummary() {
        String summary = "";

        if (mSummaryFormat != null)
            summary = String.format(mSummaryFormat, getEntry());

        return summary;
    }

}
