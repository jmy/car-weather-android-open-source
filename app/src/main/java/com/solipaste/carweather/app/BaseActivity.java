/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

public abstract class BaseActivity extends TrackedFragmentActivity {

    private MenuItem mRefreshButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        enableProgressIndicator(false);

        if (android.os.Build.VERSION.SDK_INT < 11) {
            View title = getWindow().findViewById(android.R.id.title);
            View titleBar = (View) title.getParent();
            titleBar.setBackgroundResource(R.drawable.old_title_background);
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mRefreshButton = menu.findItem(R.id.menu_refresh);
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Hide refresh and enable progress indicator
     * 
     * @param visible
     */
    protected void enableProgressIndicator(boolean visible) {
        if (mRefreshButton != null) {
            mRefreshButton.setVisible(!visible);
        }
        setProgressBarIndeterminateVisibility(visible);
    }
}
