/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.solipaste.carweather.domain.SimpleTime;

/**
 * Receiver reacting on device boot and application update re-schedule
 * notifications
 * 
 */
public class BootReceiver extends BroadcastReceiver {
    private final static String TAG = BootReceiver.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG, "onReceive: " + Log.o(intent));

        /*
         * Make sure to exit this within 10 sec!
         */

        // In case of PACKAGE_REPLACE, check if this app was updated
        if (intent != null && intent.getAction() != null
                && intent.getAction().equals(Intent.ACTION_PACKAGE_REPLACED))
            if (!intent.getData().getSchemeSpecificPart()
                    .equals(context.getPackageName())) {
                Log.d(TAG, "Some other application was updated, exiting");
                return;
            } else {
                Log.d(TAG, "This application was updated");
            }

        // Other intents are accepted as such

        Log.i(TAG, "BootReceiver invoked, configuring AlarmManager");

        // Initialize prefs
        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);

        // Handle both notification types
        handleTimingNotification(context, sharedPrefs);
        handleDepartureNotification(context, sharedPrefs);
        Log.d(TAG, "exiting");
    }
    
    private void handleTimingNotification(Context context,
            SharedPreferences sharedPrefs) {

        // Check if timing notifications are enabled
        boolean timingNotifEnabled = sharedPrefs
                .getBoolean(context.getString(R.string.pref_notification_timing_enabled_key),
                            false);

        if (timingNotifEnabled) {
            // Enabled - reschedule notifications
            String timeS = sharedPrefs
                    .getString(context.getString(R.string.pref_notification_timing_time_key),
                               context.getString(R.string.pref_notification_timing_time_default));
            if (timeS != null) {
                SimpleTime notifTime = new SimpleTime(timeS);
                PreferencesActivity.updateTimingNotification(context, true,
                                                             notifTime);
            }
        }
    }

    private void handleDepartureNotification(Context context,
            SharedPreferences sharedPrefs) {

        // Check if timing notifications are enabled
        boolean departureNotifEnabled = sharedPrefs
                .getBoolean(context.getString(R.string.pref_notification_departure_enabled_key),
                            false);

        if (departureNotifEnabled) {
            // Enabled - reschedule notifications
            String useExistingLeadTime = null;
            SimpleTime useExistingDepartureTime = null;
            PreferencesActivity
                    .updateDepartureNotification(context, true,
                                                 useExistingDepartureTime,
                                                 useExistingLeadTime);
        }
    }
}
