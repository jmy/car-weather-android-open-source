/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import com.solipaste.carweather.domain.SimpleTime;
import com.solipaste.carweather.domain.TimerData;


public interface DepartureTimeView {


    /**
     * Interface definition for a callback to be invoked when an action is
     * performed on the editor.
     */
    public interface OnActionListener {
        /**
         * Called when an user hand is moved.
         *
         * @param userHandTime new time
         * @return Return true if you have consumed the action, else false.
         */
        boolean onUserHandMoveAction(SimpleTime userHandTime);

        /**
         * Called when an user hand gesture is released.
         *
         * @param userHandTime new time
         * @return Return true if you have consumed the action, else false.
         */
        boolean onUserHandReleaseAction(SimpleTime userHandTime);
    }

    public void setOnActionListener(OnActionListener l);

    public void setUserHandTime(SimpleTime time, AnalogClockDepartureTimeView.UserHandType type);


    public int getDurationMinutes();

    public void setDurationMinutes(int d);

    public void setUserTimeData(TimerData data);
}