/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

public class HelpDialog extends DialogFragment {
    private final static String TAG = HelpDialog.class.getName();

    public interface HelpDialogListener {

        public void onHelpDialogClose(String dialogType);

        public void onWelcomeDialogCloseWithAbout();

    }

    public static final String FRAGMENT_TAG_HELPDIALOG = "com.solipaste.carweather.helpdialog";
    public static final String FRAGMENT_TAG_WELCOMEDIALOG = "com.solipaste.carweather.welcomedialog";

    private static final String CHARSET = "UTF-8";
    private static final String ASSET_PATH_FORMAT = "%s/%s";
    private static final String ASSET_PATH_FORMAT_WITH_LANG = "%s-%s/%s";
    private static final String ASSET_FOLDER = "html";

    public static final String ASSET_FILE_WELCOME = "welcome.html";

    private String mText;
    private final String mContentFile = ASSET_FILE_WELCOME;

    private HelpDialogListener mCallback;

    public HelpDialog() {
        Log.v(TAG, "AboutDialog");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate");

        super.onCreate(savedInstanceState);

        StringBuilder text = new StringBuilder();

        try {
            String langCode = Locale.getDefault().getLanguage();

            String path = String.format(ASSET_PATH_FORMAT_WITH_LANG,
                                        ASSET_FOLDER, langCode, mContentFile);
            InputStream is = null;
            try {
                is = getActivity().getAssets().open(path);
            } catch (IOException e) {
                path = String.format(ASSET_PATH_FORMAT, ASSET_FOLDER,
                                     mContentFile);
                is = getActivity().getAssets().open(path);
            }
            InputStreamReader reader = new InputStreamReader(is, CHARSET);
            BufferedReader br = new BufferedReader(reader);
            char[] cbuf = new char[1024];
            int count = 0;
            while (count != -1) {
                count = br.read(cbuf);
                if (count > 0)
                    text.append(cbuf, 0, count);
            }

        } catch (IOException e) {
            Log.e(TAG, "Reading text for dialog failed");
            e.printStackTrace();
        }

        mText = text.toString();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.v(TAG, "onCreateDialog");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Dialog has no parent, hence null
        @SuppressLint("InflateParams") final View view = inflater.inflate(R.layout.dialog_about, null);
        TextView textView = (TextView) view.findViewById(R.id.about_text);
        ScrollView scrollView = (ScrollView) view.findViewById(R.id.about_dialog_scrollview);
        ImageView imageView = (ImageView) view.findViewById(R.id.about_image);
        
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        scrollView.setScrollbarFadingEnabled(false);

        imageView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Go to Google play when image is clicked

                FragmentActivity a = getActivity();
                if (a != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(a
                            .getString(R.string.market_google_play)));
                    startActivity(i);
                }
            }
        });

        // Set text to dialog
        if (mText != null)
            textView.setText(Html.fromHtml(mText));

        // Set correct close button title
        int closeButtonTitleResource = 0;
        if (getTag() != null && getTag().equals(FRAGMENT_TAG_WELCOMEDIALOG))
            closeButtonTitleResource = R.string.welcome_dialog_close_button_title;
        else
            closeButtonTitleResource = R.string.dialog_close_button_title;

        // Set correct dialog title
        int dialogTitleResource = 0;
        if (getTag() != null && getTag().equals(FRAGMENT_TAG_WELCOMEDIALOG))
            dialogTitleResource = R.string.welcome_dialog_title;
        else
            dialogTitleResource = R.string.help_dialog_title;

        // Use the Builder class for convenient dialog construction
        builder.setView(view)
                .setTitle(getString(dialogTitleResource))
                .setNeutralButton(closeButtonTitleResource,
                                  new DialogInterface.OnClickListener() {
                                      @Override
                                      public void onClick(
                                              DialogInterface dialog, int which) {
                                          if (mCallback != null)
                                              mCallback
                                                      .onHelpDialogClose(getTag());
                                          dismiss();
                                      }
                                  });

        // Add About button to welcome dialog
        if (getTag() != null && getTag().equals(FRAGMENT_TAG_WELCOMEDIALOG))
            builder.setPositiveButton(R.string.welcome_dialog_about_button,
                                      new DialogInterface.OnClickListener() {

                                          @Override
                                          public void onClick(
                                                  DialogInterface dialog,
                                                  int which) {
                                              if (mCallback != null)
                                                  mCallback
                                                          .onWelcomeDialogCloseWithAbout();
                                              dismiss();
                                          }
                                      });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        try {
            mCallback = (HelpDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement HelpDialogListener");
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        FragmentActivity activity = getActivity();
        if (activity != null)
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

    }
}
