/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;

import android.content.Context;
import android.os.AsyncTask;

import com.solipaste.carweather.domain.Locations;

/**
 * Background task that fetched the content from server and parses the content.
 */
public class InitializeDataTask extends AsyncTask<Locations, Void, Locations> {

    private final static String TAG = "InitializeDataTask";

    //STOPSHIP
    private static final boolean GENERATE_TEST_DATA = true;

    private static final String LOCATIONS_FILE_NAME = "locations.csv";

    private final LocationsCallback mCallback;

    private final Context mContext;

    public InitializeDataTask(Context context, LocationsCallback callback) {
        mCallback = callback;
        mContext = context;
    }

    @Override
    protected Locations doInBackground(Locations... l) {
        Log.v(TAG, "doInBackground");
        // try to load XML from HTTP server and parse
        try {
            if (mCallback != null && !isCancelled()) {

                Reader reader = new BufferedReader(
                        new InputStreamReader(mContext.getAssets()
                                .open(LOCATIONS_FILE_NAME), "UTF-8"));

                if (!isCancelled())
                    l[0].Initialize(reader, GENERATE_TEST_DATA);
            
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (mCallback != null && !isCancelled())
                mCallback.onFailure();
        }
        return l[0];
    }

    @Override
    protected void onPostExecute(Locations l) {
        Log.v(TAG, "onPostExecute");
        if (mCallback != null && !isCancelled()) {
            if (l != null) {
                mCallback.setLocations(l);
                mCallback.onSuccess();

            }
        }

    }

}