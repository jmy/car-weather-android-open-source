/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;

import com.solipaste.carweather.domain.Location;

public class LocationsActivity extends BaseActivity implements
        OnLocationSelectedListener {
    private final static String TAG = LocationsActivity.class.getName();

    @TargetApi(11)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_locations);
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public void onLocationSelected(Location l) {
        Log.v(TAG, "onLocationSelected");
        setResult(Activity.RESULT_OK);
        finish();
    }
}
