/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import java.lang.reflect.Field;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter.FilterListener;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.TwoLineListItem;

import com.google.android.apps.analytics.easytracking.EasyTracker;
import com.solipaste.carweather.domain.Location;
import com.solipaste.carweather.domain.Location.Country;
import com.solipaste.carweather.domain.Locations;

@SuppressWarnings("deprecation")
public class LocationsListFragment extends ListFragment {
    private final static String TAG = LocationsListFragment.class.getName();

    private Locations mLocations = null;

    private ArrayAdapter<Location> mAdapter;

    private SharedPreferences mSharedPrefs;

    private SparseArray<String> countryNames;

    protected View mNoResultsView;

    private View mListProgressView;

    private OnLocationSelectedListener mListener;

    private InitializeDataTask mInitTask;

    public LocationsListFragment() {
        Log.v(TAG, "LocationsListFragment");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_locations, container);

        mNoResultsView = view.findViewById(R.id.locations_no_results);
        mListProgressView = view.findViewById(R.id.locations_progress);

        setListProgressBarVisibility(true);

        return view;
    }

    @Override
    public void onAttach(final android.app.Activity activity) {
        Log.v(TAG, "onAttach");
        super.onAttach(activity);

        try {
            mListener = (OnLocationSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnArticleSelectedListener");
        }

        // Initialize prefs
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity);

        // Initialize localized country names
        countryNames = new SparseArray<>();
        for (Country c : Country.values()) {
            final String localizedCountryName = getLocalizedCountryName(c);
            if (localizedCountryName != null)
                countryNames.put(c.getCode(), localizedCountryName);
        }

        // Initialize location list
        mLocations = new Locations();

        // Read location list from file
        mInitTask = new InitializeDataTask(activity, new LocationsCallback() {

            @Override
            public void setLocations(Locations locations) {
                mLocations = locations;
            }

            @Override
            public void onSuccess() {
                setListProgressBarVisibility(false);

                FragmentActivity a = getActivity();
                if (a != null)
                    initializeList();
            }

            @Override
            public void onFailure() {
                setListProgressBarVisibility(false);

                FragmentActivity a = getActivity();
                if (a != null)
                    Toast.makeText(a,
                                   R.string.locations_list_initialization_failure,
                                   Toast.LENGTH_LONG).show();
            }
        });
        mInitTask.execute(mLocations);

    }

    @Override
    public void onDetach() {
        Log.v(TAG, "onDetach");
        
        super.onDetach();

        mListener = null;
        // Try to cancel location list initialization
        if (mInitTask != null) {
            Log.d(TAG, "Cancelling mInitTask");
            mInitTask.cancel(true);
            mInitTask = null;
        }

    }

    /**
         * 
         */
    private void initializeList() {
        Log.v(TAG, "initializeList");

        // Abort if activity does not exist anymore
        FragmentActivity a = getActivity();
        if (a == null)
            return;

        final LayoutInflater inflater = (LayoutInflater) a
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final List<Location> locs = mLocations.getLocationList();
        if (locs != null)

            // List content
            mAdapter = new ArrayAdapter<Location>(a,
                    android.R.layout.simple_list_item_2, locs)
            {
                @Override
                public View getView(int position, View convertView,
                        ViewGroup parent) {
                    TwoLineListItem row;
                    if (convertView == null) {
                        row = (TwoLineListItem) inflater
                                .inflate(android.R.layout.simple_list_item_2,
                                         null);
                        row.getText1()
                                .setTextColor(getResources()
                                                      .getColor(R.color.locations_list_text1));
                        row.getText2()
                                .setTextColor(getResources()
                                                      .getColor(android.R.color.darker_gray));
                    } else {
                        row = (TwoLineListItem) convertView;
                    }

                    // possibly filter results list so get location from adapter
                    final Location loc = getItem(position);
                    if (loc != null) {
                        String city = loc.getName();
                        if (city != null)
                            row.getText1().setText(city);
                        final String uiName = loc
                                .getLongRegionUIName(countryNames);
                        if (uiName != null)
                            row.getText2().setText(uiName);
                    }
                    return row;
                }

            };

        if (mAdapter != null)
            getListView().setAdapter(mAdapter);

        // Filtering
        EditText filterText = (EditText) a.findViewById(R.id.locations_filter);
        filterText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                    int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                    int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filterWithEmptyResultNote(s.toString());
            }
        });

        String initialFilter = filterText.getText().toString();
        if (initialFilter.length() > 0) {
            filterWithEmptyResultNote(initialFilter);
        }

        filterText.setOnEditorActionListener(new OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                    KeyEvent event) {
                if (event != null && event.getAction() != KeyEvent.ACTION_DOWN)
                    return false;
                if (actionId == EditorInfo.IME_NULL && v != null) {
                    String text = v.getText().toString();
                    Location loc = mLocations.getLocation(text);
                    if (loc != null) {
                        storeLocationInPrefs(loc);
                        if (mListener != null)
                            mListener.onLocationSelected(loc);
                    }
                    return true;
                } else
                    return false;
            }
        });

        getListView().setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                    int position, long id) {
                String locS = ((TwoLineListItem) v).getText1().getText()
                        .toString();
                Location loc = mLocations.getLocation(locS);
                storeLocationInPrefs(loc);

                if (mListener != null)
                    mListener.onLocationSelected(loc);
            }
        });
    }

    private void filterWithEmptyResultNote(String filter) {
        Log.v(TAG, "filterWithProgress");
        // setListProgressBarVisibility(true);
        setNoResultsNoteVisibility(false);

        if (mAdapter != null)
            mAdapter.getFilter().filter(filter, new FilterListener() {

                @Override
                public void onFilterComplete(int count) {
                    // setListProgressBarVisibility(false);
                    setNoResultsNoteVisibility(count <= 0);
                    Log.v(TAG, "onFilterComplete");
                }
            });
    }

    private void storeLocationInPrefs(Location loc) {

        String locLong = null;
        if (loc == null) {
            Log.d(TAG, "invalid location");
        } else if (loc.getName().equals("test")) {
            locLong = "test";
            Log.v(TAG, "Using test weather");
        } else if (loc.getName().equals("random")) {
            locLong = "random";
            Log.v(TAG, "Using random weather");
        } else {
            // construct long location string
            locLong = loc.getLongName();
            if (locLong == null) {
                Log.e(TAG, "cannot construct long location: " + loc.getName());
            }
        }

        if (locLong != null) {
            // Save current user location to prefs
            // This will trigger listeners in main thread to be called
            Editor e = mSharedPrefs.edit();
            e.putString(getString(R.string.pref_location_key), loc.getName());
            e.putString(getString(R.string.pref_location_long_key), locLong);
            e.commit();

            EasyTracker
                    .getTracker()
                    .trackEvent(getActivity()
                                        .getString(R.string.analytics_event_locations_category),
                                getActivity()
                                        .getString(R.string.analytics_event_locations_locationchange_action),
                                loc.getCountryName(), 0);

        }
    }

    private static int getResId(String variableName, Context context, Class<?> c) {

        try {
            Field idField = c.getDeclaredField(variableName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    private String getLocalizedCountryName(Location.Country country) {
        int countryRes = getResId("country_name_" + country.name(),
                                  getActivity(),
                                  com.solipaste.carweather.app.R.string.class);
        String countryName = null;
        try {
            countryName = getString(countryRes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return countryName;
    }

    private void setListProgressBarVisibility(boolean visible) {
        mListProgressView
                .setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    private void setNoResultsNoteVisibility(boolean visible) {
        mNoResultsView.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }
}
