/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import java.lang.ref.WeakReference;
import java.util.Date;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.apps.analytics.easytracking.EasyTracker;
import com.solipaste.carweather.app.AboutDialog.AboutDialogListener;
import com.solipaste.carweather.app.HelpDialog.HelpDialogListener;
import com.solipaste.carweather.app.TimePickerFragment.TimePickerType;
import com.solipaste.carweather.domain.Forecast;
import com.solipaste.carweather.domain.ForecastData;
import com.solipaste.carweather.domain.HeatOptimizer;
import com.solipaste.carweather.domain.Heater;
import com.solipaste.carweather.domain.Heater.HeaterType;
import com.solipaste.carweather.domain.Location;
import com.solipaste.carweather.domain.SimpleTime;
import com.solipaste.carweather.domain.Temperature;
import com.solipaste.carweather.domain.TimerData;
import com.solipaste.carweather.domain.WeatherSymbol;
import com.solipaste.carweather.service.WeatherService;
import com.solipaste.carweather.service.WeatherService.CreditsFormat;
import com.solipaste.carweather.service.WeatherServiceAsyncTask;
import com.solipaste.carweather.service.WeatherServiceCallback;

/**
 * Main Activity for the application
 *
 */
public class MainActivity extends BaseActivity implements
        AnalogClockDepartureTimeView.OnActionListener, AboutDialogListener, HelpDialogListener,
        SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = MainActivity.class.getName();

    private static final int TIME_UPDATE_START_DELAY_MS = 100;
    private static final int FAILED_NOTIFICATIONS_HANDLE_START_DELAY_MS = 5000;

    private static final String START_FROM_NOTIFICATION_TRACKED_ALREADY = "start_from_notification_tracked_already";

    public static class MainActivityData {
        public Location location;
        public Heater heater = new Heater(HeaterType.RADIATOR);
        public Forecast departureForecast = new Forecast();
        public TimerData timerData = new TimerData();
        public boolean adsMainVisible;
        protected ForecastData weatherForecast;
        protected String weatherCredits;

        public MainActivityData() {
        }
    }

    /**
     * Runnable clock updater
     */
    private final Runnable mUpdateTimeTask = new Runnable() {
        @Override
        public void run() {
            // Work items
            if (mClock != null)
                mClock.updateWallTime();
            updateTimeDataToUI();
            storeDepartureTimeOnPrefs();

            // Calculate next repeat wall time
            long millis = System.currentTimeMillis();
            long millisUntilNextMinute = 60000 - (millis % 60000);
            // Log.v(TAG, "next clock update in " + msUntil + "ms");

            // Repeat every minutes
            mHandler.postDelayed(this, millisUntilNextMinute);
        }
    };

    /**
     * Runnable clock updater
     */
    private final Runnable mFailedNotificationsHandler = new Runnable() {
        @Override
        public void run() {
            handleFailedNotifications();
        }
    };

    private enum IntentRequestCode {
        PREFERENCES_ACTIVITY, LOCATION_ACTIVITY;

        /**
         * Returns int value. Values must not be persisted.
         */
        public int getValue() {
            return ordinal();
        }
    }

    private Toolbar mToolbar;
    private SwipeRefreshLayout mSwipeLayout;
    private ScrollView mSwipeView;

    private TextView mDurationTextView;
    private TextView mTemperatureTextView;
    private ImageView mWeatherSymboImageView;
    private TextView mStartTimeEditText;
    private Button mDepartureTimeButton;
    private TextView mStopTimeEditText;
    private TextView mStopTimeFromNowTextView;

    private TextView mTodayToggleTextView;
    private TextView mWeatherCreditsTextView;
    private ImageView mAdMainView;

    private DepartureTimeView mDepartureTime;
    private ClockView mClock;

    private MainActivityData mData = null;
    private WeatherService mWeather;
    private SharedPreferences mSharedPrefs;

    private boolean mIsRereadFromPreferencesNeeded;

    private boolean mDepartureTimeSavedToPrefs;

    private boolean mStartFromNotificationAlreadyTracked;

    // Handles responses from threads and time picker
    private final static Handler mHandler = new Handler();

    // Stores active or the last weather update task
    private AsyncTask<WeatherService, Void, ForecastData> mWeatherServiceTask;

    private HelpDialog mHelpWelcomeDialog;
    private boolean mIsWelcomeShown;

    private WeakReference<MainActivity> mWrActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        mWrActivity = new WeakReference<>(this);

        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setHomeButtonEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setElevation(2);
            }
        } else {
            Log.w(TAG, "No toolbar!");
        }

        if (savedInstanceState != null
                && savedInstanceState
                .getBoolean(START_FROM_NOTIFICATION_TRACKED_ALREADY,
                        false))
            mStartFromNotificationAlreadyTracked = true;

        mTemperatureTextView = (TextView) findViewById(R.id.temperatureValue);
        mWeatherSymboImageView = (ImageView) findViewById(R.id.weatherSymbolImage);
        if (mWeatherSymboImageView != null)
            mWeatherSymboImageView.setVisibility(View.INVISIBLE);

        mDurationTextView = (TextView) findViewById(R.id.durationValue);
        mStartTimeEditText = (TextView) findViewById(R.id.startTimeValue);
        mDepartureTimeButton = (Button) findViewById(R.id.departureTimeValue);
        mStopTimeEditText = (TextView) findViewById(R.id.stopTimeValue);
        mStopTimeFromNowTextView = (TextView) findViewById(R.id.stopTimeFromNowValue);

        mTodayToggleTextView = (TextView) findViewById(R.id.time_tomorrow);
        mDepartureTime = (DepartureTimeView) findViewById(R.id.departureView);
//        mClock = (ClockView) findViewById(R.id.analogClock);
//        mWeatherCreditsTextView = (TextView) findViewById(R.id.weatherServiceCredits);
//        if (mWeatherCreditsTextView != null) {
//            mWeatherCreditsTextView.setMovementMethod(LinkMovementMethod
//                    .getInstance());
//            mWeatherCreditsTextView.setVisibility(View.INVISIBLE);
//        }
        mAdMainView = (ImageView) findViewById(R.id.ad_main);

        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        if (mSwipeLayout != null) {
            mSwipeLayout.setOnRefreshListener(this);
            mSwipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);
        }
        // Initialize preferences to defaults if not done yet
        PreferenceManager.setDefaultValues(this.getApplicationContext(),
                R.xml.preferences_main, false);

        // Initialize prefs
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this
                .getApplicationContext());

        // Initialize data if earlier data has not been persisted
        mData = (MainActivityData) getLastCustomNonConfigurationInstance();
        if (mData == null) {
            mData = new MainActivityData();
        } else {
            if (mAdMainView != null)
                mAdMainView.setVisibility(mData.adsMainVisible ? View.VISIBLE
                        : View.GONE);
        }

        // Listen changes to analog clock
        if (mDepartureTime != null)
            mDepartureTime.setOnActionListener(this);

    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        Log.v(TAG, "onCreatePanelMenu");
        boolean visible = super.onCreatePanelMenu(featureId, menu);

        return visible;
    }

    @Override
    protected void onDestroy() {
        Log.v(TAG, "onDestroy");
        super.onDestroy();

        // Stop listening to clock
        if (mDepartureTime != null)
            mDepartureTime.setOnActionListener(null);

    }

    @Override
    protected void onStart() {
        Log.v(TAG, "onStart");
        super.onStart();

        // track application start from notification
        // TrackedActivity should have been initialized already by now
        Intent intent = getIntent();
        if (intent != null
                && intent
                .getBooleanExtra(getString(R.string.extra_notification_selected),
                        false)
                && !mStartFromNotificationAlreadyTracked) {
            EasyTracker tracker = EasyTracker.getTracker();
            if (tracker != null && EasyTracker.getTracker() != null) {
                EasyTracker
                        .getTracker()
                        .trackEvent(getString(R.string.analytics_event_notifications_category),
                                getString(R.string.analytics_event_notifications_notificationselected_action),
                                null, 0);
                // Avoid multiple action trackings if activity restarted
                mStartFromNotificationAlreadyTracked = true;
            }
        }
    }

    @Override
    protected void onStop() {
        Log.v(TAG, "onStop");
        super.onStop();

        // Cancel any running tasks
        if (mWeatherServiceTask != null) {
            boolean wasCancelled = mWeatherServiceTask.cancel(true);
            if (wasCancelled) {
                Log.d(TAG, "WeatherServiceTask was cancelled successfully");
            }
            // Disable progress indicator in any case
            enableProgressIndicator(false);
        }
    }

    @Override
    protected void onPause() {
        Log.v(TAG, "onPause");
        super.onPause();

        // Stop all handlers
        mHandler.removeCallbacksAndMessages(null);

        // Store valid user departure time to prefs
        // All other data is persisted as it changes
        storeDepartureTimeOnPrefs();
    }

    @Override
    protected void onResume() {
        Log.v(TAG, "onResume");
        super.onResume();

        // Stop all handlers before restarting them
        mHandler.removeCallbacksAndMessages(null);

        // Resume clock updates
        mHandler.postDelayed(mUpdateTimeTask, TIME_UPDATE_START_DELAY_MS);

        // Handle failed notifications
        mHandler.postDelayed(mFailedNotificationsHandler,
                FAILED_NOTIFICATIONS_HANDLE_START_DELAY_MS);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        Log.v(TAG, "onWindowFocusChanged: " + hasFocus);
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus) {
            if (mData.location == null || mIsRereadFromPreferencesNeeded)
                // current location not yet available or not yet read from prefs
                updateFromPreferences();

            mIsRereadFromPreferencesNeeded = false;

            // Update departure time immediately to UI
            // Otherwise there is no data if calculate fails
            if (mDepartureTime != null)
                mDepartureTime.setUserTimeData(mData.timerData);
            updateDepartureTimeToDigital();

            // Check first start based on location
            if (mData.location != null) {
                // A. Non-first start procedure:
                updateLocationToUI();

                // location available and
                // window is visible - recalculate all
                calculateAll(false, false); // TODO Wait until we have network
                // connection

            } else {
                // B. The first start procedure:
                // First show welcome dialog and then let user select the
                // location
                showFirstStart();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Reload settings if we returned from preferences
        if (requestCode == IntentRequestCode.PREFERENCES_ACTIVITY.getValue())
            mIsRereadFromPreferencesNeeded = true;

        // Reset temperature data if a location was selected in Locations
        // list
        if (requestCode == IntentRequestCode.LOCATION_ACTIVITY.getValue()
                && resultCode == Activity.RESULT_OK) {
            mData.weatherForecast = null;
            mData.location = null;
        }
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        super.onRetainCustomNonConfigurationInstance();

        return mData;

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(START_FROM_NOTIFICATION_TRACKED_ALREADY,
                mStartFromNotificationAlreadyTracked);
    }

    private void storeDepartureTimeOnPrefs() {

        if (mData.timerData.isValid() && !mDepartureTimeSavedToPrefs) {
            Log.v(TAG, "storeDepartureTimeOnPrefs, save needed");

            Editor editor = mSharedPrefs.edit();
            editor.putString(getString(R.string.pref_departure_time_hour_key),
                    String.valueOf(mData.timerData.getReadyTime()
                            .getHour()));
            editor.putString(getString(R.string.pref_departure_time_min_key),
                    String.valueOf(mData.timerData.getReadyTime()
                            .getMinute()));
            editor.commit();
            mDepartureTimeSavedToPrefs = true;

            // Also update departure time notification but only if enabled
            if (mSharedPrefs
                    .getBoolean(getString(R.string.pref_notification_departure_enabled_key),
                            false))

                PreferencesActivity
                        .updateDepartureNotification(this, true,
                                mData.timerData
                                        .getReadyTime(),
                                null);
        }
    }

    /**
     * Update local variables from shared preferences
     */
    private void updateFromPreferences() {
        Log.v(TAG, "updateFromPreferences");

        // Heater type
        // ////////////
        mData.heater = readHeaterTypeFromPrefs(this, mSharedPrefs);

        // Departure time
        // ///////////
        mData.timerData = getTimerDataFromPrefs(this, mSharedPrefs);

        // Ads visible
        // ////////////
        mData.adsMainVisible = mSharedPrefs
                .getBoolean(getString(R.string.pref_ads_visible_main), false);
        if (mAdMainView != null)
            mAdMainView.setVisibility(mData.adsMainVisible ? View.VISIBLE
                    : View.GONE);

        // Location
        // /////////
        String name = mSharedPrefs
                .getString(getString(R.string.pref_location_key), "");
        String longName = mSharedPrefs
                .getString(getString(R.string.pref_location_long_key), "");

        if (name != null && name.length() > 0 && longName != null
                && longName.length() > 0) {
            // Initialize location
            mData.location = new Location(name, longName);
        }

    }

    private void handleFailedNotifications() {

        // Check if there are failed notifications recorded on prefs
        String dateLongString = null;
        try {
            dateLongString = mSharedPrefs
                    .getString(getString(R.string.pref_notification_failed_time_key),
                            null);
        } catch (ClassCastException e) {
            Editor editor = mSharedPrefs.edit();
            editor.remove(getString(R.string.pref_notification_failed_time_key));
            editor.commit();
            return;
        }

        // Show toast about the failed notification
        if (dateLongString != null) {
            Log.v(TAG, "handleFailedNotifications: " + dateLongString);

            long dateLong = Long.parseLong(dateLongString);

            String formattedTimeString = DateFormat
                    .getTimeFormat(getApplicationContext())
                    .format(new Date(dateLong));

            Toast.makeText(this,
                    getString(R.string.toast_notification_failed_format,
                            formattedTimeString), Toast.LENGTH_LONG)
                    .show();
            Editor editor = mSharedPrefs.edit();
            editor.remove(getString(R.string.pref_notification_failed_time_key));
            editor.commit();
        }
    }

    /**
     * Calculates data for UI Uses cached data if available and fresh enough
     */
    public void calculateAll(boolean forceRefresh, boolean silent) {
        Log.v(TAG, "calculateAll");

        if (mData.location == null)
            // location not yet selected - just cancel silently
            return;

        if (mData.weatherForecast == null
                || !mData.weatherForecast.isFreshData() || forceRefresh)
            reloadDataFromNetwork(silent);
        else
            // Use existing temperature data
            calculateData();
    }

    public void calculateData() {
        Log.v(TAG, "calculateData");

        // Get temp for current departure time
        if (mData.weatherForecast != null && mData.timerData.isValid()) {
            Date date = mData.timerData.getReadyTime().getDate();

            // Get temperature from data
            // TODO Check that forecast for given data exists first
            mData.departureForecast = mData.weatherForecast.getForecast(date);

            // Update temperature - could be error value
            updateTemperatureToUI();

            // Calculate optimal heating duration
            mData.timerData.setDuration(calculateDuration());

            // Update all clocks
            updateTimeDataToUI();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.v(TAG, "onOptionsItemSelected");

        int id = item.getItemId();
        switch (id) {
            case R.id.menu_about:
                showAboutDialog();
                return true;
            case R.id.menu_location:
                showLocationSelectionList();
                return true;
            case R.id.menu_settings:
                Intent myIntent = new Intent(this, PreferencesActivity.class);
                startActivityForResult(myIntent,
                        IntentRequestCode.PREFERENCES_ACTIVITY
                                .getValue());
                return true;
            case R.id.menu_refresh:
                calculateAll(true, false);
                return true;
            // TODO Release: Disable debug menus
            // case R.id.menu_debug_preferences:
            // Utils.logPreferences(this);
            // return true;
            // case R.id.menu_test_notification:
            // Intent i = NotificationService.getNotificationIntent(this);
            // sendBroadcast(i);
            // return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSwipeLayout.setRefreshing(false);
            }
        }, 500);
        calculateAll(true, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    /**
     * Show dialog for setting exact time
     *
     * @param v
     */
    public void onTimeClick(View v) {
        Log.v(TAG, "onTimeClick");

        // Show time picker
        if (mData.timerData.isValid()) {
            DialogFragment newFragment = null;
            if (v.getId() == mStartTimeEditText.getId()) {
                newFragment = TimePickerFragment.newInstance(mData.timerData
                        .getStartTime(), TimePickerType.START_TIME);
            } else if (v.getId() == mDepartureTimeButton.getId()) {
                newFragment = TimePickerFragment.newInstance(mData.timerData
                        .getReadyTime(), TimePickerType.DEPARTURE_TIME);
            }

            if (newFragment != null)
                newFragment.show(getSupportFragmentManager(), "timePicker");
        }
    }

    public void onStartTimeClick(View v) {
        Log.v(TAG, "onStartTimeClick");

        // Set start time to current time
        SimpleTime now = new SimpleTime();
        mData.timerData.setStartTime(now);

        // deparature time changes
        mDepartureTimeSavedToPrefs = false;

        updateTimeDataToUI();

        Toast.makeText(this, getString(R.string.toast_departure_time_asap),
                Toast.LENGTH_SHORT).show();
    }

    public void onTemperatureClick(View v) {
        Log.v(TAG, "onTemperatureClick");

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(mData.weatherCredits));
        startActivity(i);
    }

    public void onAdMainClick(View v) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(getString(R.string.ad_main_url_link)));
        startActivity(browserIntent);
    }

    /**
     * Exact time set by user
     *
     * @param time
     * @param type
     */
    public void handleTimePickerResult(SimpleTime time, TimePickerType type) {
        Log.v(TAG, "Time picker response: " + time.toString());

        // Note that this might be called twice doe to JB bug
        switch (type) {
            case START_TIME:
                mData.timerData.setStartTime(time);
                break;
            case DEPARTURE_TIME:
                updateDepartureTime(time);
                break;
        }

        // Update all clocks
        updateTimeDataToUI();
    }

    @Override
    public boolean onUserHandMoveAction(SimpleTime userHandTime) {

        if (mSwipeLayout != null)
            mSwipeLayout.setEnabled(false);
        boolean modified = false;

        if (updateDepartureTime(userHandTime)) {
            modified = true;

            if (isLiveTemperatureUpdateEnabled())
                // Fetch temperature and update it to UI
                calculateAll(false, false);
            else
                // Update only departure time
                updateDepartureTimeToDigital();
        }

        return modified;
    }

    @Override
    public boolean onUserHandReleaseAction(SimpleTime userHandTime) {

        if (mSwipeLayout != null)
            mSwipeLayout.setEnabled(true);

        // update all except analog clock
        boolean modified = updateDepartureTime(userHandTime);

        // Fetch temperature and update it to UI
        // Also updates departure time
        calculateAll(false, false);

        return modified;
    }

    /**
     * @param newTime
     * @return {@code true} if different from current time
     */
    private boolean updateDepartureTime(SimpleTime newTime) {

        boolean modified = false;

        if (mData.timerData.isValid()) {
            if (!mData.timerData.getReadyTime().equals(newTime)) {
                modified = true;
                mData.timerData.setReadyTime(newTime);
                mDepartureTimeSavedToPrefs = false;
            }
        }
        return modified;
    }

    /**
     * Fetch feather forecast and calculate duration in own Thread. Then update
     * UI in main thread.
     */
    private void reloadDataFromNetwork(boolean silent) {
        Log.v(TAG, "reloadDataFromNetwork");

        // Test network connection before accessing network
        if (!Utils.isNetworkAvailable(this)) {
            Toast.makeText(this,
                    getString(R.string.toast_no_network_available),
                    Toast.LENGTH_LONG).show();

            // Abort
            Log.e(TAG, "No network connection, aborting");
            return;
        }

        if (!silent) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.toast_reloading_data_from_network),
                    Toast.LENGTH_SHORT).show();
        }

        // Generate full location path name for the weather service
        String locationLong = mData.location.getLongName();
        if (locationLong != null) {
            Log.d(TAG, "Reading weather data for " + locationLong);

            // Check if new weather service is needed
            if (mWeather == null
                    || !mWeather.getLongLocation().equals(locationLong)) {
                mWeather = WeatherServiceAsyncTask
                        .getWeatherService(getApplicationContext(),
                                locationLong, false, false);
            }
        }

        // Cancel any previous run tasks to avoid duplicate tasks
        if (mWeatherServiceTask != null) {
            boolean wasCancelled = mWeatherServiceTask.cancel(true);
            if (wasCancelled)
                Log.d(TAG, "WeatherServiceTask was cancelled successfully");
        }

        // AsyncTask is used to read weather data
        enableProgressIndicator(true);
        mWeatherServiceTask = new WeatherServiceAsyncTask(
                new WeatherServiceCallback() {

                    @Override
                    public void setForecastData(ForecastData data) {

                        mData.weatherForecast = data;

                        mData.weatherCredits = mWeather
                                .getCredits(CreditsFormat.shortText);
                    }

                    @Override
                    public void onSuccess() {
                        enableProgressIndicator(false);

                        // Calculate and update all data
                        calculateData();
                    }

                    @Override
                    public void onFailure() {
                        enableProgressIndicator(false);

                        // Invalidate timer data
                        mData.timerData.reset();
                        mData.departureForecast = new Forecast(new Temperature(
                                Temperature.ERR_VALUE));
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.toast_error_parsing_weather_data),
                                Toast.LENGTH_LONG).show();
                    }

                }).execute(mWeather);

    }

    private boolean isLiveTemperatureUpdateEnabled() {
        return false;
    }

    /**
     * Calculate duration for current departureForecast and timer data
     */
    private int calculateDuration() {
        return HeatOptimizer
                .optimalDuration(mData.heater,
                        mData.departureForecast.temperature,
                        mData.timerData.getTimerPeriod());

    }

    /**
     * Update current departureForecast to UI
     */
    private void updateTemperatureToUI() {

        if (mData.departureForecast != null) {

            // Temperature digit
            final String temperatureText = mData.departureForecast.temperature
                    .toScaledString();
            mTemperatureTextView.setText(temperatureText);

            // Credits
            if (mData.weatherCredits != null) {
                if (mWeatherCreditsTextView != null) {
                    mWeatherCreditsTextView.setText(Html
                            .fromHtml(mData.weatherCredits));
                    mWeatherCreditsTextView.setSelected(true);
                    mWeatherCreditsTextView.setVisibility(View.VISIBLE);
                }
            } else {
                if (mWeatherCreditsTextView != null)
                    mWeatherCreditsTextView.setVisibility(View.INVISIBLE);
            }

            // Weather symbol
            int weatherSymbolResource = getWeatherSymbolResource(mData.departureForecast.symbol);
            if (mWeatherSymboImageView != null)
                if (weatherSymbolResource > 0) {
                    mWeatherSymboImageView
                            .setImageResource(weatherSymbolResource);
                    mWeatherSymboImageView.setVisibility(View.VISIBLE);
                } else {
                    mWeatherSymboImageView.setVisibility(View.INVISIBLE);
                }

        }
    }

    /**
     * Update new start time on digital display
     */
    private void updateStartTimeToDigital() {
        if (mData.timerData.isValid()) {

            // Reset text color first
            mStartTimeEditText.setTextColor(getResources()
                    .getColor(android.R.color.primary_text_dark_nodisable));

            if (mData.timerData.getDuration() > 0) {

                // Check if start time is too early
                if (!mData.timerData.isStartTimeInFuture()) {

                    mStartTimeEditText.setTextColor(getResources()
                            .getColor(R.color.error_value));
                }

                // Show time
                String startTimeText = mData.timerData.getStartTime()
                        .toString();
                mStartTimeEditText.setText(startTimeText);

            } else {
                // No valid time
                mStartTimeEditText.setText(getString(R.string.empty_time));
            }
        }

    }

    /**
     * Update new start time on digital display and update today/tomorrow
     * indicator
     */
    private void updateDepartureTimeToDigital() {

        if (mData.timerData.isValid()) {
            // Departure time is shown regardless of the duration
            SimpleTime readyTime = mData.timerData.getReadyTime();
            if (mDepartureTimeButton != null)
                mDepartureTimeButton.setText(readyTime.toString());
            if (readyTime.isToday())
                if (mTodayToggleTextView != null)
                    mTodayToggleTextView.setText(getString(R.string.time_today));
                else if (mTodayToggleTextView != null)
                    mTodayToggleTextView.setText(getString(R.string.time_tomorrow));
        }
    }

    /**
     * Update new stop time on digital display and minutes from now
     */
    private void updateStopTimeToDigital() {

        if (mData.timerData.isValid()) {
            if (mData.timerData.getDuration() > 0) {
                mStopTimeEditText.setText(mData.timerData.getStopTime()
                        .toString());
                SimpleTime t = mData.timerData.getStopTimeFromNow();
                mStopTimeFromNowTextView.setText(t.toString());
            } else {
                mStopTimeEditText.setText(getString(R.string.empty_time));
                mStopTimeFromNowTextView.setText(R.string.empty_time);
            }
        }
    }

    /**
     * Update duration
     */
    private void updateDurationToUI() {
        final String durationText = mData.timerData.getDurationHoursMinString();
        mDurationTextView.setText(durationText);
        if (mDepartureTime != null)
            mDepartureTime.setDurationMinutes(mData.timerData.getDuration());
    }

    /**
     * Update all time data on UI
     */
    private void updateTimeDataToUI() {
        Log.v(TAG, "updateTimeDataToUI");

        if (!mData.timerData.isValid())
            return;

        if (mDepartureTime != null)
            mDepartureTime.setUserTimeData(mData.timerData);

        updateDurationToUI();
        updateStartTimeToDigital();
        updateDepartureTimeToDigital();
        updateStopTimeToDigital();

    }

    /**
     * New location set.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void updateLocationToUI() {
        Log.v(TAG, "updateLocationToUI");

        // Location may still be empty on the first start
        if (mData.location != null) {
            String locS = mData.location.getName();

            if (locS != null) {
                if (android.os.Build.VERSION.SDK_INT >= 11) {
                    ActionBar bar = getSupportActionBar();
                    if (bar != null) {
                        bar.setTitle(locS);
                    }
                } else {
                    setTitle(locS);
                }
            }
        }
    }

    private void showLocationSelectionList() {
        Log.v(TAG, "showLocationSelectionList");
        Intent i = new Intent(getApplicationContext(),
                com.solipaste.carweather.app.LocationsActivity.class);
        startActivityForResult(i,
                IntentRequestCode.LOCATION_ACTIVITY.getValue());
    }

    private void showAboutDialog() {
        Log.v(TAG, "showAboutDialog");

        FragmentManager fm = getSupportFragmentManager();
        AboutDialog aboutDialog = new AboutDialog();
        aboutDialog.show(fm, AboutDialog.FRAGMENT_TAG_ABOUTDIALOG);
    }

    @Override
    public void onAboutDialogClose() {
        // Nothing for now
    }

    @Override
    public void onAboutDialogCloseWithShowHelp() {
        showHelpDialog();
    }

    /**
     * Ensures that welcome is shown only once
     */
    private void showFirstStart() {
        Log.v(TAG, "showFirstStart");
        if (mIsWelcomeShown) {
            showLocationSelectionList();
        } else {
            mIsWelcomeShown = true;
            showWelcomeDialog();
        }
    }

    private void showWelcomeDialog() {
        Log.v(TAG, "showWelcomeDialog");

        // use weak reference to avoid state loss issues
        if (mWrActivity != null && mWrActivity.get() != null
                && mWrActivity.get().isFinishing() != true) {
            FragmentManager fm = mWrActivity.get().getSupportFragmentManager();

            if (mHelpWelcomeDialog == null
                    || mHelpWelcomeDialog.getDialog() == null) {
                mHelpWelcomeDialog = new HelpDialog();
                mHelpWelcomeDialog.show(fm,
                        HelpDialog.FRAGMENT_TAG_WELCOMEDIALOG);
            } else {
                Log.d(TAG, "Welcome dialog already visible");
            }
        }
    }

    private void showHelpDialog() {
        Log.v(TAG, "showHelpDialog");

        FragmentManager fm = getSupportFragmentManager();
        if (mHelpWelcomeDialog == null
                || mHelpWelcomeDialog.getDialog() == null) {
            mHelpWelcomeDialog = new HelpDialog();
            mHelpWelcomeDialog.show(fm, HelpDialog.FRAGMENT_TAG_HELPDIALOG);
        } else {
            Log.d(TAG, "Help dialog already visible");
        }

    }

    @Override
    public void onHelpDialogClose(String dialogType) {
        if (dialogType != null
                && dialogType.equals(HelpDialog.FRAGMENT_TAG_WELCOMEDIALOG)) {
            // First time routine
            showLocationSelectionList();
        }
    }

    @Override
    public void onWelcomeDialogCloseWithAbout() {
        showAboutDialog();
    }

    public static Heater readHeaterTypeFromPrefs(Context context,
                                                 SharedPreferences prefs) {
        String value = prefs.getString(context
                .getString(R.string.pref_heater_type_key), context
                .getString(R.string.pref_heater_radiator_value)); // default
        Heater.HeaterType type = HeaterType.RADIATOR; // the default as
        // defined in
        // prefs.xml
        if (value.equals(context.getString(R.string.pref_heater_block_value))) {
            type = HeaterType.BLOCK;
        } else if (value.equals(context
                .getString(R.string.pref_heater_radiator_value))) {
            type = HeaterType.RADIATOR;
        }
        return new Heater(type);
    }

    /**
     * @param prefs
     * @return
     */
    public static TimerData getTimerDataFromPrefs(Context context,
                                                  SharedPreferences prefs) {
        TimerData data = new TimerData();

        String departureTimeHourString = prefs.getString(context
                .getString(R.string.pref_departure_time_hour_key), context
                .getString(R.string.pref_departure_time_hour_default));
        String departureTimeMinString = prefs.getString(context
                .getString(R.string.pref_departure_time_min_key), context
                .getString(R.string.pref_departure_time_min_default));

        int hour = 0;
        int min = 0;
        try {
            hour = Integer.valueOf(departureTimeHourString);
            min = Integer.valueOf(departureTimeMinString);
        } catch (NumberFormatException e) {
            // use defaults set above;
        }
        data.setReadyTime(new SimpleTime(hour, min));

        // max timer duration
        // ///////////////////
        String maxDurS = prefs.getString(context
                .getString(R.string.pref_timer_maxduration_key), context
                .getString(R.string.pref_timer_maxduration_default));

        int maxTimerDuration = 0;
        try {
            // Valid unless prefs hacked manually
            maxTimerDuration = Integer.parseInt(maxDurS);
        } catch (NumberFormatException e) {
            maxTimerDuration = TimerData.TIMER_PERIOD_DEFAULT;
        }

        if (maxTimerDuration > 0) {
            data.setTimerPeriod(maxTimerDuration);
        }

        return data;
    }

    private int getWeatherSymbolResource(WeatherSymbol symbol) {
        int resource = -1;
        switch (symbol) {
            case SNOW:
            case SLEET:
                resource = R.drawable.snow_indicator;
                break;
            default:
                resource = -1;
                break;
        }
        return resource;
    }

}