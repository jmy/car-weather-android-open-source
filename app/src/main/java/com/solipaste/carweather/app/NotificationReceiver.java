/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.commonsware.cwac.wakeful.WakefulIntentService;

/**
 * Receiver reacting on timed intent to show notification
 * 
 */
public class NotificationReceiver extends BroadcastReceiver {

    private final static String TAG = "NotificationReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG, "onReceive: " + intent);

        // Create new intent to start the service
        Intent serviceIntent = new Intent(context, NotificationService.class);

        // Copy action from the received intent
        serviceIntent.setAction(intent.getAction());

        /*
         * Any component that wants to send work to a WakefulIntentService
         * subclass needs to call either:
         * 
         * WakefulIntentService.sendWakefulWork(context, MyService.class);
         * 
         * (where MyService.class is the WakefulIntentService subclass)
         * 
         * or:
         * 
         * WakefulIntentService.sendWakefulWork(context, intentOfWork);
         * 
         * (where intentOfWork is an Intent that will be used to call
         * startService() on your WakefulIntentService subclass)
         * 
         * Implementations of WakefulIntentService must override doWakefulWork()
         * instead of onHandleIntent(). doWakefulWork() will be processed within
         * the bounds of a WakeLock. Otherwise, the semantics of doWakefulWork()
         * are identical to onHandleIntent(). doWakefulWork() will be passed the
         * Intent supplied to sendWakefulWork() (or an Intent created by the
         * sendWakefulWork() method, depending on which flavor of that method
         * you use).
         * 
         * And that's it. WakefulIntentService handles the rest.
         * 
         * NOTE: this only works with local services. You have no means of
         * accessing the static WakeLock of a remote service.
         * 
         * NOTE #2: Your application must hold the WAKE_LOCK permission.
         * 
         * NOTE #3: If you get an "WakeLock under-locked" exception, make sure
         * that you are not starting your service by some means other than
         * sendWakefulWork().
         */

        // GO!
        WakefulIntentService.sendWakefulWork(context, serviceIntent);
    }

}
