/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;

import com.commonsware.cwac.wakeful.WakefulIntentService;
import com.google.android.apps.analytics.easytracking.EasyTracker;
import com.solipaste.carweather.domain.ForecastData;
import com.solipaste.carweather.domain.HeatOptimizer;
import com.solipaste.carweather.domain.Heater;
import com.solipaste.carweather.domain.Heater.HeaterType;
import com.solipaste.carweather.domain.SimpleTime;
import com.solipaste.carweather.domain.Temperature;
import com.solipaste.carweather.domain.TimerData;
import com.solipaste.carweather.service.WeatherService;
import com.solipaste.carweather.service.WeatherServiceAsyncTask;

import java.util.Calendar;
import java.util.Date;

public class NotificationService extends WakefulIntentService {
    private final static String TAG = NotificationService.class.getName();

    private static final int MINUTES_IN_HOUR = 60;

    // Unique id
    private static final int TIMING_REMINDER_NOTIFICATION_ID = 0;
    private static final int DEPARTURE_REMINDER_NOTIFICATION_ID = 1;

    private SharedPreferences mSharedPrefs;
    private boolean mTimerNotificationsEnabled;
    private boolean mDepartureNotificationsEnabled;
    private boolean mNotifyOnWeekdaysOnly;
    private String mCurrentLocationLong;

    private Heater mHeater;
    private TimerData mTimerData;
    private boolean mUsingFreshTemperatureData = false;

    /**
     * @param name
     */
    public NotificationService() {
        super("NotificationService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "onCreate");
        mHeater = new Heater(HeaterType.RADIATOR);

        // Initialize prefs
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this
                .getApplicationContext());

        updateFromPreferences();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "onDestroy"); // Just for logging
    }

    /**
     *
     */
    private void updateFromPreferences() {

        // Timing Notification enabled
        // ///////////
        mTimerNotificationsEnabled = mSharedPrefs
                .getBoolean(getString(R.string.pref_notification_timing_enabled_key),
                        false);

        // Timing Notification enabled
        // ///////////
        mDepartureNotificationsEnabled = mSharedPrefs
                .getBoolean(getString(R.string.pref_notification_departure_enabled_key),
                        false);

        // Time data
        // ///////////
        mTimerData = MainActivity.getTimerDataFromPrefs(this, mSharedPrefs);

        // Heater type
        mHeater = MainActivity.readHeaterTypeFromPrefs(this, mSharedPrefs);

        // Location
        // /////////
        mCurrentLocationLong = mSharedPrefs
                .getString(getString(R.string.pref_location_long_key), "");

        // Weekday only
        // /////////
        mNotifyOnWeekdaysOnly = mSharedPrefs
                .getBoolean(getString(R.string.pref_notification_weekdays_only_key),
                        true);

    }

    @Override
    protected void doWakefulWork(Intent intent) {

        if (intent != null) {

            String action = intent.getAction();
            int notificationType = -1;
            // show notification of correct type
            if (action != null) {
                if (action
                        .equals(getString(R.string.action_notify_heating_timing)))
                    notificationType = TIMING_REMINDER_NOTIFICATION_ID;

                else if (action
                        .equals(getString(R.string.action_notify_departure)))
                    notificationType = DEPARTURE_REMINDER_NOTIFICATION_ID;
            }

            // All alarms are time-time alarms so recreate the next alarm
            rescheduleNotification(notificationType);

            if (notificationType != -1)
                initiateNotification(notificationType);
            else
                Log.e(TAG, "Invalid notification type");
        }

    }

    /**
     * Reschedules the notification to occur the next time.
     * @param notificationType
     */
    private void rescheduleNotification(int notificationType) {
        // show notification of correct type
        switch (notificationType) {
            case TIMING_REMINDER_NOTIFICATION_ID:
                if (mTimerNotificationsEnabled) {
                    // Enabled - reschedule notifications
                    String timeS = mSharedPrefs
                            .getString(getBaseContext().getString(R.string.pref_notification_timing_time_key),
                                    getBaseContext().getString(R.string.pref_notification_timing_time_default));
                    if (timeS != null) {
                        SimpleTime notifTime = new SimpleTime(timeS);
                        PreferencesActivity.updateTimingNotification(getBaseContext(), true,
                                notifTime);
                    }
                }
                break;
            case DEPARTURE_REMINDER_NOTIFICATION_ID:
                if (mDepartureNotificationsEnabled) {
                    // Enabled - reschedule notifications
                    String useExistingLeadTime = null;
                    SimpleTime useExistingDepartureTime = null;
                    PreferencesActivity
                            .updateDepartureNotification(getBaseContext(), true,
                                    useExistingDepartureTime,
                                    useExistingLeadTime);
                }
                break;
            default:
                Log.e(TAG, "Invalid notification type");
        }

    }

    /**
     * Calculate heating duration based on location, weather and heater
     *
     * @return
     */
    private void initiateNotification(int notificationType) {
        Log.v(TAG, "initiateNotification");

        // Preconditions
        if (mCurrentLocationLong == null
                || this.mCurrentLocationLong.length() == 0) {
            Log.e(TAG, "Location missing");
            return;
        }

        Log.d(TAG, "longLocation: " + mCurrentLocationLong);

        if (mTimerData == null || !mTimerData.isValid()) {
            Log.e(TAG, "Time data missing");
            return;
        }

        // Initialize all data from network
        if (initializeTimerDataFromNetwork(notificationType)) {
            Log.i(TAG,
                    "Data from network or cache available. Showing notification.");

            // show notification of correct type
            switch (notificationType) {
                case TIMING_REMINDER_NOTIFICATION_ID:
                    handleTimingNotification(mTimerData);
                    break;
                case DEPARTURE_REMINDER_NOTIFICATION_ID:
                    handleDepartureNotification(mTimerData);
                    break;
                default:
                    Log.e(TAG, "Invalid notification type");
            }

            // Clear any old failures
            clearFailedNotificationRecords();

        } else {
            Log.e(TAG,
                    "Reading fresh data from network or cache failed. Recorded failure on the prefs. Skipping notification!");

            // Failure
            recordFailedNotification(notificationType);

            EasyTracker
                    .getTracker()
                    .trackEvent(getString(R.string.analytics_event_notifications_category),
                            getString(R.string.analytics_event_notifications_weatherservicefailure_action),
                            null, notificationType);

        }
    }

    /**
     * Read weather forecast from the service. First check the network
     * connectivity, retrying few times.
     *
     * @param notificationType
     * @return {@code true} if weather data was read successfully, {@code false}
     * otherwise
     */
    private boolean initializeTimerDataFromNetwork(int notificationType) {
        Log.v(TAG, "initializeTimerDataFromNetwork");

        boolean success = false;

        // Get suitable weather service with background service and fallback
        // enabled
        final boolean fallbackToCachedData = true;
        final boolean backgroundUsage = true;

        WeatherService weather = WeatherServiceAsyncTask
                .getWeatherService(this, mCurrentLocationLong, backgroundUsage,
                        fallbackToCachedData);

        // Get weather forecast data from the service
        ForecastData temperatureForecast = weather.getForecastData();

        // Store info whether we are using fresh or cached data
        mUsingFreshTemperatureData = temperatureForecast.isFreshData();

        // Check that the data is valid (do not check for freshness)
        if (temperatureForecast.isValidData()) {

            Temperature temperature = null;
            switch (notificationType) {
                case TIMING_REMINDER_NOTIFICATION_ID:
                    // Get temperature for ready(!) time
                    final Date date = mTimerData.getReadyTime().getDate();
                    temperature = temperatureForecast.getTemperature(date);
                    break;
                case DEPARTURE_REMINDER_NOTIFICATION_ID:
                    // Get temperature for now
                    temperature = temperatureForecast.getTemperature();
                    break;
            }

            // Calculate optimal heating duration
            if (temperature != null && temperature.isValid()) {
                Log.d(TAG, "Temp: " + temperature);
                int duration = HeatOptimizer
                        .optimalDuration(mHeater, temperature,
                                mTimerData.getTimerPeriod());
                mTimerData.setDuration(duration);
                Log.d(TAG, "Duration: " + duration);
                success = true;
            }
        }
        return success;
    }

    private void handleTimingNotification(TimerData data) {
        Log.v(TAG, "handleTimingNotification");

        if (!mTimerNotificationsEnabled)
            return;

        boolean notifyToday = true;

        if (mNotifyOnWeekdaysOnly) {
            // Skip if the next departure time occurs on weekend
            SimpleTime readyTime = data.getReadyTime();
            Date readyDate = readyTime.getDate(); // returns the following day
            // if time is in the past
            notifyToday = !isDateOnWeekend(readyDate);
        }

        if (notifyToday)

            if (data.getDuration() > 0)
                showTimingNotification();
            else
                Log.d(TAG,
                        "Timing notification skipped due low heating duration");
        else
            Log.d(TAG, "Timing notification skipped due weekend");

    }

    private void handleDepartureNotification(TimerData data) {
        Log.v(TAG, "handleDepartureNotification");

        if (!mDepartureNotificationsEnabled)
            return;

        boolean notifyToday = true;

        if (mNotifyOnWeekdaysOnly) {
            // Skip if today is weekend
            Date now = new Date();
            notifyToday = !isDateOnWeekend(now);
        }

        if (notifyToday)
            if (data.getDuration() > 0)
                showDepartureNotification();
            else
                Log.d(TAG,
                        "Departure notification skipped due low heating duration");
        else
            Log.d(TAG, "Departure notification skipped due weekend");
    }

    private boolean isDateOnWeekend(Date readyDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(readyDate);
        int weekday = cal.get(Calendar.DAY_OF_WEEK);
        boolean isWeekend = false;
        if (weekday == Calendar.SATURDAY || weekday == Calendar.SUNDAY) {
            isWeekend = true;
        }
        return isWeekend;
    }

    /**
     * Show notification
     */
    private void showTimingNotification() {
        Log.v(TAG, "showTimingNotification");

        // PendingIntent to launch activity if the user selects
        // the notification
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra(getString(R.string.extra_notification_selected), true);

        PendingIntent detailsIntent = PendingIntent.getActivity(this, 0, i, 0);

        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Generate text with heating time in hours and minutes
        String titleText = "";
        int durationHours = 0;
        int durationMinutes = mTimerData.getDuration();
        if (durationMinutes >= MINUTES_IN_HOUR) {
            durationHours = durationMinutes / MINUTES_IN_HOUR;
            durationMinutes -= durationHours * MINUTES_IN_HOUR;
        }

        if (durationHours == 0) {
            titleText = getString(R.string.notification_timing_content_title_0_hour_with_minutes_format,
                    durationHours, durationMinutes);

        } else if (durationHours == 1) {
            if (durationMinutes == 0)
                titleText = getString(R.string.notification_timing_content_title_1_hour,
                        durationHours, durationMinutes);
            else
                titleText = getString(R.string.notification_timing_content_title_1_hour_with_minutes_format,
                        durationHours, durationMinutes);

        } else {
            if (durationMinutes == 0)

                titleText = getString(R.string.notification_timing_content_title_no_minutes_format,
                        durationHours, durationMinutes);
            else
                titleText = getString(R.string.notification_timing_content_title_with_minutes_format,
                        durationHours, durationMinutes);
        }

        // Content text will depend on the freshness of the data
        String contentText = "";
        if (mUsingFreshTemperatureData) {
            contentText = getString(R.string.notification_timing_content_text_format,
                    mTimerData.getStopTime().toString());
        } else {
            contentText = getString(R.string.notification_timing_content_text_cached_data);
        }

        Builder builder = new NotificationCompat.Builder(this);
        Notification notif = builder
                .setDefaults(Notification.DEFAULT_SOUND
                        | Notification.DEFAULT_LIGHTS)
                .setSmallIcon(R.drawable.ic_stat_notification)
                .setTicker(getString(R.string.notification_timing_ticker))
                .setContentTitle(titleText).setContentText(contentText)
                .setContentIntent(detailsIntent).setAutoCancel(true).build();

        if (nm != null && notif != null) {
            nm.notify(TIMING_REMINDER_NOTIFICATION_ID, notif);

            EasyTracker
                    .getTracker()
                    .trackEvent(getString(R.string.analytics_event_notifications_category),
                            getString(R.string.analytics_event_notifications_heaterrequiredtriggered_action),
                            "", mTimerData.getDuration());
        } else {
            Log.e(TAG,
                    "Something went wrong during timing notification construction");
        }
    }

    /**
     * Show notification
     */
    private void showDepartureNotification() {
        Log.v(TAG, "showDepartureNotification");

        // PendingIntent to launch activity if the user selects
        // the notification
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra(getString(R.string.extra_notification_selected), true);

        PendingIntent detailsIntent = PendingIntent.getActivity(this, 0, i, 0);

        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Construct title text for the notification
        String titleText = "";
        final int minutesFromNow = mTimerData.getReadyTime().minutesFromNow();
        if (minutesFromNow > 0)
            titleText = String
                    .format((getString(R.string.notification_departure_content_title_format)),
                            minutesFromNow);
        else
            titleText = getString(R.string.notification_departure_content_title_now);

        // Construct content text for the notification
        String contentText = String
                .format((getString(R.string.notification_departure_content_text_format)),
                        mTimerData.getReadyTime().toString());

        Builder builder = new NotificationCompat.Builder(this);
        Notification notif = builder
                .setDefaults(Notification.DEFAULT_SOUND
                        | Notification.DEFAULT_LIGHTS)
                .setSmallIcon(R.drawable.ic_stat_notification)
                .setTicker(getString(R.string.notification_departure_ticker))
                .setContentTitle(titleText).setContentText(contentText)
                .setContentIntent(detailsIntent).setAutoCancel(true).build();

        if (nm != null && notif != null) {
            nm.notify(DEPARTURE_REMINDER_NOTIFICATION_ID, notif);

            EasyTracker
                    .getTracker()
                    .trackEvent(getString(R.string.analytics_event_notifications_category),
                            getString(R.string.analytics_event_notifications_departuretimetriggered_action),
                            "", 0);
        } else {
            Log.e(TAG,
                    "Something went wrong during timing notification construction");
        }
    }

    private void recordFailedNotification(int notificationType) {
        Log.v(TAG, "recordFailedNotification: " + notificationType);

        Editor editor = mSharedPrefs.edit();
        Date now = new Date();
        editor.putString(getString(R.string.pref_notification_failed_time_key),
                String.valueOf(now.getTime()));
        editor.commit();
    }

    private void clearFailedNotificationRecords() {
        Log.v(TAG, "clearFailedNotificationRecords");

        Editor editor = mSharedPrefs.edit();
        editor.remove(getString(R.string.pref_notification_failed_time_key));
        editor.commit();
    }

    /**
     * Create intent that will trigger notification
     *
     * @return
     */
    static public Intent getTimingNotificationIntent(Context c) {
        Intent intent = new Intent(
                c.getString(R.string.action_notify_heating_timing));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_FROM_BACKGROUND);
        return intent;
    }

    /**
     * Create intent that will trigger notification
     *
     * @return
     */
    static public Intent getDepartureNotificationIntent(Context c) {
        Intent intent = new Intent(
                c.getString(R.string.action_notify_departure));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_FROM_BACKGROUND);
        return intent;
    }
}
