/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;

import com.google.android.apps.analytics.easytracking.EasyTracker;
import com.solipaste.carweather.domain.SimpleTime;
import com.solipaste.carweather.domain.TimerData;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PreferencesActivity extends PreferenceActivity {

    private final static String TAG = PreferencesActivity.class.getName();

    private TimePreference mTimingNotifTimePref;
    private CheckBoxPreference mTimingNotifEnabedPref;
    private CheckBoxPreference mDepartureNotifEnabledPref;
    private CheckBoxPreference mWeekdayOnlyNotifPref;
    private AutoSummaryListPreference mDepartureNotifLeadPref;

    private String mTodayString;
    private String mTomorrowString;

    private static final int DEPARTURE_NOTIFICATION_LEAD_DEFAULT_MIN = 15;

    private final OnPreferenceChangeListener mTimingPreferenceListener = new OnPreferenceChangeListener()
    {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if (preference
                    .getKey()
                    .equals(getString(R.string.pref_notification_timing_enabled_key))) {
                // null indicates that time was not changed
                updateTimingNotification((Boolean) newValue, null);
            } else if (preference
                    .getKey()
                    .equals(getString(R.string.pref_notification_timing_time_key))) {
                // timing notification were enabled if time settings was enabled
                updateTimingNotification(true, (SimpleTime) newValue);
            }

            updateTimingNotifTimeSummary(preference, newValue);

            // true ensures that data is persisted
            return true;
        }
    };

    private final OnPreferenceChangeListener mDeparturePreferenceListener = new OnPreferenceChangeListener()
    {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if (preference
                    .getKey()
                    .equals(getString(R.string.pref_notification_departure_enabled_key))) {
                // null indicates that time was not changed
                updateDepartureNotification((Boolean) newValue, null);
            } else if (preference
                    .getKey()
                    .equals(getString(R.string.pref_notification_departure_lead_key))) {
                // timing notification were enabled if time settings was enabled
                updateDepartureNotification(true, (String) newValue);
            }

            // true ensures that data is persisted
            return true;
        }
    };

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        EasyTracker.getTracker().setContext(this);

        addPreferencesFromResource(R.xml.preferences_main);

        // Remove hidden preference category
        PreferenceCategory pref = (PreferenceCategory) findPreference(getString(R.string.pref_hidden_category));
        if (pref != null)
            getPreferenceScreen().removePreference(pref);

        // initialize preferences that are used to schedule notifications
        mTimingNotifEnabedPref = (CheckBoxPreference) findPreference(getString(R.string.pref_notification_timing_enabled_key));
        mTimingNotifEnabedPref
                .setOnPreferenceChangeListener(mTimingPreferenceListener);
        mTimingNotifTimePref = (TimePreference) findPreference(getString(R.string.pref_notification_timing_time_key));
        mTimingNotifTimePref
                .setOnPreferenceChangeListener(mTimingPreferenceListener);

        mWeekdayOnlyNotifPref = (CheckBoxPreference) findPreference(getString(R.string.pref_notification_weekdays_only_key));
        mWeekdayOnlyNotifPref
                .setOnPreferenceChangeListener(mTimingPreferenceListener);

        mDepartureNotifEnabledPref = (CheckBoxPreference) findPreference(getString(R.string.pref_notification_departure_enabled_key));
        mDepartureNotifEnabledPref
                .setOnPreferenceChangeListener(mDeparturePreferenceListener);

        mDepartureNotifLeadPref = (AutoSummaryListPreference) findPreference(getString(R.string.pref_notification_departure_lead_key));
        mDepartureNotifLeadPref
                .setOnPreferenceChangeListener(mDeparturePreferenceListener);

        mTodayString = getString(R.string.today_as_part_of_sentence);
        mTomorrowString = getString(R.string.tomorrow_as_part_of_sentence);

        // Intial summary
        updateTimingNotifTimeSummary(null, null);

    }

    @SuppressLint("SimpleDateFormat")
    protected void updateTimingNotifTimeSummary(Preference preference,
            Object newValue) {
        Log.v(TAG, "updateTimingNotifTimeSummary");

        // Check if time was edited
        SimpleTime time = null;
        if (preference != null
                && preference
                        .getKey()
                        .equals(getString(R.string.pref_notification_timing_time_key)))
            time = (SimpleTime) newValue;
        else
            time = mTimingNotifTimePref.getTime();

        // Check if weekday restriction was edited
        boolean weekdaysOnly = false;
        if (preference != null
                && preference
                        .getKey()
                        .equals(getString(R.string.pref_notification_weekdays_only_key)))
            weekdaysOnly = (Boolean) newValue;
        else
            weekdaysOnly = mWeekdayOnlyNotifPref.isChecked();

        String dateString = null;

        // Use original defined summary as a format string
        Calendar nextNotificationCal = Calendar.getInstance();
        nextNotificationCal.setTime(time.getDate());
        boolean isWeekend = isWeekend(nextNotificationCal);
        Calendar todayCal = Calendar.getInstance();

        if (weekdaysOnly && isWeekend) {
            // Set the first day after weekend
            nextNotificationCal.add(Calendar.WEEK_OF_YEAR, 1);
            nextNotificationCal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        }

        String summaryFormat = null;
        if (sameDay(nextNotificationCal, todayCal)) {
            // Today
            dateString = mTodayString;
            summaryFormat = getString(R.string.notification_timing_time_summary_format);
        } else if (isToday(nextNotificationCal)) {
            // Tomorrow
            dateString = mTomorrowString;
            summaryFormat = getString(R.string.notification_timing_time_summary_format);
        } else {
            summaryFormat = getString(R.string.notification_timing_time_summary_weekday_format);
            // Some other day
            dateString = new SimpleDateFormat("EEEE")
                    .format(nextNotificationCal.getTime());
        }

        mTimingNotifTimePref.setSummary(String.format(summaryFormat,
                                                      dateString,
                                                      time.toString()));
    }

    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getTracker().trackActivityStart(this);
    }

    @Override
    protected void onStop() {
        Log.v(TAG, "onStop");
        super.onStop();
        setResult(RESULT_OK);

        EasyTracker.getTracker().trackActivityStop(this);
    }

    @SuppressWarnings("deprecation")
    @Override
    public Object onRetainNonConfigurationInstance() {
        Object o = super.onRetainNonConfigurationInstance();
        EasyTracker.getTracker().trackActivityRetainNonConfigurationInstance();
        return o;

    }

    private void updateTimingNotification(boolean wasEnabled, SimpleTime newTime) {

        String persistedTime = null;

        // We need to handle case where notifications are enabled for the first
        // time and default time is shown but not yet set by user

        if (newTime == null) {
            // Notification was enabled - check if user has set time previously
            persistedTime = mTimingNotifTimePref
                    .getSharedPreferences()
                    .getString(getString(R.string.pref_notification_timing_time_key),
                               null);

            if (persistedTime == null) {
                // Show time dialog because no time yet set
                // Continue setting notification with default time. When user
                // defines correct time then also the notification gets updated.
                // Note that use can cancel the dialog in which case
                // notification at default time stays active
                Log.d(TAG, "No notif time set yet - showing dialog");
                mTimingNotifTimePref.show();
            }
        }

        // Maybe using default time here
        SimpleTime notifTime = (newTime == null) ? mTimingNotifTimePref
                .getTime() : newTime;

        updateTimingNotification(getBaseContext(), wasEnabled, notifTime);
    }

    private void updateDepartureNotification(boolean wasEnabled, String newLead) {

        // null depart time will be read from prefs
        updateDepartureNotification(getBaseContext(), wasEnabled, null, newLead);
    }

    /**
     * Schedule timing notification
     * 
     * @param context
     * @param wasEnabled
     */
    @SuppressLint("DefaultLocale")
    static void updateTimingNotification(Context context, boolean wasEnabled,
            SimpleTime notifTime) {
        Log.v(TAG, "updateTimingNotification: " + wasEnabled + ", " + notifTime);

        // Construct intent
        Intent intent = NotificationService
                .getTimingNotificationIntent(context);

        // Set alarm
        setOrCancelAlarm(context, wasEnabled, intent, notifTime);

    }

    /**
     * Schedule departure notification
     * 
     * @param context
     * @param wasEnabled
     * @param departureTime
     * @param newLead
     */
    static void updateDepartureNotification(Context context,
            boolean wasEnabled, SimpleTime departureTime, String newLead) {
        Log.v(TAG, "updateDepartureNotification: " + wasEnabled + ", "
                + departureTime + ", "
                + (newLead != null ? newLead : "[old lead]"));

        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());

        // Ensure that departure time exists
        if (departureTime == null) {
            TimerData timerData = MainActivity
                    .getTimerDataFromPrefs(context, sharedPrefs);
            departureTime = timerData.getReadyTime();
        }

        // Ensure that lead time exists
        int leadMinInt = DEPARTURE_NOTIFICATION_LEAD_DEFAULT_MIN;
        String leadMinString = newLead;
        if (leadMinString == null) {
            // Was enable - fetch the old lead time value

            leadMinString = sharedPrefs
                    .getString(context.getString(R.string.pref_notification_departure_lead_key),
                               context.getString(R.string.pref_notification_departure_lead_default));
            Log.d(TAG, "Old lead time: " + leadMinString);
        }

        // Convert lead value to integer
        try {
            leadMinInt = Integer.parseInt(leadMinString);
        } catch (NumberFormatException e) {
            // use initial value
        }

        // Construct intent
        Intent intent = NotificationService
                .getDepartureNotificationIntent(context);

        // Substract lead minutes and convert to data that is always in the
        // future
        SimpleTime notifTime = departureTime.subMinutes(leadMinInt);

        // Set alarm
        setOrCancelAlarm(context, wasEnabled, intent, notifTime);
    }

    /**
     * 
     * Set or cancel alarm using Alarm Manager
     * 
     * NOTE that this must be redone after alarm, reboot and after app update!
     * 
     * @param context
     * @param setAlarm
     *            If true then alarm is set, otherwise alarm is cancelled
     * @param intent
     *            Intent to lauch when alarm triggers
     * @param time
     *            When to trigger alarm for the next time
     */
    private static void setOrCancelAlarm(Context context, boolean setAlarm,
            Intent intent, SimpleTime time) {
        Log.v(TAG, String
                .format("setOrCancelAlarm: enabled: %b, time: %s, intent: %s ",
                        setAlarm, time != null ? time.getDate() : "null",
                        intent != null ? intent : "null"));

        // Validate input
        if (context == null || intent == null || time == null)
            return;

        // Construct pending intent that contains the passed intent
        PendingIntent pendingIntent = PendingIntent
                .getBroadcast(context, 0, intent,
                              PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(ALARM_SERVICE);
        if (alarmManager != null) {

            // Either set or cancel repeating alarm at given time
            if (setAlarm && time != null) {

                // Alarm is always in the future
                // Guarantees that alarm with lead time will not retrigger
                // Uses the current TZ from dateLong, if TZ changes then the
                // alarm needs to be
                // recreated using that TZ
                long dateLong = time.getDate().getTime();

                // Set alarm to go off in <dateLong> ms from now
                // Repeating alarm not supported anymore from M onwards
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    // required for the new low-power idle modes
                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, dateLong,
                            pendingIntent);
                } else {
                    alarmManager.set(AlarmManager.RTC_WAKEUP, dateLong,
                            pendingIntent);
                }

            } else {

                // Not really mandatory as service checks if notifications are
                // enabled
                if (pendingIntent != null)
                    alarmManager.cancel(pendingIntent);
            }
        }
    }

    private static boolean sameDay(Calendar calendar, Calendar otherCalendar) {
        return calendar.get(Calendar.DAY_OF_YEAR) == otherCalendar
                .get(Calendar.DAY_OF_YEAR)
                && calendar.get(Calendar.DAY_OF_YEAR) == otherCalendar
                        .get(Calendar.DAY_OF_YEAR);
    }

    private static boolean isToday(Calendar calendar) {
        Calendar tomorrowCal = Calendar.getInstance();
        tomorrowCal.add(Calendar.DAY_OF_YEAR, 1);
        return sameDay(calendar, tomorrowCal);
    }

    private static boolean isWeekend(Calendar cal) {
        int weekday = cal.get(Calendar.DAY_OF_WEEK);
        return (weekday == Calendar.SATURDAY || weekday == Calendar.SUNDAY);
    }
}
