/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.OverScroller;

import com.solipaste.carweather.domain.SimpleTime;
import com.solipaste.carweather.domain.TimerData;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class SliderDepartureTimeView extends View implements DepartureTimeView, GestureDetector.OnGestureListener {
    private final static String TAG = AnalogClockDepartureTimeView.class.getName();

    private static final int ROUND_TOUCH_MINUTE = 5;
    public static final int VISIBLE_HOURS = 12;
    private static final int MINUTES_IN_HOUR = 60;

    private GestureDetectorCompat mDetector;
    private OverScroller mScroller;

    private DepartureTimeView.OnActionListener mOnActionListener;
    private TimerData mUserTimeData;
    private Bitmap mUserHandleBitmapUnselected;
    private Bitmap mUserHandleBitmapSelected;
    private final Paint mUserHandPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Rect mUserHandRect;
    private boolean mIsUserHandSelected = false;
    private int mUserHandStart = 0;

    public SliderDepartureTimeView(Context context) {
        super(context);
    }

    private void init() {

        mDetector = new GestureDetectorCompat(getContext(),this);
        mScroller = new OverScroller(getContext());

        // Init timer data
        mUserTimeData = new TimerData();

        // User handle bitmaps
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inPreferredConfig = Bitmap.Config.ARGB_8888;
        opts.inDither = false;
        mUserHandleBitmapUnselected = BitmapFactory
                .decodeResource(getResources(),
                        R.drawable.slider_departure_time, opts);
        mUserHandleBitmapSelected = BitmapFactory
                .decodeResource(getResources(),
                        R.drawable.slider_departure_time, opts);

        // Using this paint ensures AA
        mUserHandPaint.setAntiAlias(true);
        mUserHandPaint.setFilterBitmap(true);
        mUserHandPaint.setDither(true);

        // Source rect wide enough so that apect ratio stays intact
        mUserHandRect = new Rect(0, 0,
                mUserHandleBitmapUnselected.getWidth(),
                mUserHandleBitmapUnselected.getHeight());


    }

    public SliderDepartureTimeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        init();
    }


    @Override
    public void setOnActionListener(DepartureTimeView.OnActionListener l) {
        mOnActionListener = l;
    }

    @Override
    public void setUserHandTime(SimpleTime time, AnalogClockDepartureTimeView.UserHandType type) {
    }

    @Override
    public int getDurationMinutes() {
        return mUserTimeData.getDuration();
    }

    @Override
    public void setDurationMinutes(int d) {
        mUserTimeData.setDuration(d);
    }

    @Override
    public void setUserTimeData(TimerData data) {
        // take a copy of the timer data to allow avoid time updates during or
        // before animations
        mUserTimeData = new TimerData(data);
        postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.v(TAG, "onDraw start");

        float userHandHourF = getUserHandHourF(mUserTimeData);

        if (mUserTimeData.isValid())
            // Draw user handle on top
            drawUserHand(canvas, userHandHourF);
        else
            Log.e(TAG, "time data invalid, cannot draw user hand");

    }

    private void drawUserHand(Canvas canvas, float userHandHourF) {

        Bitmap userHand;
/*
        if (mIsUserHandSelected)
            userHand = mUserHandleBitmapSelected;
        else
*/
            userHand = mUserHandleBitmapUnselected;

        int h = getMeasuredHeight();
        int w = getMeasuredWidth();
//        Log.v(TAG, String.format("w: %d, h: %d", w, h));

        canvas.drawBitmap(userHand, null, new Rect(mUserHandStart, 0, w + mUserHandStart, h), mUserHandPaint);
        canvas.drawBitmap(userHand, mUserHandRect, new Rect(mUserHandStart - w, 0, mUserHandStart, h), mUserHandPaint);

    }

    private static float getUserHandHourF(TimerData data) {
        float userHandHourF = 0;
        if (data.isValid()) {
            userHandHourF = data.getReadyTime().getHour();
            userHandHourF += ((float) data.getReadyTime().getMinute()) / 60;
        }
        return userHandHourF;
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int w = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        int h = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);

//        Log.v(TAG, String.format("min: w: %d, h: %d", w, h));

        h = mUserHandleBitmapUnselected.getHeight();
//        Log.v(TAG, String.format("measured: w: %d, h: %d", w, h));
        setMeasuredDimension(w, h);
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
//        Log.d(TAG, "computeScroll");

        if (mScroller.computeScrollOffset()) {
            // The scroller isn't finished, meaning a fling or programmatic pan operation is
            // currently active.

            int currX = mScroller.getCurrX();

            updateDepartTime(currX);

            mUserHandStart = currX;
            // Set new ready time

            ViewCompat.postInvalidateOnAnimation(this);
        }

    }

    private void updateDepartTime(int currX) {
        int touchHours = (int) ((float) currX / getMeasuredWidth() * VISIBLE_HOURS); // cut decimals
        int touchMinutes = (int) (((float) currX / getMeasuredWidth() * VISIBLE_HOURS) % 1 * MINUTES_IN_HOUR);

        // Round to nearest ROUND_TOUCH_MINUTE
        touchMinutes = (int) Math.rint((double) touchMinutes
                / ROUND_TOUCH_MINUTE)
                * ROUND_TOUCH_MINUTE;
        if (touchMinutes >= 60) {
            // next hour
            touchHours++;
            touchMinutes = 0;
        }

        // Avoid 24:00
        if (touchHours >= 24) {
            touchHours = 0;
            touchMinutes = 0;
        }

        SimpleTime touchTime = new SimpleTime(touchHours, touchMinutes);

        mUserTimeData.setReadyTime(touchTime);

        if (mOnActionListener != null && mUserTimeData.isValid()) {
            mOnActionListener.onUserHandMoveAction(mUserTimeData
                    .getReadyTime());
        }
//        Log.d(TAG, "New time from touch: " + touchHours + ":" + touchMinutes);
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {

        return this.mDetector.onTouchEvent(event) || super.onTouchEvent(event);

/*
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_UP:

                // Mark handle unselected
                mIsUserHandSelected = false;
                break;

            case MotionEvent.ACTION_DOWN:

                // Mark handle selected
                mIsUserHandSelected = true;

                // redraw unselected handle
                invalidate();
                return true;

            case MotionEvent.ACTION_MOVE:
                // Mark handle selected (probably not needed again)
                mIsUserHandSelected = true;
                break;

            default:
                // Ignore other events
                return super.onTouchEvent(event);
        }

        // Calculate new start
        float pX = event.getX();

        int touchHours = (int) (pX / getMeasuredWidth() * VISIBLE_HOURS); // cut decimals
        int touchMinutes = (int) ((pX / getMeasuredWidth() * VISIBLE_HOURS) % 1 * MINUTES_IN_HOUR);

        // Round to nearest ROUND_TOUCH_MINUTE
        touchMinutes = (int) Math.rint((double) touchMinutes
                / ROUND_TOUCH_MINUTE)
                * ROUND_TOUCH_MINUTE;
        if (touchMinutes >= 60) {
            // next hour
            touchHours++;
            touchMinutes = 0;
        }

        // Avoid 24:00
        if (touchHours >= 24) {
            touchHours = 0;
            touchMinutes = 0;
        }

        SimpleTime touchTime = new SimpleTime(touchHours, touchMinutes);

        mUserHandStart = (int) pX;
        // Set new ready time
        mUserTimeData.setReadyTime(touchTime);
        Log.d(TAG, "New time from touch: " + touchHours + ":" + touchMinutes);

        // Redraw
        invalidate();

        if (action == MotionEvent.ACTION_UP) {

            // Inform activity
            if (mOnActionListener != null && mUserTimeData.isValid()) {
                mOnActionListener.onUserHandReleaseAction(mUserTimeData
                        .getReadyTime());
            }
        } else {
            if (isLiveUpdate())
                // Inform activity
                if (mOnActionListener != null && mUserTimeData.isValid()) {
                    mOnActionListener.onUserHandMoveAction(mUserTimeData
                            .getReadyTime());
                }
        }

        return true;
*/
    }

    public boolean isLiveUpdate() {
        return true;
    }

    @Override
    public boolean onDown(MotionEvent event) {
        Log.d(TAG,"onDown: " + event.toString());
        mScroller.forceFinished(true);
        ViewCompat.postInvalidateOnAnimation(SliderDepartureTimeView.this);
        return true;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {
        Log.d(TAG, "onFling: " + event1.toString() + event2.toString());
        fling((int) velocityX, 0);
        return true;
    }

    private void fling(int velocityX, int velocityY) {
        mScroller.forceFinished(true);
        mScroller.fling(mUserHandStart, 0, velocityX, 0, 0, 10000, getMeasuredWidth(), getMeasuredHeight());
        ViewCompat.postInvalidateOnAnimation(this);

    }

    @Override
    public void onLongPress(MotionEvent event) {
//        Log.d(TAG, "onLongPress: " + event.toString());
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                            float distanceY) {
        Log.d(TAG, "onScroll: " + distanceX);

        mUserHandStart = mUserHandStart - (int) distanceX;
        ViewCompat.postInvalidateOnAnimation(this);
        updateDepartTime(mUserHandStart);

        return true;
    }

    @Override
    public void onShowPress(MotionEvent event) {
//        Log.d(TAG, "onShowPress: " + event.toString());
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
//        Log.d(TAG, "onSingleTapUp: " + event.toString());
        return false;
    }


}