/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.widget.TimePicker;

import com.solipaste.carweather.domain.SimpleTime;

/**
 * Time picker dialog for selecting exact start time
 * 
 */
public class TimePickerFragment extends DialogFragment implements
        TimePickerDialog.OnTimeSetListener {

    private static final String ARG_TYPE = "type";
    private static final String ARG_MINUTE = "minute";
    private static final String ARG_HOUR = "hour";

    private TimePickerType mTimePickerType = TimePickerType.DEPARTURE_TIME;

    /**
     * START_TIME is deprecated
     */
    public enum TimePickerType {
        START_TIME, DEPARTURE_TIME
    }

    public static TimePickerFragment newInstance(SimpleTime time,
            TimePickerType type) {
        TimePickerFragment frag = new TimePickerFragment();

        Bundle b = new Bundle();
        b.putInt(ARG_TYPE, type.ordinal());
        b.putInt(ARG_HOUR, time.getHour());
        b.putInt(ARG_MINUTE, time.getMinute());
        frag.setArguments(b);

        return frag;
    }

    public TimePickerFragment() {
        // Required
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle b = getArguments();
        int hour = 0;
        int minute = 0;

        // get arguments
        if (b != null) {
            mTimePickerType = TimePickerType.values()[b.getInt(ARG_TYPE)];
            hour = b.getInt(ARG_HOUR);
            minute = b.getInt(ARG_MINUTE);
        }

        Boolean is24HourClock = true; // Because the main view clock is also
                                      // always 24h

        // Create a new instance of TimePickerDialog with current start time
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                this, hour, minute, is24HourClock);

        // Set title
        switch (mTimePickerType) {
        case DEPARTURE_TIME:
            timePickerDialog.setTitle(getActivity()
                    .getString(R.string.time_picker_title_departure));
            break;
        default:
            // not used
            break;
        }
        return timePickerDialog;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        FragmentActivity a = getActivity();

        // Checks for null also
        if (a instanceof MainActivity) {
            SimpleTime t = new SimpleTime(hourOfDay, minute);
            ((MainActivity) getActivity())
                    .handleTimePickerResult(t, mTimePickerType);
        }

    }
}