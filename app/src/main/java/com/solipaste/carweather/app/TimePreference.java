/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

import com.solipaste.carweather.domain.SimpleTime;

public class TimePreference extends DialogPreference {
    private TimePicker mPicker = null;
    private SimpleTime mTime;

    public TimePreference(Context ctxt, AttributeSet attrs) {
        super(ctxt, attrs);

        init(ctxt);
    }

    public TimePreference(Context ctxt, AttributeSet attrs, int defStyle) {
        super(ctxt, attrs, defStyle);

        init(ctxt);
    }

    private void init(Context ctxt) {
        setPositiveButtonText(R.string.button_label_set);
        setNegativeButtonText(android.R.string.cancel);
        mTime = new SimpleTime();
    }

    public void show() {
        showDialog(null);
    }

    @Override
    protected View onCreateDialogView() {
        mPicker = new TimePicker(getContext());
        mPicker.setIs24HourView(DateFormat.is24HourFormat(getContext()));
        return (mPicker);
    }

    @Override
    protected void onBindDialogView(View v) {
        super.onBindDialogView(v);
        mPicker.setCurrentHour(mTime.getHour());
        mPicker.setCurrentMinute(mTime.getMinute());
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
            // Always initialize with the current date
            SimpleTime newTime = new SimpleTime(mPicker.getCurrentHour(),
                    mPicker.getCurrentMinute());

            if (callChangeListener(newTime)) {

                // persist time only if positive answer from listener
                mTime = newTime;
                persistString(mTime.toString());

                // Set summary
                setSummary(getSummary());
                notifyChanged();
            }
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return (a.getString(index));
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {

        if (restoreValue) {
            if (defaultValue == null) {
                mTime = new SimpleTime(
                        getPersistedString(getContext()
                                .getString(R.string.pref_notification_timing_time_default)));
            } else {
                mTime = new SimpleTime(
                        getPersistedString((String) defaultValue));
            }
        } else {
            if (defaultValue == null) {
                mTime = new SimpleTime();
            } else {
                mTime = new SimpleTime((String) defaultValue);
            }
        }
    }

    public SimpleTime getTime() {
        return mTime;
    }

    public void setTime(SimpleTime time) {
        mTime = time;
    }

}