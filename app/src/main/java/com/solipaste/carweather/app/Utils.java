/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.app;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Map;

import javax.security.auth.x500.X500Principal;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;

public class Utils {
    private final static String TAG = Utils.class.getName();

    private static final X500Principal DEBUG_DN = new X500Principal(
            "CN=Android Debug,O=Android,C=US");

    static boolean isDebuggable(Context ctx) {
        boolean debuggable = false;

        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo pinfo = ctx.getPackageManager()
                    .getPackageInfo(ctx.getPackageName(),
                                    PackageManager.GET_SIGNATURES);
            Signature signatures[] = pinfo.signatures;

            for (Signature signature : signatures) {
                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                ByteArrayInputStream stream = new ByteArrayInputStream(
                        signature.toByteArray());
                X509Certificate cert = (X509Certificate) cf
                        .generateCertificate(stream);
                debuggable = cert.getSubjectX500Principal().equals(DEBUG_DN);
                if (debuggable)
                    break;
            }

        } catch (NameNotFoundException | CertificateException e) {
            // debuggable variable will remain false
        }
        return debuggable;
    }

    public static void logPreferences(Context context) {
        Log.v(TAG, "logPreferences");

        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());

        Map<String, ?> map = sp.getAll();
        Log.d(TAG, "Printing all preferences:");
        for (Map.Entry<String, ?> entry : map.entrySet()) {
            Log.d(TAG,
                  "Name: " + entry.getKey() + ", value: " + entry.getValue());
        }
    }

    /**
     * Shows error about a throwable object
     * 
     * @param t
     *            Throwable object
     */
    void showThrowable(Context context, Throwable t) {
        Log.v(TAG, "showThrowable");

        AlertDialog.Builder builder = new AlertDialog.Builder(
                context.getApplicationContext());
        builder.setTitle("Exception")
                .setMessage(t.toString())
                .setPositiveButton(context.getResources()
                                           .getString(R.string.close), null)
                .show();
    }

    /**
     * Shows a dialog with text
     * 
     * @param text
     */
    void showAlert(Context context, String text) {
        Log.v(TAG, "showAlert");

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(text)
                .setCancelable(true)
                .setNeutralButton(context.getResources()
                                          .getString(R.string.close),
                                  new DialogInterface.OnClickListener() {
                                      @Override
                                      public void onClick(
                                              DialogInterface dialog, int id) {
                                          dialog.cancel();
                                      }
                                  }).show();
    }

    /**
     * Verifies if network connection is UP
     * 
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        // Log.v(TAG, "isNetworkAvailable");
    
        Context appContext = context.getApplicationContext();
        ConnectivityManager connectivity = (ConnectivityManager) appContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            Log.e(TAG, "getSystemService for CONNECTIVITY_SERVICE returned null");
        } else {
            NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();
            return (networkInfo != null && networkInfo.isConnected());
        }
        return false;
    }
}
