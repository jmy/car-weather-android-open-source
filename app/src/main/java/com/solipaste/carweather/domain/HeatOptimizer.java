/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import com.solipaste.carweather.domain.Heater.HeaterType;

public abstract class HeatOptimizer {

    /**
     * 
     */
    private static final int NO_MAX_DURATION = 0;
    /**
     * 
     */
    private static final int MIN_TEMP_LIMIT = 0;
    private static final int FIRST_TEMP_LIMIT = -5;
    private static final int SECOND_TEMP_LIMIT = -10;

    /**
     * 
     */

    /**
     * Calculates optimal heating duration based on heater type, temperature and
     * maximum duration
     * 
     * @param h
     * @param temp
     * @param maxDur
     * @return Valid duration in minutes or zero.
     */
    public static int optimalDuration(Heater h, Temperature temp, int maxDur) {

        int dur = 0;
        if (h != null && temp.isValid()) {
            HeaterType ht = h.getType();
            dur = calculateDuration(temp, ht);
            if (maxDur != NO_MAX_DURATION) // no max duration set
                dur = Math.min(dur, maxDur);
        }
        return dur;
    }

    /**
     * Calculate optimal heating duration based on 
     * http://motiva.fi/liikenne/henkiloautoilu/taloudellinen_ajotapa/moottorin_esilammitys
     * <p>
     * <code>
     * Temperature    Block  Radiator
     *   0 ... -5:      0.5     1
     *  -5 ... -10:      1      2
     * -10 ... :         2      3 
     * </code>
     * 
     * @param temp
     * @param ht
     * @return
     */
    private static int calculateDuration(Temperature temp, HeaterType ht) {
        final int dur;

        if (temp.compareTo(new Temperature(MIN_TEMP_LIMIT)) == 1) {
            dur = 0;
        } else if (temp.isInRangeInclusive(FIRST_TEMP_LIMIT, MIN_TEMP_LIMIT)) {
            dur = HeaterType.BLOCK == ht ? 30 : 60;
        } else if (temp.isInRangeInclusive(SECOND_TEMP_LIMIT, FIRST_TEMP_LIMIT)) {
            dur = HeaterType.BLOCK == ht ? 60 : 120;
        } else {
            dur = HeaterType.BLOCK == ht ? 120 : 180;
        }

        return dur;
    }
}
