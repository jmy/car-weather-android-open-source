/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

public class Heater {
//    private static final int INITIAL_MAX_ALLOWED_BLOCK_HEAT_DURATION = 2 * 60;
//    private static final int INITIAL_MAX_ALLOWED_RADIATOR_HEAT_DURATION = 3 * 60;
//    static private final int MAX_HEAT_DURATION_LIMIT = 12 * 60; // 12 hours
//    static private final int MIN_HEAT_DURATION_LIMIT = 30; // 0.5 hours

    public enum HeaterType {
        RADIATOR, BLOCK
    }

    /**
     * In minutes
     */


    private HeaterType type = HeaterType.RADIATOR;

    public Heater(HeaterType type) {
        this.type = type;
//        resetInitialMaxAllowedHeatDuration();
    }

//    /**
//     */
//    public void resetInitialMaxAllowedHeatDuration() {
//        if (type == HeaterType.RADIATOR )
//            maxAllowedHeatDuration = INITIAL_MAX_ALLOWED_RADIATOR_HEAT_DURATION;
//        else if (type == HeaterType.BLOCK)
//            maxAllowedHeatDuration = INITIAL_MAX_ALLOWED_BLOCK_HEAT_DURATION;
//    }

    /**
     * @return the type
     */
    public HeaterType getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(HeaterType type) {
        this.type = type;
    }

//    /**
//     * @param maxAllowedHeatDuration
//     *            the maxAllowedHeatDuration to set
//     */
//    public void setMaxHeatDuration(int maxHeatDuration) {
//        if (maxHeatDuration <= MAX_HEAT_DURATION_LIMIT
//                && maxHeatDuration >= MIN_HEAT_DURATION_LIMIT)
//            this.maxAllowedHeatDuration = maxHeatDuration;
//        else if (maxHeatDuration < MIN_HEAT_DURATION_LIMIT)
//            this.maxAllowedHeatDuration = MIN_HEAT_DURATION_LIMIT;
//        else if (maxHeatDuration > MIN_HEAT_DURATION_LIMIT)
//            this.maxAllowedHeatDuration = MAX_HEAT_DURATION_LIMIT;
//    }

}
