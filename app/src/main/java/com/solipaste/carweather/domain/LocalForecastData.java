/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import java.util.Date;

public class LocalForecastData implements ForecastData {

    private final Forecast mForecast;

    public LocalForecastData(Forecast t) {
        mForecast = t;
    }

    public LocalForecastData(Temperature temperature) {
        mForecast = new Forecast(temperature);
    }

    public LocalForecastData(Temperature temperature, WeatherSymbol symbol) {
        mForecast = new Forecast(temperature, symbol);
    }
    
    @Override
    public String getCreditsText() {
        return "Using test data";
    }

    @Override
    public String getCreditsUrl() {
        return null;
    }

    @Override
    public Temperature getTemperature() {
        return mForecast.temperature;
    }

    @Override
    public Temperature getTemperature(Date time) {
        return mForecast.temperature;
    }

    @Override
    public boolean isFreshData() {
        return true;
    }

    @Override
    public boolean isValidData() {
        return true;
    }

    @Override
    public Forecast getForecast(Date time) {
        return mForecast;
    }

}
