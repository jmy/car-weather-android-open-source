/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import java.io.IOException;

import android.util.SparseArray;

public class Location {
    // private final List<String> mData;
    private final String mName;
    private String mClassCode;
    private Country mCountry = Country.FI;
    private int mAdmin1;
    private int mAdmin2;
    private int mAdmin3;
    private String mLongName;
    private String mUiName;

    public enum Country {
        FI(1) {
            @Override
            public String toString() {
                return "Finland";
            }
        },
        SE(2) {
            @Override
            public String toString() {
                return "Sweden";
            }
        },
        NO(3) {
            @Override
            public String toString() {
                return "Norway";
            }
        };

        private int mCode;

        private Country(int c) {
            mCode = c;
        }

        static Country createCountry(String s) {
            Country c = null;
            if (s.equalsIgnoreCase("FI"))
                c = FI;
            if (s.equalsIgnoreCase("SE"))
                c = SE;
            if (s.equalsIgnoreCase("NO"))
                c = NO;
            return c;
        }

        public int getCode() {
            return mCode;
        }

        public void setCode(int code) {
            mCode = code;
        }
    };

    /**
     * <code>
     * name              : name of geographical point (utf8) varchar(200)
     * feature class     : see http://www.geonames.org/export/codes.html, char(1)
     * country code      : ISO-3166 2-letter country code, 2 characters
     * admin1 code       : fipscode (subject to change to iso code), see exceptions below, see file admin1Codes.txt for display names of this code; varchar(20)
     * admin2 code       : code for the second administrative division, a county in the US, see file admin2Codes.txt; varchar(80) 
     * admin3 code       : code for third level administrative division, varchar(20)
     * </code>
     * 
     * @param r
     * @throws IOException
     */
    public Location(String name, String classCode, String country,
            String admin1, String admin2, String admin3) {
        mName = name;
        mClassCode = classCode;
        mCountry = Country.createCountry(country); // maybe null

        try {
            if (admin1 != null && admin1.length() > 0)
                mAdmin1 = Integer.parseInt(admin1);
            if (admin2 != null && admin2.length() > 0)
                mAdmin2 = Integer.parseInt(admin2);
            if (admin3 != null && admin3.length() > 0)
                mAdmin3 = Integer.parseInt(admin3);
        } catch (NumberFormatException e) {
            // e.printStackTrace(); File locations.csv tested on 2012.11.07 for
            // incorrect values
        }
    }

    public Location(String name, String classCode, String country,
            String admin1, String admin2) {
        this(name, classCode, country, admin1, admin2, null);
    }

    public Location(String name, String longName) {
        mName = name;
        mLongName = longName;
    }

    @Override
    public String toString() {
        return mName;
    }

    public String getName() {
        return mName;
    }

    public String getClassCode() {
        return mClassCode;
    }

    public Country getCountry() {
        return mCountry;
    }

    public int getAdmin1() {
        return mAdmin1;
    }

    public int getAdmin2() {
        return mAdmin2;
    }

    public int getAdmin3() {
        return mAdmin3;
    }

    public void setAdmin3(String s) {
        mAdmin3 = Integer.parseInt(s);
    }

    public void setLongName(String name) {
        this.mLongName = name;
    }

    public String getLongName() {

        if (mLongName == null) {
            // construct long location string
            String country = getCountryName();
            if (country != null)
                country = country.replace(' ', '_');
            String reg = getRegionName();
            if (reg != null)
                reg = reg.replace(' ', '_');
            String reg2 = getRegion2Name();
            if (reg2 != null)
                reg2 = reg2.replace(' ', '_');
            String city = getName();
            if (city != null)
                city = city.replace(' ', '_');
            if (country != null && country.length() > 0 && reg != null
                    && reg.length() > 0)
                if (reg2 != null && reg2.length() > 0)
                    mLongName = String.format("%s/%s/%s/%s", country, reg,
                                              reg2, city);
                else
                    mLongName = String.format("%s/%s/%s", country, reg, city);
        }
        return mLongName;
    }

    public String getLongRegionUIName(SparseArray<String> localizedCountryNames) {

        if (mUiName == null) {
            // construct UI friendly location string
            String countryName = null;
            if (localizedCountryNames != null && mCountry != null)
                countryName = localizedCountryNames.get(mCountry.getCode());
            String reg = null;
            String reg2 = null;
            if (mCountry != Country.FI) {
                // Ignore obsolete county names in case of Finland
                reg = getRegionName();
                reg2 = getRegion2Name();
            }

            // Hide duplicate region names
            if (mName != null && reg2 != null && mName.equals(reg2))
                reg2 = null;
            if (mName != null && reg != null && mName.equals(reg))
                reg = null;

            // Construct string with fields that are not null
            if (reg != null) {
                if (reg2 != null)
                    mUiName = String.format("%s, %s, %s", countryName, reg,
                                            reg2);
                else
                    mUiName = String.format("%s, %s", countryName, reg);
            } else {
                mUiName = countryName;
            }

        }
        return mUiName;
    }

    /**
     * @param regionId
     * @return
     */
    public String getRegionName() {
        String reg = null;
        int regionId = getAdmin1();
        if (regionId > 0 && mCountry != null) {
            switch (mCountry) {
            case FI:
                reg = regNamesFI[regionId];
                break;
            case SE:
                reg = regNamesSE[regionId];
                break;
            case NO:
                reg = regNamesNO[regionId];
                break;
            default:
                break;
            }
        }

        return reg;
    }

    /**
     * @param regionId
     * @return
     */
    public String getRegion2Name() {
        String reg = null;
        int region2Id = getAdmin2();
        if (region2Id > 0 && mCountry != null) {
            switch (mCountry) {
            case NO:
                reg = reg2NamesNO.get(region2Id);
                break;
            default:
                break;
            }
        }

        return reg;
    }

    public String getCountryName() {
        String c = null;
        if (mCountry != null)
            c = mCountry.toString();
        return c;
    }

    private static final String[] regNamesFI = { "", // 00
            "", // 01
            "", // 02
            "", // 03
            "", // 04
            "", // 05
            "Lappland", // 06
            "", // 07
            "Oulu", // 08
            "", // 09
            "", // 10
            "", // 11
            "", // 12
            "Southern Finland", // 13
            "Eastern Finland", // 14
            "Western Finland" // 15
    };
    private static final String[] regNamesSE = { "", // 00
            "", // 01
            "Blekinge", // 02
            "Gävleborg", // 03
            "", // 04
            "Gotland", // 05
            "Halland", // 06
            "Jämtland",// 07
            "Jönköping", // 08
            "Kalmar", // 09
            "Dalarna", // 10
            "", // 11
            "Kronoberg", // 12
            "", // 13
            "Norrbotten", // 14
            "Örebro", // 15
            "Östergötland", // 16
            "", // 17
            "Södermanland", // 18
            "", // 19
            "", // 20
            "Uppsala", // 21
            "Värmland", // 22
            "Västerbotten", // 23
            "Västernorrland", // 24
            "Västmanland", // 25
            "Stockholm", // 26
            "Skåne", // 27
            "Västra Götaland" // 28
    };
    private static final String[] regNamesNO = { "", // 00
            "Akershus", // 01
            "Aust-Agder", // 02
            "", // 03
            "Buskerud county", // 04
            "Finnmark", // 05
            "Hedmark", // 06
            "Hordaland", // 07
            "Møre og Romsdal", // 08
            "Nordland", // 09
            "Nord-Trøndelag", // 10
            "Oppland", // 11
            "Oslo", // 12
            "Østfold", // 13
            "Rogaland", // 14
            "Sogn og Fjordane", // 15
            "Sør-Trøndelag", // 16
            "Telemark", // 17
            "Troms", // 18
            "Vest-Agder", // 19
            "Vestfold", // 20

    };
    private static final SparseArray<String> reg2NamesNO = new SparseArray<>();
    static {
        reg2NamesNO.put(101, "Halden");
        reg2NamesNO.put(104, "Moss");
        reg2NamesNO.put(105, "Sarpsborg");
        reg2NamesNO.put(106, "Fredrikstad");
        reg2NamesNO.put(111, "Hvaler");
        reg2NamesNO.put(118, "Aremark");
        reg2NamesNO.put(119, "Marker");
        reg2NamesNO.put(121, "Rømskog");
        reg2NamesNO.put(122, "Trøgstad");
        reg2NamesNO.put(123, "Spydeberg");
        reg2NamesNO.put(124, "Askim");
        reg2NamesNO.put(125, "Eidsberg");
        reg2NamesNO.put(127, "Skiptvet");
        reg2NamesNO.put(128, "Rakkestad");
        reg2NamesNO.put(135, "Råde");
        reg2NamesNO.put(136, "Rygge");
        reg2NamesNO.put(137, "Våler");
        reg2NamesNO.put(138, "Hobøl");
        reg2NamesNO.put(211, "Vestby");
        reg2NamesNO.put(213, "Ski");
        reg2NamesNO.put(214, "Ås");
        reg2NamesNO.put(215, "Frogn");
        reg2NamesNO.put(216, "Nesodden");
        reg2NamesNO.put(217, "Oppegård");
        reg2NamesNO.put(219, "Bærum");
        reg2NamesNO.put(220, "Asker");
        reg2NamesNO.put(221, "Aurskog-Høland");
        reg2NamesNO.put(226, "Sørum");
        reg2NamesNO.put(227, "Fet");
        reg2NamesNO.put(228, "Rælingen");
        reg2NamesNO.put(229, "Enebakk");
        reg2NamesNO.put(230, "Lørenskog");
        reg2NamesNO.put(231, "Skedsmo");
        reg2NamesNO.put(233, "Nittedal");
        reg2NamesNO.put(234, "Gjerdrum");
        reg2NamesNO.put(235, "Ullensaker");
        reg2NamesNO.put(236, "Nes");
        reg2NamesNO.put(237, "Eidsvoll");
        reg2NamesNO.put(238, "Nannestad");
        reg2NamesNO.put(239, "Hurdal");
        reg2NamesNO.put(301, "Oslo");
        reg2NamesNO.put(402, "Kongsvinger");
        reg2NamesNO.put(403, "Hamar");
        reg2NamesNO.put(412, "Ringsaker");
        reg2NamesNO.put(415, "Løten");
        reg2NamesNO.put(417, "Stange");
        reg2NamesNO.put(418, "Nord-Odal");
        reg2NamesNO.put(419, "Sør-Odal");
        reg2NamesNO.put(420, "Eidskog");
        reg2NamesNO.put(423, "Grue");
        reg2NamesNO.put(425, "Åsnes");
        reg2NamesNO.put(426, "Våler");
        reg2NamesNO.put(427, "Elverum");
        reg2NamesNO.put(428, "Trysil");
        reg2NamesNO.put(429, "Åmot");
        reg2NamesNO.put(430, "Stor-Elvdal");
        reg2NamesNO.put(432, "Rendalen");
        reg2NamesNO.put(434, "Engerdal");
        reg2NamesNO.put(436, "Tolga");
        reg2NamesNO.put(437, "Tynset");
        reg2NamesNO.put(438, "Alvdal");
        reg2NamesNO.put(439, "Folldal");
        reg2NamesNO.put(441, "Os");
        reg2NamesNO.put(501, "Lillehammer");
        reg2NamesNO.put(502, "Gjøvik");
        reg2NamesNO.put(511, "Dovre");
        reg2NamesNO.put(512, "Lesja");
        reg2NamesNO.put(513, "Skjåk");
        reg2NamesNO.put(514, "Lom");
        reg2NamesNO.put(515, "Vågå");
        reg2NamesNO.put(516, "Nord-Fron");
        reg2NamesNO.put(517, "Sel");
        reg2NamesNO.put(519, "Sør-Fron");
        reg2NamesNO.put(520, "Ringebu");
        reg2NamesNO.put(521, "Øyer");
        reg2NamesNO.put(522, "Gausdal");
        reg2NamesNO.put(528, "Østre Toten");
        reg2NamesNO.put(529, "Vestre Toten");
        reg2NamesNO.put(532, "Jevnaker");
        reg2NamesNO.put(533, "Lunner");
        reg2NamesNO.put(534, "Gran");
        reg2NamesNO.put(536, "Søndre Land");
        reg2NamesNO.put(538, "Nordre Land");
        reg2NamesNO.put(540, "Sør-Aurdal");
        reg2NamesNO.put(541, "Etnedal");
        reg2NamesNO.put(542, "Nord-Aurdal");
        reg2NamesNO.put(543, "Vestre Slidre");
        reg2NamesNO.put(544, "Øystre Slidre");
        reg2NamesNO.put(545, "Vang");
        reg2NamesNO.put(602, "Drammen");
        reg2NamesNO.put(604, "Kongsberg");
        reg2NamesNO.put(605, "Ringerike");
        reg2NamesNO.put(612, "Hole");
        reg2NamesNO.put(615, "Flå");
        reg2NamesNO.put(616, "Nes");
        reg2NamesNO.put(617, "Gol");
        reg2NamesNO.put(618, "Hemsedal");
        reg2NamesNO.put(619, "Ål");
        reg2NamesNO.put(620, "Hol");
        reg2NamesNO.put(621, "Sigdal");
        reg2NamesNO.put(622, "Krødsherad");
        reg2NamesNO.put(623, "Modum");
        reg2NamesNO.put(624, "Øvre Eiker");
        reg2NamesNO.put(625, "Nedre Eiker");
        reg2NamesNO.put(626, "Lier");
        reg2NamesNO.put(627, "Røyken");
        reg2NamesNO.put(628, "Hurum");
        reg2NamesNO.put(631, "Flesberg");
        reg2NamesNO.put(632, "Rollag");
        reg2NamesNO.put(633, "Nore og Uvdal");
        reg2NamesNO.put(701, "Horten");
        reg2NamesNO.put(702, "Holmestrand");
        reg2NamesNO.put(704, "Tønsberg");
        reg2NamesNO.put(706, "Sandefjord");
        reg2NamesNO.put(709, "Larvik");
        reg2NamesNO.put(711, "Svelvik");
        reg2NamesNO.put(713, "Sande");
        reg2NamesNO.put(714, "Hof");
        reg2NamesNO.put(716, "Re");
        reg2NamesNO.put(719, "Andebu");
        reg2NamesNO.put(720, "Stokke");
        reg2NamesNO.put(722, "Nøtterøy");
        reg2NamesNO.put(723, "Tjøme");
        reg2NamesNO.put(728, "Lardal");
        reg2NamesNO.put(805, "Porsgrunn");
        reg2NamesNO.put(806, "Skien");
        reg2NamesNO.put(807, "Notodden");
        reg2NamesNO.put(811, "Siljan");
        reg2NamesNO.put(814, "Bamble");
        reg2NamesNO.put(815, "Kragerø");
        reg2NamesNO.put(817, "Drangedal");
        reg2NamesNO.put(819, "Nome");
        reg2NamesNO.put(821, "Bø");
        reg2NamesNO.put(822, "Sauherad");
        reg2NamesNO.put(826, "Tinn");
        reg2NamesNO.put(827, "Hjartdal");
        reg2NamesNO.put(828, "Seljord");
        reg2NamesNO.put(829, "Kviteseid");
        reg2NamesNO.put(830, "Nissedal");
        reg2NamesNO.put(831, "Fyresdal");
        reg2NamesNO.put(833, "Tokke");
        reg2NamesNO.put(834, "Vinje");
        reg2NamesNO.put(901, "Risør");
        reg2NamesNO.put(904, "Grimstad");
        reg2NamesNO.put(906, "Arendal");
        reg2NamesNO.put(911, "Gjerstad");
        reg2NamesNO.put(912, "Vegårshei");
        reg2NamesNO.put(914, "Tvedestrand");
        reg2NamesNO.put(919, "Froland");
        reg2NamesNO.put(926, "Lillesand");
        reg2NamesNO.put(928, "Birkenes");
        reg2NamesNO.put(929, "Åmli");
        reg2NamesNO.put(935, "Iveland");
        reg2NamesNO.put(937, "Evje og Hornnes");
        reg2NamesNO.put(938, "Bygland");
        reg2NamesNO.put(940, "Valle");
        reg2NamesNO.put(941, "Bykle");
        reg2NamesNO.put(1001, "Kristiansand");
        reg2NamesNO.put(1002, "Mandal");
        reg2NamesNO.put(1003, "Farsund");
        reg2NamesNO.put(1004, "Flekkefjord");
        reg2NamesNO.put(1014, "Vennesla");
        reg2NamesNO.put(1017, "Songdalen");
        reg2NamesNO.put(1018, "Søgne");
        reg2NamesNO.put(1021, "Marnardal");
        reg2NamesNO.put(1026, "Åseral");
        reg2NamesNO.put(1027, "Audnedal");
        reg2NamesNO.put(1029, "Lindesnes");
        reg2NamesNO.put(1032, "Lyngdal");
        reg2NamesNO.put(1034, "Hægebostad");
        reg2NamesNO.put(1037, "Kvinesdal");
        reg2NamesNO.put(1046, "Sirdal");
        reg2NamesNO.put(1101, "Eigersund");
        reg2NamesNO.put(1102, "Sandnes");
        reg2NamesNO.put(1103, "Stavanger");
        reg2NamesNO.put(1106, "Haugesund");
        reg2NamesNO.put(1111, "Sokndal");
        reg2NamesNO.put(1112, "Lund");
        reg2NamesNO.put(1114, "Bjerkreim");
        reg2NamesNO.put(1119, "Hå");
        reg2NamesNO.put(1120, "Klepp");
        reg2NamesNO.put(1121, "Time");
        reg2NamesNO.put(1122, "Gjesdal");
        reg2NamesNO.put(1124, "Sola");
        reg2NamesNO.put(1127, "Randaberg");
        reg2NamesNO.put(1129, "Forsand");
        reg2NamesNO.put(1130, "Strand");
        reg2NamesNO.put(1133, "Hjelmeland");
        reg2NamesNO.put(1134, "Suldal");
        reg2NamesNO.put(1135, "Sauda");
        reg2NamesNO.put(1141, "Finnøy");
        reg2NamesNO.put(1142, "Rennesøy");
        reg2NamesNO.put(1144, "Kvitsøy");
        reg2NamesNO.put(1145, "Bokn");
        reg2NamesNO.put(1146, "Tysvær");
        reg2NamesNO.put(1149, "Karmøy");
        reg2NamesNO.put(1151, "Utsira");
        reg2NamesNO.put(1160, "Vindafjord");
        reg2NamesNO.put(1201, "Bergen");
        reg2NamesNO.put(1211, "Etne");
        reg2NamesNO.put(1216, "Sveio");
        reg2NamesNO.put(1219, "Bømlo");
        reg2NamesNO.put(1221, "Stord");
        reg2NamesNO.put(1222, "Fitjar");
        reg2NamesNO.put(1223, "Tysnes");
        reg2NamesNO.put(1224, "Kvinnherad");
        reg2NamesNO.put(1227, "Jondal");
        reg2NamesNO.put(1228, "Odda");
        reg2NamesNO.put(1231, "Ullensvang");
        reg2NamesNO.put(1232, "Eidfjord");
        reg2NamesNO.put(1233, "Ulvik");
        reg2NamesNO.put(1234, "Granvin");
        reg2NamesNO.put(1235, "Voss");
        reg2NamesNO.put(1238, "Kvam");
        reg2NamesNO.put(1241, "Fusa");
        reg2NamesNO.put(1242, "Samnanger");
        reg2NamesNO.put(1243, "Os");
        reg2NamesNO.put(1244, "Austevoll");
        reg2NamesNO.put(1245, "Sund");
        reg2NamesNO.put(1246, "Fjell");
        reg2NamesNO.put(1247, "Askøy");
        reg2NamesNO.put(1251, "Vaksdal");
        reg2NamesNO.put(1252, "Modalen");
        reg2NamesNO.put(1253, "Osterøy");
        reg2NamesNO.put(1256, "Meland");
        reg2NamesNO.put(1259, "Øygarden");
        reg2NamesNO.put(1260, "Radøy");
        reg2NamesNO.put(1263, "Lindås");
        reg2NamesNO.put(1264, "Austrheim");
        reg2NamesNO.put(1265, "Fedje");
        reg2NamesNO.put(1266, "Masfjorden");
        reg2NamesNO.put(1401, "Flora");
        reg2NamesNO.put(1411, "Gulen");
        reg2NamesNO.put(1412, "Solund");
        reg2NamesNO.put(1413, "Hyllestad");
        reg2NamesNO.put(1416, "Høyanger");
        reg2NamesNO.put(1417, "Vik");
        reg2NamesNO.put(1418, "Balestrand");
        reg2NamesNO.put(1419, "Leikanger");
        reg2NamesNO.put(1420, "Sogndal");
        reg2NamesNO.put(1421, "Aurland");
        reg2NamesNO.put(1422, "Lærdal");
        reg2NamesNO.put(1424, "Årdal");
        reg2NamesNO.put(1426, "Luster");
        reg2NamesNO.put(1428, "Askvoll");
        reg2NamesNO.put(1429, "Fjaler");
        reg2NamesNO.put(1430, "Gaular");
        reg2NamesNO.put(1431, "Jølster");
        reg2NamesNO.put(1432, "Førde");
        reg2NamesNO.put(1433, "Naustdal");
        reg2NamesNO.put(1438, "Bremanger");
        reg2NamesNO.put(1439, "Vågsøy");
        reg2NamesNO.put(1441, "Selje");
        reg2NamesNO.put(1443, "Eid");
        reg2NamesNO.put(1444, "Hornindal");
        reg2NamesNO.put(1445, "Gloppen");
        reg2NamesNO.put(1449, "Stryn");
        reg2NamesNO.put(1502, "Molde");
        reg2NamesNO.put(1504, "Ålesund");
        reg2NamesNO.put(1505, "Kristiansund");
        reg2NamesNO.put(1511, "Vanylven");
        reg2NamesNO.put(1514, "Sande");
        reg2NamesNO.put(1515, "Herøy");
        reg2NamesNO.put(1516, "Ulstein");
        reg2NamesNO.put(1517, "Hareid");
        reg2NamesNO.put(1519, "Volda");
        reg2NamesNO.put(1520, "Ørsta");
        reg2NamesNO.put(1523, "Ørskog");
        reg2NamesNO.put(1524, "Norddal");
        reg2NamesNO.put(1525, "Stranda");
        reg2NamesNO.put(1526, "Stordal");
        reg2NamesNO.put(1528, "Sykkylven");
        reg2NamesNO.put(1529, "Skodje");
        reg2NamesNO.put(1531, "Sula");
        reg2NamesNO.put(1532, "Giske");
        reg2NamesNO.put(1534, "Haram");
        reg2NamesNO.put(1535, "Vestnes");
        reg2NamesNO.put(1539, "Rauma");
        reg2NamesNO.put(1543, "Nesset");
        reg2NamesNO.put(1545, "Midsund");
        reg2NamesNO.put(1546, "Sandøy");
        reg2NamesNO.put(1547, "Aukra");
        reg2NamesNO.put(1548, "Fræna");
        reg2NamesNO.put(1551, "Eide");
        reg2NamesNO.put(1554, "Averøy");
        reg2NamesNO.put(1557, "Gjemnes");
        reg2NamesNO.put(1560, "Tingvoll");
        reg2NamesNO.put(1563, "Sunndal");
        reg2NamesNO.put(1566, "Surnadal");
        reg2NamesNO.put(1567, "Rindal");
        reg2NamesNO.put(1571, "Halsa");
        reg2NamesNO.put(1573, "Smøla");
        reg2NamesNO.put(1576, "Aure");
        reg2NamesNO.put(1601, "Trondheim");
        reg2NamesNO.put(1612, "Hemne");
        reg2NamesNO.put(1613, "Snillfjord");
        reg2NamesNO.put(1617, "Hitra");
        reg2NamesNO.put(1620, "Frøya");
        reg2NamesNO.put(1621, "Ørland");
        reg2NamesNO.put(1622, "Agdenes");
        reg2NamesNO.put(1624, "Rissa");
        reg2NamesNO.put(1627, "Bjugn");
        reg2NamesNO.put(1630, "Åfjord");
        reg2NamesNO.put(1632, "Roan");
        reg2NamesNO.put(1633, "Osen");
        reg2NamesNO.put(1634, "Oppdal");
        reg2NamesNO.put(1635, "Rennebu");
        reg2NamesNO.put(1636, "Meldal");
        reg2NamesNO.put(1638, "Orkdal");
        reg2NamesNO.put(1640, "Røros");
        reg2NamesNO.put(1644, "Holtålen");
        reg2NamesNO.put(1648, "Midtre Gauldal");
        reg2NamesNO.put(1653, "Melhus");
        reg2NamesNO.put(1657, "Skaun");
        reg2NamesNO.put(1662, "Klæbu");
        reg2NamesNO.put(1663, "Malvik");
        reg2NamesNO.put(1664, "Selbu");
        reg2NamesNO.put(1665, "Tydal");
        reg2NamesNO.put(1702, "Steinkjer");
        reg2NamesNO.put(1703, "Namsos");
        reg2NamesNO.put(1711, "Meråker");
        reg2NamesNO.put(1714, "Stjørdal");
        reg2NamesNO.put(1717, "Frosta");
        reg2NamesNO.put(1718, "Leksvik");
        reg2NamesNO.put(1719, "Levanger");
        reg2NamesNO.put(1721, "Verdal");
        reg2NamesNO.put(1723, "Mosvik");
        reg2NamesNO.put(1724, "Verran");
        reg2NamesNO.put(1725, "Namdalseid");
        reg2NamesNO.put(1729, "Inderøy");
        reg2NamesNO.put(1736, "Snåsa");
        reg2NamesNO.put(1738, "Lierne");
        reg2NamesNO.put(1739, "Røyrvik");
        reg2NamesNO.put(1740, "Namsskogan");
        reg2NamesNO.put(1742, "Grong");
        reg2NamesNO.put(1743, "Høylandet");
        reg2NamesNO.put(1744, "Overhalla");
        reg2NamesNO.put(1748, "Fosnes");
        reg2NamesNO.put(1749, "Flatanger");
        reg2NamesNO.put(1750, "Vikna");
        reg2NamesNO.put(1751, "Nærøy");
        reg2NamesNO.put(1755, "Leka");
        reg2NamesNO.put(1804, "Bodø");
        reg2NamesNO.put(1805, "Narvik");
        reg2NamesNO.put(1811, "Bindal");
        reg2NamesNO.put(1812, "Sømna");
        reg2NamesNO.put(1813, "Brønnøy");
        reg2NamesNO.put(1815, "Vega");
        reg2NamesNO.put(1816, "Vevelstad");
        reg2NamesNO.put(1818, "Herøy");
        reg2NamesNO.put(1820, "Alstahaug");
        reg2NamesNO.put(1822, "Leirfjord");
        reg2NamesNO.put(1824, "Vefsn");
        reg2NamesNO.put(1825, "Grane");
        reg2NamesNO.put(1826, "Hattfjelldal");
        reg2NamesNO.put(1827, "Dønna");
        reg2NamesNO.put(1828, "Nesna");
        reg2NamesNO.put(1832, "Hemnes");
        reg2NamesNO.put(1833, "Rana");
        reg2NamesNO.put(1834, "Lurøy");
        reg2NamesNO.put(1835, "Træna");
        reg2NamesNO.put(1836, "Rødøy");
        reg2NamesNO.put(1837, "Meløy");
        reg2NamesNO.put(1838, "Gildeskål");
        reg2NamesNO.put(1839, "Beiarn");
        reg2NamesNO.put(1840, "Saltdal");
        reg2NamesNO.put(1841, "Fauske");
        reg2NamesNO.put(1845, "Sørfold");
        reg2NamesNO.put(1848, "Steigen");
        reg2NamesNO.put(1849, "Hamarøy");
        reg2NamesNO.put(1850, "Tysfjord");
        reg2NamesNO.put(1851, "Lødingen");
        reg2NamesNO.put(1852, "Tjeldsund");
        reg2NamesNO.put(1853, "Evenes");
        reg2NamesNO.put(1854, "Ballangen");
        reg2NamesNO.put(1856, "Røst");
        reg2NamesNO.put(1857, "Værøy");
        reg2NamesNO.put(1859, "Flakstad");
        reg2NamesNO.put(1860, "Vestvågøy");
        reg2NamesNO.put(1865, "Vågan");
        reg2NamesNO.put(1866, "Hadsel");
        reg2NamesNO.put(1867, "Bø");
        reg2NamesNO.put(1868, "Øksnes");
        reg2NamesNO.put(1870, "Sortland");
        reg2NamesNO.put(1871, "Andøy");
        reg2NamesNO.put(1874, "Moskenes");
        reg2NamesNO.put(1901, "Harstad");
        reg2NamesNO.put(1902, "Tromsø");
        reg2NamesNO.put(1911, "Kvæfjord");
        reg2NamesNO.put(1913, "Skånland");
        reg2NamesNO.put(1915, "Bjarkøy");
        reg2NamesNO.put(1917, "Ibestad");
        reg2NamesNO.put(1919, "Gratangen");
        reg2NamesNO.put(1920, "Lavangen");
        reg2NamesNO.put(1922, "Bardu");
        reg2NamesNO.put(1923, "Salangen");
        reg2NamesNO.put(1924, "Målselv");
        reg2NamesNO.put(1925, "Sørreisa");
        reg2NamesNO.put(1926, "Dyrøy");
        reg2NamesNO.put(1927, "Tranøy");
        reg2NamesNO.put(1928, "Torsken");
        reg2NamesNO.put(1929, "Berg");
        reg2NamesNO.put(1931, "Lenvik");
        reg2NamesNO.put(1933, "Balsfjord");
        reg2NamesNO.put(1936, "Karlsøy");
        reg2NamesNO.put(1938, "Lyngen");
        reg2NamesNO.put(1939, "Storfjord");
        reg2NamesNO.put(1940, "Kåfjord");
        reg2NamesNO.put(1941, "Skjervøy");
        reg2NamesNO.put(1942, "Nordreisa");
        reg2NamesNO.put(1943, "Kvænangen");
        reg2NamesNO.put(2002, "Vardø");
        reg2NamesNO.put(2003, "Vadsø");
        reg2NamesNO.put(2004, "Hammerfest");
        reg2NamesNO.put(2011, "Kautokeino");
        reg2NamesNO.put(2012, "Alta");
        reg2NamesNO.put(2014, "Loppa");
        reg2NamesNO.put(2015, "Hasvik");
        reg2NamesNO.put(2017, "Kvalsund");
        reg2NamesNO.put(2018, "Måsøy");
        reg2NamesNO.put(2019, "Nordkapp");
        reg2NamesNO.put(2020, "Porsanger");
        reg2NamesNO.put(2021, "Karasjok");
        reg2NamesNO.put(2022, "Lebesby");
        reg2NamesNO.put(2023, "Gamvik");
        reg2NamesNO.put(2024, "Berlevåg");
        reg2NamesNO.put(2025, "Tana");
        reg2NamesNO.put(2027, "Nesseby");
        reg2NamesNO.put(2028, "Båtsfjord");
        reg2NamesNO.put(2030, "Sør-Varanger");
    }
}