/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import au.com.bytecode.opencsv.CSVReader;

public class Locations {

    private final LinkedHashMap<String, Location> mLocs;
    private boolean isInitialized = false;

    public Locations() {
        mLocs = new LinkedHashMap<>();
    }

    /**
     * Initialize data from file
     * 
     * @param r
     * @param testData
     * @throws IOException
     */
    public void Initialize(Reader r, boolean testData) throws IOException {

        mLocs.clear();

        CSVReader reader = null;
        try {
            if (testData) {
                addTestLocations();
            }
            reader = new CSVReader(r);
            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                // nextLine[] is an array of values from the line
                Location l = new Location(nextLine[1], nextLine[2],
                        nextLine[3], nextLine[4], nextLine[5]);
                if (nextLine.length == 7)
                    l.setAdmin3(nextLine[6]);

                mLocs.put(nextLine[1].toLowerCase(Locale.getDefault()), l);
            }
        } finally {
            if (reader != null)
                reader.close();
            isInitialized = true;
        }
    }

    private void addTestLocations() {
        Location testLoc = new Location("test 5", "*test 5");
        mLocs.put("test 5", testLoc);
        
        testLoc = new Location("test 0", "*test 0");
        mLocs.put("test 0", testLoc);

        testLoc = new Location("test -4", "*test -4");
        mLocs.put("test -4", testLoc);

        testLoc = new Location("test -9", "*test -9");
        mLocs.put("test -9", testLoc);

        testLoc = new Location("test -14", "*test -14");
        mLocs.put("test -14", testLoc);

        testLoc = new Location("snow", "*snow");
        mLocs.put("snow", testLoc);

        testLoc = new Location("sleet", "*sleet");
        mLocs.put("sleet", testLoc);
    }

    public boolean isInitialized() {
        return isInitialized;
    }

    /**
     * @return Map with lower case strings as keys
     */
    @Deprecated
    public Map<String, Location> getLocations() {
        return mLocs;
    }

    /**
     * Searches location case insensitively
     * 
     * @param mData
     * @return
     */
    public Location getLocation(String l) {
        return mLocs.get(l.toLowerCase(Locale.getDefault()));
    }

    public List<String> getNameList() {
        ArrayList<String> names = new ArrayList<>(mLocs.size());
        for (Map.Entry<String, Location> entry : mLocs.entrySet()) {
            names.add(entry.getValue().getName());
        }
        return names;
    }

    public List<Location> getLocationList() {

        List<Location> locations = new ArrayList<>(mLocs.values());
        return locations;
    }

    public boolean exists(String l) {
        return mLocs.containsKey(l.toLowerCase(Locale.getDefault()));
    }

    public int size() {
        return mLocs.size();
    }
}
