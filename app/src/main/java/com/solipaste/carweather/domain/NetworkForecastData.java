/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;

import com.solipaste.carweather.app.Log;

/**
 * Data container class for storing and caching temperature data
 * 
 */

public class NetworkForecastData implements ForecastData {
    private final static String TAG = NetworkForecastData.class.getName();

    private static final String CACHE_NAME_ENCODING = "SHA-1";

    // Ensure that the last time slot is never used if there is more than 24h
    // forecast
    // See: [#43721819] Temperature for the current time used if departure time
    // is > 23h but < 24h from now
    private static final int MAX_TIME_PERIODS_READ = 25;

    public static final String CACHE_FILE_NAME_PREFIX = "temp_";
    private static final String TEMP_FILE_NAME_PREFIX = "tmp";
    private static final String TEMP_FILE_NAME_SUFFIX = ".xml";
    private Map<Date, Forecast> mForecastMap;

    private String mCreditsText;
    private String mCreditsUrl;

    private Date mTimestamp;

    private static final int MINUTE_MS = 60 * 1000;
    private static final int HOUR_MS = 60 * MINUTE_MS;
    private static final int DAY_MS = 24 * HOUR_MS;

    public static final int CACHE_VALIDITY_PERIOD = 10 * MINUTE_MS;
    public static final long CACHE_FALLBACK_VALIDITY_PERIOD = DAY_MS;

    /**
     * Initialize with cache data if one exists
     * 
     */
    public NetworkForecastData() {
    }

    /**
     * @param creditText
     * @param creditUrl
     */
    private void setCreditsData(String text, String url) {
        mCreditsText = text;
        mCreditsUrl = url;
    }

    @Override
    public String getCreditsText() {
        return mCreditsText;
    }

    @Override
    public String getCreditsUrl() {
        return mCreditsUrl;
    }

    @Override
    public Temperature getTemperature() {
        Log.v(TAG, "getTemperature(now)");

        if (isValidData()) {
            // assume that the first entry is the most current
            return getTemperatureMap().entrySet().iterator().next().getValue().temperature;
        } else {
            // return dummy temp
            return new Temperature(Temperature.ERR_VALUE);
        }

    }

    @Override
    public Forecast getForecast(Date time) {
        Log.v(TAG, "getForecast: " + time);

        Forecast value = null;
        if (isValidData()) {
            Date key = dateKeyInRangeInMap(time, mForecastMap);
            if (key != null) {
                // value found in some range
                value = mForecastMap.get(key);
            } else {
                Log.w(TAG, "No forecast found for given time");
                value = new Forecast();
            }
        }
        return value;
    }

    @Override
    public Temperature getTemperature(Date time) {
        Log.v(TAG, "getTemperature: " + time);

        Temperature value = null;
        Forecast forecast = getForecast(time);
        if (forecast != null) {
            value = forecast.temperature;
        } else {
            Log.w(TAG,
                  "No temperature found for given time: reverting to current time");
            // return current temp instead
            value = getTemperature();
        }
        return value;
    }

    /**
     * @return the mForecastMap
     */
    public Map<Date, Forecast> getTemperatureMap() {
        return mForecastMap;
    }

    public void setTemperatures(Map<Date, Forecast> temperatures) {
        if (temperatures != null && !temperatures.isEmpty()) {
            this.mForecastMap = temperatures;

        } else {
            reset();
        }
    }

    @Override
    public boolean isValidData() {
        if (mForecastMap != null && !mForecastMap.isEmpty())
            return true;
        else
            return false;
    }

    @Override
    public boolean isFreshData() {
        Date now = new Date();

        return isValidData()
                && mTimestamp != null
                && (now.getTime() - mTimestamp.getTime() < CACHE_VALIDITY_PERIOD);
    }

    public void resetTimeStamp() {
        mTimestamp = new Date();

    }

    public Date getTimeStamp() {
        return mTimestamp;
    }

    private void setTimeStamp(long lastModified) {
        mTimestamp = new Date(lastModified);

    }

    /**
     * Clear all data
     * 
     */
    public void reset() {
        mForecastMap = null;
        mTimestamp = null;

    }

    /**
     * @return safe file handle for cache data
     */
    // TODO tests missing
    public static File getCacheFile(String id, Context context) {
        if (context == null) {
            // using test data
            return null;
        }

        String safeFileName = null;
        // get its digest
        MessageDigest sha;
        try {
            sha = MessageDigest.getInstance(CACHE_NAME_ENCODING);
            byte[] result = sha.digest(id.getBytes());
            safeFileName = CACHE_FILE_NAME_PREFIX + hexEncode(result);
            // Log.v(TAG, "cache file name " + safeFileName);
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "Could not encode cache file name");
            e.printStackTrace();
        }
        File file = null;
        if (safeFileName != null)
            file = new File(context.getCacheDir(), safeFileName);
        return file;
    }

    /**
     * Get data from cache
     * 
     * @param location
     * @param context
     * @return data (never null)
     */
    public static NetworkForecastData fromCache(String location, Context context) {
        Log.v(TAG, "fromCache");

        NetworkForecastData data = null;

        File cacheFile = getCacheFile(location, context);

        // Read data from the file
        data = fromFile(cacheFile);

        if (data == null)
            // Ensure that null is never returned
            data = new NetworkForecastData();

        return data;
    }

    /**
     * Get data from file
     * 
     * @param cacheFile
     * @return data or null if failure
     */
    private static NetworkForecastData fromFile(File cacheFile) {

        NetworkForecastData data = null;

        BufferedInputStream iStream = null;
        InputStreamReader r = null;
        try {
            iStream = new BufferedInputStream(new FileInputStream(cacheFile));

            r = new InputStreamReader(iStream, "UTF-8");

            data = parseXML(r);

            // Set time stamp to time when cache was written
            data.setTimeStamp(cacheFile.lastModified());

            Log.v(TAG, "Read cache success");
        } catch (Exception e) {
            Log.v(TAG, "Reading from cache fails: " + e.getMessage());
        } finally {
            if (iStream != null)
                try {
                    iStream.close();
                } catch (IOException e) {
                    // nothing can be done here
                }
            if (r != null)
                try {
                    r.close();
                } catch (IOException e) {
                    // nothing can be done here
                }
        }
        return data;
    }

    public static boolean moveTempToCache(File tempFile, String location,
            Context context) {

        boolean success = false;
        File cacheFile = getCacheFile(location, context);

        if (tempFile != null && cacheFile != null) {

            // First delete old cache if exists, ignore result
            cacheFile.delete();
            success = tempFile.renameTo(cacheFile);
            Log.d(TAG, "moved temp to cache: " + tempFile.getName() + " => "
                    + cacheFile.getName() + " : " + success);

            // Try to clean up if errors
            // Note that if rename fails old cache is lost
            if (!success) {
                tempFile.delete();
                cacheFile.delete();
            }
        }

        return success;

    }

    public static File writeToCache(String location, Reader reader,
            Context context, boolean useTempCache) {
        Log.v(TAG, "writeToCache");

        // Get file
        File cacheFile = (!useTempCache ? getCacheFile(location, context)
                : getTempFile(context));

        // write data to file
        cacheFile = writeToFile(reader, cacheFile);

        return cacheFile;

    }

    private static File writeToFile(Reader reader, File cacheFile) {
        BufferedWriter fileOutput = null;
        try {

            fileOutput = new BufferedWriter(new FileWriter(cacheFile));

            char[] cbuf = new char[1024];

            int readChars = 0; // used to store a temporary size of the
                               // buffer

            while (readChars != -1) {
                readChars = reader.read(cbuf);
                if (readChars > 0)
                    fileOutput.write(cbuf, 0, readChars);
            }

            Log.v(TAG, "Write cache success");

        } catch (Exception e) {
            Log.v(TAG, "Writing cache fails: " + e.getMessage());

            if (cacheFile != null) {
                try {
                    // try to delete the file
                    cacheFile.delete();
                } catch (Exception e1) {
                    // nothing
                }
            }
            // Return null File
            cacheFile = null;
        } finally {
            if (fileOutput != null)
                try {
                    fileOutput.close();
                } catch (IOException e) {
                    // nothing can be do here
                }
        }
        return cacheFile;
    }

    // Public visibility for testing purposes
    public static NetworkForecastData parseXML(Reader reader) {
        // Log.v(TAG, "fromXML");

        NetworkForecastData data = new NetworkForecastData();

        final String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

        Map<Date, Forecast> tempMap = new LinkedHashMap<>();

        SimpleDateFormat df = (SimpleDateFormat) DateFormat
                .getDateTimeInstance();
        df.applyPattern(DATE_FORMAT_PATTERN);

        XmlPullParserFactory factory;
        try {
            factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(reader);
            parser.nextTag();
            parser.require(XmlPullParser.START_TAG, null, "weatherdata");
            int eventType;
            Date currentDate = null;
            Forecast currentForecast = null;
            boolean inCredits = false;
            boolean inTabular = false;

            // Log.v(TAG, "Parsing xml...");
            while ((eventType = parser.next()) != XmlPullParser.END_DOCUMENT
                    && tempMap.size() <= MAX_TIME_PERIODS_READ) {

                if (eventType == XmlPullParser.START_TAG) {

                    if (parser.getName().equalsIgnoreCase("credit")) {
                        inCredits = true;

                    } else if (inCredits
                            && parser.getName().equalsIgnoreCase("link")) {
                        inCredits = false;
                        String creditText = parser.getAttributeValue(null,
                                                                     "text");
                        String creditUrl = parser
                                .getAttributeValue(null, "url");
                        data.setCreditsData(creditText, creditUrl);

                    } else if (parser.getName().equalsIgnoreCase("tabular")) {
                        // This is the forecast data we are interested in
                        inTabular = true;

                    } else if (inTabular
                            && parser.getName().equalsIgnoreCase("time")) {
                        // Store the start time to be used later
                        String from = parser.getAttributeValue(null, "from");

                        // Store date and create new weather
                        currentDate = df.parse(from);
                        currentForecast = new Forecast();

                    } else if (currentDate != null
                            && parser.getName().equalsIgnoreCase("temperature")) {
                        if (parser.getAttributeValue(null, "unit")
                                .equalsIgnoreCase("celsius")) {
                            String tempS = parser.getAttributeValue(null,
                                                                    "value");
                            Log.v(TAG, String.format("date: %s, temp: %s",
                                                     currentDate, tempS));

                            // Store temperature
                            // throws if not valid Temperature
                            currentForecast.temperature = new Temperature(tempS);

                        } else {
                            Log.e(TAG, "Invalid temperature unit");
                        }

                    } else if (currentDate != null
                            && parser.getName().equalsIgnoreCase("symbol")) {
                        String symbolS = parser.getAttributeValue(null,
                                                                  "number");
                        if (symbolS != null) {
                            // throws if not valid Temperature
                            int symbol = Integer.parseInt(symbolS);
                            Log.v(TAG, String.format("date: %s, symbol: %d",
                                                     currentDate, symbol));

                            // Store symbol
                            currentForecast.symbol = WeatherSymbol
                                    .fromNetworkSymbol(symbol);
                        }
                    }

                } else if (eventType == XmlPullParser.END_TAG) {

                    if (parser.getName().equalsIgnoreCase("tabular")) {
                        inTabular = false;

                    } else if (parser.getName().equalsIgnoreCase("credit")) {
                        inCredits = false;

                    } else if (parser.getName().equalsIgnoreCase("time")) {
                        // All time data collected
                        // Store it in the map
                        if (currentDate != null && currentForecast != null
                                && currentForecast.temperature != null) {
                            tempMap.put(currentDate, currentForecast);
                        }

                        // reset - not used anymore
                        currentForecast = null;
                        currentDate = null;
                    }
                }
            }

            if (!tempMap.isEmpty())
                data.setTemperatures(tempMap);
            else
                // Something failed - return empty data
                data.reset();

        } catch (Exception e) {
            // No errors allowed
            Log.e(TAG, "XML parsing error for data for yr.no");
            Log.e(TAG, e.getLocalizedMessage());
            data.reset();
        }

        return data;

    }

    private static File getTempFile(Context context) {
        File outputDir = context.getCacheDir();
        File file = null;
        try {
            file = File.createTempFile(TEMP_FILE_NAME_PREFIX,
                                       TEMP_FILE_NAME_SUFFIX, outputDir);
        } catch (IOException e) {
            Log.e(TAG, "Could not create temp file");
            e.printStackTrace();
        }
        return file;
    }

    /**
     * This implementation follows the example of David Flanagan's book
     * "Java In A Nutshell", and converts a byte array into a String of hex
     * characters.
     * 
     */
    private static String hexEncode(byte[] aInput) {
        StringBuilder result = new StringBuilder();
        char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f' };
        for (byte b : aInput) {
            result.append(digits[(b & 0xf0) >> 4]);
            result.append(digits[b & 0x0f]);
        }
        return result.toString();
    }

    /**
     * Get date key that contains given date
     * 
     * @param date
     * @return
     */
    private static Date dateKeyInRangeInMap(Date date, Map<Date, Forecast> map) {
        Date prevEntry = null;
        Date correctEntry = null;
        for (Date entry : map.keySet()) {
            if (entry.after(date)) { // is entry later than the current time
                if (prevEntry != null) { // is this the first entry
                    correctEntry = prevEntry;
                } else {
                    correctEntry = entry; // return the first entry
                    // TODO: Check that the first entry is not more than one
                    // hour later the asked time
                }
                break;
            }
            prevEntry = entry; // remember the previous entry
        }
        return correctEntry;
    }

    private static long maxAge(boolean forFallbackUse) {
        Log.v(TAG, "maxAge, forFallbackUse: " + forFallbackUse);

        return forFallbackUse ? NetworkForecastData.CACHE_FALLBACK_VALIDITY_PERIOD
                : NetworkForecastData.CACHE_VALIDITY_PERIOD;
    }
}