/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SimpleTime {

    private static final int MAX_HOUR = 23;
    private static final int MIN_UNIT = 0;
    private static final int MAX_MIN = 59;
    private final int hours;
    private final int minutes;

    /**
     * Initialize with current time
     */
    public SimpleTime() {
        Calendar cal = Calendar.getInstance();
        this.hours = cal.get(Calendar.HOUR_OF_DAY);
        this.minutes = cal.get(Calendar.MINUTE);
    }

    /**
     * Copy constructor
     * 
     * @param other
     */
    public SimpleTime(SimpleTime other) {
        if (other != null) {
            this.hours = other.hours;
            this.minutes = other.minutes;
        } else {
            this.hours = 0;
            this.minutes = 0;
        }
    }

    public SimpleTime(int hour, int minute) {
        if (hour > MAX_HOUR || minute > MAX_MIN || hour < MIN_UNIT
                || minute < MIN_UNIT) {
            this.hours = 0;
            this.minutes = 0;
        } else {
            this.hours = hour;
            this.minutes = minute;
        }
    }

    public SimpleTime(int minutes) {
        if (minutes < MIN_UNIT || minutes > (24 * 60 - 1)) {
            this.hours = 0;
            this.minutes = 0;
        } else {
            int hours = 0;
            if (minutes >= 60) {
                hours = minutes / 60;
                minutes -= hours * 60;
            }
            this.hours = hours;
            this.minutes = minutes;
        }
    }

    public SimpleTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        this.hours = cal.get(Calendar.HOUR_OF_DAY);
        this.minutes = cal.get(Calendar.MINUTE);
    }

    public SimpleTime(Calendar cal) {
        this.hours = cal.get(Calendar.HOUR_OF_DAY);
        this.minutes = cal.get(Calendar.MINUTE);
    }

    public SimpleTime(String time) {
        String[] pieces = time.split(":");
        int hour = Integer.parseInt(pieces[0]);
        int minute = Integer.parseInt(pieces[1]);
        if (hour > MAX_HOUR || minute > MAX_MIN || hour < MIN_UNIT
                || minute < MIN_UNIT) {
            this.hours = 0;
            this.minutes = 0;
        } else {
            this.hours = hour;
            this.minutes = minute;
        }
    }

    /**
     * Creates date object out of this where date is today. Assumes that date is
     * tomorrow if time is before current time.
     * 
     * @return Date object
     */
    public Date getDate() {
        Calendar cal = getCalendar();
        return cal.getTime();
    }

    /**
     * Creates calendar object out of this where date is today. Assumes that
     * date is tomorrow if time is before current time.
     * 
     * @return Calendar object
     */
    public Calendar getCalendar() {
        Calendar cal = getCalendarToday();

        if (isToday() == false) {
            cal.add(Calendar.DAY_OF_YEAR, 1);
        }
        return cal;
    }

    public Boolean isToday() {
        return isInFuture();
    }

    private Boolean isInFuture() {
        Calendar cal = getCalendarToday();
        Date now = new Date();
        return now.before(cal.getTime());
    }

    public int getHour() {
        return hours;
    }

    public int getMinute() {
        return minutes;
    }

    public SimpleTime addMinutes(int minutesToAdd) {
        return subMinutes(-minutesToAdd);
    }

    public SimpleTime subMinutes(int minutesToSubstract) {
        int newHour = hours;
        int newMin = minutes;
        newMin -= minutesToSubstract;

        // Handle subtraction
        while (newMin < 0) {
            newHour--;
            newMin += 60;
        }
        // Handle addition
        while (newMin > 59) {
            newHour++;
            newMin -= 60;
        }
        // Avoid negative hours
        if (newHour < 0)
            newHour += 24;
        // Avoid +23 hours
        if (newHour > 23)
            newHour -= 24;

        return new SimpleTime(newHour, newMin);

    }

    // public void setMinute(int minutes) {
    // this.minute = minutes;
    // }

    @Override
    public String toString() {
        String s = String
                .format(Locale.getDefault(), "%d:%02d", hours, minutes);
        return s;
    }

    /**
     * How minutes from now If time is in past then assumes that the time is for
     * tomorrow Rounds result down to minute: 9 min x sec => 9 min
     * 
     * @return how many minutes - always positive
     */
    public int minutesFrom(Calendar cal) {

        // Reset other time fields using a copy of calendar
        Calendar calWithoutSeconds = (Calendar) cal.clone();
        calWithoutSeconds.set(Calendar.SECOND, 0);
        calWithoutSeconds.set(Calendar.MILLISECOND, 0);

        float milliDiff = 0;

        if (calWithoutSeconds.get(Calendar.HOUR_OF_DAY) == hours
                && calWithoutSeconds.get(Calendar.MINUTE) == minutes) {
            milliDiff = 0;
        } else {
            milliDiff = getDate().getTime()
                    - calWithoutSeconds.getTimeInMillis();
        }
        return (int) Math.floor(milliDiff / 1000 / 60);

    }

    /**
     * How minutes from now If time is in past then assumes that the time is for
     * tomorrow Rounds result down to minute: 9 min x sec => 9 min
     * 
     * @return how many minutes - always positive
     */
    public int minutesFromNow() {
        Calendar cal = Calendar.getInstance();

        return minutesFrom(cal);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + hours;
        result = prime * result + minutes;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SimpleTime other = (SimpleTime) obj;
        if (hours != other.hours)
            return false;
        if (minutes != other.minutes)
            return false;
        return true;
    }

    private Calendar getCalendarToday() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, minutes);

        // Reset other time fields.
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }
}
