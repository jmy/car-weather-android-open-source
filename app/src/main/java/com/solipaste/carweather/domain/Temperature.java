/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public class Temperature extends BigDecimal {

    private static final int MAX_SENSIBLE_TEMP = 100;
    private static final int MIN_SENSIBLE_TEMP = -100;
    /**
     * The number of decimals when printing with fixed scale
     * 
     */
    public static final int NUMBER_OF_DECIMALS = 0;
    private static final long serialVersionUID = -1910388281657082610L;

    public static final int ERR_VALUE = 200;
    public static final String ERROR_VALUE_STRING = "--";

    /**
     * @throws NumberFormatException
     *             is val is not valid Temperature
     * @param val
     */
    public Temperature(String val) {
        super(val);
    }

    public Temperature(BigInteger val) {
        super(val);
    }

    public Temperature(int val) {
        super(val);
    }

    public Temperature(long val) {
        super(val);
    }

    public Temperature() {
        super(ERR_VALUE);
    }

    /**
     * Equals works as compareTo
     * 
     * @param x
     * @return
     */
    public boolean equals(Temperature x) {
        return this.compareTo(x) == 0 ? true : false;
    }

    /**
     * Is valid and sensible temperature value
     * 
     * @return
     */
    public boolean isValid() {
        return isInRangeInclusive(MIN_SENSIBLE_TEMP, MAX_SENSIBLE_TEMP);
    }

    public boolean isInRangeInclusive(Temperature min, Temperature max) {
        if (min.compareTo(max) == 1)
            return false;
        else
            return this.compareTo(min) >= 0 && this.compareTo(max) <= 0;
    }

    public boolean isInRangeInclusive(int min, int max) {
        return this.isInRangeInclusive(new Temperature(min), new Temperature(
                max));
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.math.BigDecimal#toString()
     */
    public String toScaledString() {
        String value;
        if (isValid()) {
            BigDecimal bd = setScale(NUMBER_OF_DECIMALS, RoundingMode.HALF_UP);
            if (this.compareTo(ZERO) == 1)
                value = "+" + bd.toString();
            else
                value = bd.toString();
        } else {
            value = ERROR_VALUE_STRING;
        }
        return value;
    }

}
