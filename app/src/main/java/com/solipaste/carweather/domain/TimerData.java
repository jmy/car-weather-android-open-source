/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import java.util.Calendar;

public class TimerData {
    /**
     * Defaults
     */
    private static final int DURATION_DEFAULT = 0;
    private static final int DURATION_MIN = 0;
    public static final int TIMER_PERIOD_DEFAULT = 120;
    public static final int TIMER_PERIOD_UNLIMITED = 24 * 60;

    /**
     * Defined by user.
     */
    private SimpleTime mReadyTime;

    /**
     * Heat on time. Calculated based on duration and ready time: mReadyTime -
     * mHeatDuration.
     */
    private SimpleTime mStartTime;

    /**
     * Calculated based on timer period and start time: mStartTime +
     * mTimerPeriod. Can be also set by user.
     */
    private SimpleTime mStopTime;

    /**
     * The optimal heat on time. Calculated based on temperature. Must be less
     * or equal than mTimerPeriod.
     */
    private int mHeatDuration = DURATION_DEFAULT;

    /**
     * Defines the max heating period. Defined by timer model used. Typically 2
     * hours. 1440 means 24h = unlimited.
     */
    private int mTimerPeriod = TIMER_PERIOD_DEFAULT;

    private boolean mIsValid = false;

    public TimerData() {
    }

    /**
     * Copy constructor
     * 
     * @param another
     */
    public TimerData(TimerData another) {
        mReadyTime = new SimpleTime(another.mReadyTime);
        mStartTime = new SimpleTime(another.mStartTime);
        mStopTime = new SimpleTime(another.mStopTime);
        mHeatDuration = another.mHeatDuration;
        mTimerPeriod = another.mTimerPeriod;
        mIsValid = another.mIsValid;
    }

    public boolean isValid() {
        return mIsValid;
    }

    /**
     * Check validity before call or check return value for null
     * 
     * @return
     */
    public SimpleTime getReadyTime() {
        return mReadyTime;
    }

    public void setReadyTime(SimpleTime readyTime) {
        mReadyTime = readyTime;
        mStartTime = mReadyTime.subMinutes(mHeatDuration);
        if (mTimerPeriod == TIMER_PERIOD_UNLIMITED)
            mStopTime = mReadyTime;
        else
            mStopTime = mStartTime.addMinutes(mTimerPeriod);
        mIsValid = true;
    }

    /**
     * Check validity before call or check return value for null
     * 
     * @return
     */
    public SimpleTime getStartTime() {
        return mStartTime;
    }

    /**
     * Set start time and calculate ready and stop times
     * 
     * @param startTime
     */
    public void setStartTime(SimpleTime startTime) {
        mStartTime = startTime;
        mReadyTime = startTime.addMinutes(mHeatDuration);
        if (mTimerPeriod == TIMER_PERIOD_UNLIMITED)
            mStopTime = mReadyTime;
        else
            mStopTime = startTime.addMinutes(mTimerPeriod);
        mIsValid = true;
    }

    /**
     * Check validity before call or check return value for null
     * 
     * @return
     */
    public SimpleTime getStopTime() {
        return mStopTime;
    }

    public SimpleTime getStopTimeFromNow() {
        int mins = mStopTime.minutesFromNow();
        SimpleTime t = new SimpleTime(mins);
        return t;
    }

    public int getDuration() {
        return mHeatDuration;
    }

    public String getDurationHoursMinString() {
        SimpleTime t = new SimpleTime(mHeatDuration);
        return t.toString();
    }

    public boolean setDuration(int duration) {
        if (duration >= DURATION_MIN && duration <= mTimerPeriod) {
            this.mHeatDuration = duration;
            if (mIsValid) {
                mStartTime = mReadyTime.subMinutes(duration);
                if (mTimerPeriod == TIMER_PERIOD_UNLIMITED)
                    mStopTime = mReadyTime;
                else
                    mStopTime = mStartTime.addMinutes(mTimerPeriod);
            }
            return true;
        } else {
            return false;
        }
    }

    public int getTimerPeriod() {
        return mTimerPeriod;
    }

    public void setTimerPeriod(int timerPeriod) {
        mTimerPeriod = timerPeriod;
        if (mIsValid) {
            if (mTimerPeriod == TIMER_PERIOD_UNLIMITED)
                mStopTime = mReadyTime;
            else
                mStopTime = mStartTime.addMinutes(timerPeriod);
        }
    }

    public void reset() {
        mIsValid = false;
        mHeatDuration = DURATION_DEFAULT;
        mTimerPeriod = TIMER_PERIOD_DEFAULT;
        mReadyTime = null;
        mStartTime = null;
        mStopTime = null;
    }

    public boolean isStartTimeInFuture() {

        Calendar readyTimeCal = mReadyTime.getCalendar();
        readyTimeCal.add(Calendar.MINUTE, -mHeatDuration);
        readyTimeCal.add(Calendar.SECOND, 59); // add some seconds so that time
                                               // with same hour and min digit
                                               // are still considered to be in
                                               // the future
        return readyTimeCal.after(Calendar.getInstance());
    }
}