/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

public enum WeatherSymbol {

    CLEAR(0), SNOW(1), SLEET(2);
    private int value;

    private WeatherSymbol(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    /**
     * Get weather symbol based on the yr.no weather code
     * http://om.yr.no/forklaring/symbol/
     * 
     * @param networkValue
     * @return
     */
    public static WeatherSymbol fromNetworkSymbol(int networkValue) {
        WeatherSymbol symbol = CLEAR;

        // Consider normal rain as clear

        switch (networkValue) {
        case 8:
        case 13:
        case 14:
            // case 18: // not used anymore
            // case 19: // not used anymore
        case 21:
            symbol = SNOW;
            break;
        case 7:
        case 12:
        case 20:
        case 23:
            symbol = SLEET;
            break;
        default:
            break;
        }
        return symbol;
    }
}
