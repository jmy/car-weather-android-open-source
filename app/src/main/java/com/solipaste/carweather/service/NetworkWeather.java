/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;

import android.content.Context;

import com.solipaste.carweather.app.Log;
import com.solipaste.carweather.app.R;
import com.solipaste.carweather.app.Utils;
import com.solipaste.carweather.domain.NetworkForecastData;
import com.solipaste.carweather.domain.Temperature;
import com.solipaste.carweather.domain.ForecastData;

public class NetworkWeather implements WeatherService {

    static final String TAG = NetworkWeather.class.getName();

    private static final String URL_PATH_SEPARATOR = "/";
    private static final String URL_SCHEME = "http";
    private static final String URL_USER_INFO = null;
    private static final String URL_HOST = "www.yr.no";
    // private static final String URL_HOST = "192.168.255.11";
    private static final int URL_PORT = -1;
    // private static final int URL_PORT= "8080";
    private static final String URL_PATH_PREFIX = "/place/";
    private static final String URL_PATH_POSTFIX = "/forecast_hour_by_hour.xml";

    private static final String CREDIT_TEMPLATE = "<a href=\"%2$s\">%1$s</a>";
    private static String mWeatherDataDisclaimerTemplateShort;

    private static final String CHARSET = "UTF-8";

    private static final int TIMEOUT_CONNECTION_MS = 5 * 1000;
    private static final int TIMEOUT_READ_MS = 10 * 1000;
    private static final int TIMEOUT_MULTIPLIER_FOR_BACKGROUND = 3;

    private final Context mContext;
    private final String mLongLocation;
    private final boolean mBackgroundUsage;

    private NetworkForecastData mData;

    private final boolean mFallbackToCachedData;

    /**
     * @param mContext
     *            Null if running in test mode
     * @param mLongLocation
     *            in format "Finland/Western_Finland/Tampere"
     * @param backgroundUsage
     *            weather used from UI or from background service
     * @param fallbackToCachedData
     */
    public NetworkWeather(Context context, String location,
            boolean backgroundUsage, boolean fallbackToCachedData) {
        Log.v(TAG, "NetworkWeather");

        mContext = context;
        mLongLocation = location;
        mBackgroundUsage = backgroundUsage;
        mFallbackToCachedData = fallbackToCachedData;

        if (mContext != null)
            mWeatherDataDisclaimerTemplateShort = context
                    .getString(R.string.weather_data_disclaimer_yrno);

        initilalizeTemperatureData();
    }

    private void initilalizeTemperatureData() {

        // Read mData from cache if available
        if (mContext != null && cleanAndTestCacheData()) {
            this.mData = NetworkForecastData.fromCache(mLongLocation,
                                                          mContext);
            Log.d(TAG, "Reusing cached data from " + mData.getTimeStamp());

        } else {
            Log.d(TAG, "No cached data available - start with empty data");
            this.mData = new NetworkForecastData();

        }
    }

    @Override
    public String getLongLocation() {
        return mLongLocation;
    }

    @Override
    public ForecastData getForecastData() {
        Log.v(TAG, "getTemperatureData");

        if (mData.isFreshData()) {
            Log.d(TAG,
                  "Fresh and valid data already loaded - just return the data");
        } else {
            Log.d(TAG,
                  "Stale or missing temperature data - let's try to reload it from the network");
            refreshWeatherData(null);
        }

        return mData;
    }

    @Override
    public Temperature getTemperature() {
        Log.v(TAG, "getTemperature(now)");

        // Ensure that fresh data is loaded
        getForecastData();

        // get current temperature
        return mData.getTemperature();
    }

    @Override
    public Temperature getTemperature(Date time) {
        Log.v(TAG, "getTemperature: " + time.toString());

        // Ensure that fresh data is loaded
        getForecastData();

        return mData.getTemperature(time);
    }

    @Override
    public void refreshWeatherData(Reader userReader) {
        Log.v(TAG, "refreshWeatherData: "
                + (userReader == null ? "from NW" : "using reader"));

        // Validate mLongLocation string
        // Require at least two parts: country, reg and city
        if (mLongLocation != null
                && mLongLocation.split(URL_PATH_SEPARATOR).length >= 3) {
            doRefreshWeatherDataFromNetwork(userReader);
        } else {
            Log.e(TAG, "Invalid long location: " + mLongLocation);
        }
    }

    private void doRefreshWeatherDataFromNetwork(Reader userReader) {
        Log.v(TAG, "doRefreshWeatherDataFromNetwork");

        Reader reader = null;

        // delete old mData
        mData.reset();

        URI uri = null;
        HttpURLConnection urlConnection = null;
        try {
            if (userReader == null) {

                // Check that network is up
                if (mContext != null && Utils.isNetworkAvailable(mContext)) {
                    
                    // Construct uri and convert it to then url
                    uri = new URI(URL_SCHEME, URL_USER_INFO, URL_HOST,
                            URL_PORT, URL_PATH_PREFIX + mLongLocation
                                    + URL_PATH_POSTFIX, null, null);

                    urlConnection = getConnection(uri);

                    InputStream in = urlConnection.getInputStream();

                    int responseStatusCode = urlConnection.getResponseCode();
                    Log.d(TAG, "Response status code from the server: "
                            + responseStatusCode);
                    reader = new InputStreamReader(in, CHARSET);
                    
                } else {
                    Log.e(TAG, "No network connection available...");
                }

            } else {
                // use given reader
                reader = userReader;
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Invalid URL: " + (uri != null ? uri : "null"));
            Log.e(TAG, e.getMessage());

        } catch (URISyntaxException e) {
            Log.e(TAG, "Invalid URI: " + (uri != null ? uri : "null"));
            Log.e(TAG, e.getMessage());

        } catch (IOException e) {
            Log.e(TAG, "Network connection error to "
                    + (uri != null ? uri : "null"));
            Log.e(TAG, e.toString());

        }

        if (reader != null) {
            // Network stream is available, try to read from it:
            // write data to temp cache first and then close the stream
            File file = NetworkForecastData.writeToCache(mLongLocation,
                                                            reader, mContext,
                                                            true);

            // Clean up NW connection
            if (urlConnection != null)
                urlConnection.disconnect();
            urlConnection = null;

            // Move temp cache to normal cache
            NetworkForecastData.moveTempToCache(file, mLongLocation,
                                                   mContext);

            // Try to read it back from the normal cache
            mData = NetworkForecastData.fromCache(mLongLocation, mContext);

        } else {
            // Network error:
            // Cleanup NW connection
            if (urlConnection != null)
                urlConnection.disconnect();
            urlConnection = null;

        }

        // Check for the validity
        if (mData.isValidData()) {
            Log.d(TAG,
                  "Valid data loaded from network on " + mData.getTimeStamp());

        } else if (mFallbackToCachedData) {
            // Reading data from network failed
            // We are allowed to use old cache so try to read from it
            // instead

            mData = NetworkForecastData.fromCache(mLongLocation, mContext);

            // Check for the validity
            if (mData.isValidData()) {
                Log.d(TAG,
                      "Fallback allowed: Valid fallback data loaded from cache on "
                              + mData.getTimeStamp());
            } else {
                Log.d(TAG, "Failed to load fallback data from cache");
                mData.reset();
            }

        } else {
            Log.d(TAG, "Failed to load data from network and cache");
            mData.reset();

        }
    }

    private HttpURLConnection getConnection(URI uri)
            throws MalformedURLException, IOException {

        HttpURLConnection urlConnection;
        URL url = uri.toURL();
        Log.v(TAG, "getConnection: " + url);

        urlConnection = (HttpURLConnection) url.openConnection();

        int timeoutMultiplier = (mBackgroundUsage ? TIMEOUT_MULTIPLIER_FOR_BACKGROUND
                : 1);
        urlConnection.setConnectTimeout(TIMEOUT_CONNECTION_MS
                * timeoutMultiplier);
        urlConnection.setReadTimeout(TIMEOUT_READ_MS * timeoutMultiplier);
        return urlConnection;
    }

    @Override
    public String getCredits(CreditsFormat format) {
        String credits;
        String formatString;
        if (format == CreditsFormat.longText)
            formatString = CREDIT_TEMPLATE;
        else
            formatString = mWeatherDataDisclaimerTemplateShort;

        final String text = mData.getCreditsText();
        final String url = mData.getCreditsUrl();
        if (text != null && url != null && formatString != null)
            credits = String.format(formatString, text, url);
        else
            credits = "";

        return credits;
    }

    /**
     * Cleanup all old cache data. Then check if fresh cached data exists for
     * the mLocation.
     * 
     * @param r
     * @return {@code true} if fresh data found
     */
    private boolean cleanAndTestCacheData() {// TODO tests missing

        if (mContext == null) {
            // using test data
            return false;
        }

        // First cleanup all old cache data
        deleteOldCaches(!mFallbackToCachedData);

        boolean isFresh = false;
        File cacheFile = NetworkForecastData.getCacheFile(mLongLocation,
                                                             mContext);
        if (cacheFile != null && cacheFile.exists()) {
            isFresh = true;
            Log.d(NetworkWeather.TAG,
                  "Fresh cache file found: " + cacheFile.getName());
        }
        return isFresh;
    }

    private void deleteOldCaches(boolean fallbackCachesAlso) {
        Log.v(TAG, "deleteOldCaches, fallbackCachesAlso: " + fallbackCachesAlso);

        Date now = new Date();
        File cacheDir = mContext.getCacheDir();
        String[] children = cacheDir.list();
        for (String aChildren : children) {
            File fileItem = new File(cacheDir, aChildren);
            if (fileItem != null
                    && fileItem.isFile()
                    && fileItem
                    .getName()
                    .startsWith(NetworkForecastData.CACHE_FILE_NAME_PREFIX)) {
                if (isStale(fileItem, now, !fallbackCachesAlso)) {
                    fileItem.delete();
                    Log.d(NetworkWeather.TAG, "Stale cache file deleted: "
                            + fileItem.getName());
                }
            }
        }
    }

    private boolean isStale(File fileItem, Date now, boolean forFallbackUse) {
        Log.v(TAG, "isStale: " + fileItem.getName() + ", forFallbackUse: "
                + forFallbackUse);

        final long maxAge = forFallbackUse ? NetworkForecastData.CACHE_FALLBACK_VALIDITY_PERIOD
                : NetworkForecastData.CACHE_VALIDITY_PERIOD;
        return now.getTime() - fileItem.lastModified() > maxAge;
    }

    /**
     * @param testData
     */
    public void setTestData(NetworkForecastData testData) {
        this.mData = testData;

    }

}
