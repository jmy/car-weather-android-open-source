/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.service;

import java.io.Reader;
import java.util.Date;
import java.util.Random;

import com.solipaste.carweather.domain.ForecastData;
import com.solipaste.carweather.domain.LocalForecastData;
import com.solipaste.carweather.domain.Temperature;

public class RandomWeather implements WeatherService {

    int min;
    int max;
    private final ForecastData data;

    public RandomWeather(int min, int max) {
        data = new LocalForecastData(new Temperature(
                new Random().nextInt(max - min) + min));
        this.min = min;
        this.max = max;
    }

    @Override
    public Temperature getTemperature() {
        return new Temperature(new Random().nextInt(max - min) + min);
    }

    @Override
    public String getLongLocation() {
        return "random";
    }

    @Override
    public Temperature getTemperature(Date time) {
        return getTemperature();
    }

    @Override
    public void refreshWeatherData(Reader r) {
    }

    @Override
    public String getCredits(CreditsFormat format) {
        return "using random data";
    }

    @Override
    public ForecastData getForecastData() {
        return data;
    }

}
