/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.service;

import java.io.Reader;
import java.util.Date;

import com.solipaste.carweather.domain.ForecastData;
import com.solipaste.carweather.domain.LocalForecastData;
import com.solipaste.carweather.domain.Temperature;
import com.solipaste.carweather.domain.WeatherSymbol;

public class UserWeather implements WeatherService {

    ForecastData data;

    public UserWeather(Temperature temp) {
        data = new LocalForecastData(temp);
    }

    public UserWeather(Temperature temp, WeatherSymbol sym) {
        data = new LocalForecastData(temp, sym);
    }
    
    @Override
    public String getLongLocation() {
        return "test";
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.solipaste.carweather.domain.WeatherService#temperature(android.text
     * .format .Time)
     */
    @Override
    public Temperature getTemperature(Date time) {
        return getTemperature();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.solipaste.carweather.domain.WeatherService#temperature()
     */
    @Override
    public Temperature getTemperature() {
        return data.getTemperature();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.solipaste.carweather.domain.WeatherService#refreshWeatherData()
     */
    @Override
    public void refreshWeatherData(Reader r) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.solipaste.carweather.domain.WeatherService#getCredits()
     */
    @Override
    public String getCredits(CreditsFormat format) {
        return "http://m.yr.no/place/Greenland/Sermersooq/Nuuk/";
    }

    @Override
    public ForecastData getForecastData() {
        return data;
    }

}
