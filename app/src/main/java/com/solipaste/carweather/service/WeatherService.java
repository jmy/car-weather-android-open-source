/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.service;

import java.io.Reader;
import java.util.Date;

import com.solipaste.carweather.domain.Temperature;
import com.solipaste.carweather.domain.ForecastData;

public interface WeatherService {

    public enum CreditsFormat {
        shortText, longText
    };

    /**
     * @return current temperate
     */
    public Temperature getTemperature();

    public String getLongLocation();

    public ForecastData getForecastData();

    /**
     * @param time
     * @return temperature at some point of time
     */
    public Temperature getTemperature(Date time);

    /**
     * Force refreshing weather data
     * 
     * @param r
     *            optional Reader object to be used
     */
    public void refreshWeatherData(Reader r);
    
    /**
     * Get credit texts
     * 
     * @param format
     *            Type of credits data using {@link CreditsFormat}
     * @return HTML string or null if no mWeatherCredits available
     */
    public String getCredits(CreditsFormat format);

}
