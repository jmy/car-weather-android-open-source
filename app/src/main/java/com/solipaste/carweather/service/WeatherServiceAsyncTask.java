/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.service;

import android.content.Context;
import android.os.AsyncTask;

import com.solipaste.carweather.app.Log;
import com.solipaste.carweather.domain.ForecastData;
import com.solipaste.carweather.domain.Temperature;
import com.solipaste.carweather.domain.WeatherSymbol;

public class WeatherServiceAsyncTask extends
        AsyncTask<WeatherService, Void, ForecastData> {
    private final static String TAG = WeatherServiceAsyncTask.class.getName();

    private final WeatherServiceCallback mCallback;

    public WeatherServiceAsyncTask(WeatherServiceCallback callback) {
        super();
        mCallback = callback;
    }

    @Override
    protected void onPostExecute(ForecastData data) {
        Log.v(TAG, "onPostExecute");

        // Do nothing if cancelled
        if (!isCancelled()) {

            if (data != null && data.isValidData()) {
                mCallback.setForecastData(data);
                mCallback.onSuccess();
            } else {
                mCallback.onFailure();
            }

        }
    }

    @Override
    protected ForecastData doInBackground(WeatherService... weather) {
        Log.v(TAG, "doInBackground");

        ForecastData forecast = null;

        if (!isCancelled()) {
            // Get temperature data
            forecast = weather[0].getForecastData();
        }
        return forecast;
    }

    /**
     * @param longLocation
     * @param backgroundUsage
     *            are we calling from UI or from service
     * @return
     */
    public static WeatherService getWeatherService(Context context,
            final String longLocation, boolean backgroundUsage,
            boolean fallbackToCachedData) {
        WeatherService w = null;

        if (longLocation.startsWith("*test")) {
            Log.d(TAG, "Using test data: " + longLocation);
            int temp = -4;
            String[] splits = longLocation.split("\\s");
            if (splits.length == 2)
                try {
                    temp = Integer.parseInt(splits[1]);
                } catch (NumberFormatException e1) {
                    // Use default
                }
            w = new UserWeather(new Temperature(temp));

        } else if (longLocation.startsWith("*snow")) {
            Log.d(TAG, "Using test data: " + longLocation);
            w = new UserWeather(new Temperature(-30), WeatherSymbol.SNOW);

        } else if (longLocation.startsWith("*sleet")) {
            Log.d(TAG, "Using test data: " + longLocation);
            w = new UserWeather(new Temperature(-30), WeatherSymbol.SLEET);
            
        } else
            w = new NetworkWeather(context, longLocation, backgroundUsage,
                    fallbackToCachedData);

        return w;
    };

}
