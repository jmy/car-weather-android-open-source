/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.solipaste.carweather.domain.Heater.HeaterType;

/**
 * Updated based on Motiva 2013 recommendations:
 * http://motiva.fi/liikenne/henkiloautoilu/taloudellinen_ajotapa/moottorin_esilammitys
 * 
 */
public class HeatOptimizerTest {

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {

    }

    /**
     * Test method for
     * {com.solipaste.carweather.domainather.domain.HeatOptimizer
     * #opticom.solipaste.carweather.domain.carweather.domain.Heater,
     * Temperature, int)} .
     */
    @Test
    public void optimalDuration_BlockSummerConditions_DurationZero() {
        Heater h = new Heater(HeaterType.BLOCK);
        final Temperature t = new Temperature(30);
        assertEquals(0, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_BlockBarelyColdConditions_ZeroDuration() {
        Heater h = new Heater(HeaterType.BLOCK);
        final Temperature t = new Temperature(5);
        assertEquals(0, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_BlockAlmostFreezingConditions_DurationZero() {
        Heater h = new Heater(HeaterType.BLOCK);
        final Temperature t = new Temperature("0.1");
        assertEquals(0, HeatOptimizer.optimalDuration(h, t, 0));
    }
    
    @Test
    public void optimalDuration_BlockFreezingConditions_ShortDuration() {
        Heater h = new Heater(HeaterType.BLOCK);
        final Temperature t = new Temperature(0);
        assertEquals(30, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_BlockColdish_ShortDuration() {
        Heater h = new Heater(HeaterType.BLOCK);
        final Temperature t = new Temperature("-4.9");
        assertEquals(30, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_BlockColdish2_ShortDuration() {
        Heater h = new Heater(HeaterType.BLOCK);
        final Temperature t = new Temperature(-5);
        assertEquals(30, HeatOptimizer.optimalDuration(h, t, 0));
    }
    
    @Test
    public void optimalDuration_BlockCold_LongishDuration() {
        Heater h = new Heater(HeaterType.BLOCK);
        final Temperature t = new Temperature("-5.1");
        assertEquals(60, HeatOptimizer.optimalDuration(h, t, 0));
    }
    
    @Test
    public void optimalDuration_BlockCold2_LongishDuration() {
        Heater h = new Heater(HeaterType.BLOCK);
        final Temperature t = new Temperature("-9.9");
        assertEquals(60, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_BlockCold3_LongishDuration() {
        Heater h = new Heater(HeaterType.BLOCK);
        final Temperature t = new Temperature(-10);
        assertEquals(60, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_BlockVeryCold_LongDuration() {
        Heater h = new Heater(HeaterType.BLOCK);
        final Temperature t = new Temperature("-10.1");
        assertEquals(2 * 60, HeatOptimizer.optimalDuration(h, t, 0));
    }
    
    @Test
    public void optimalDuration_BlockVeryCold2_LongDuration() {
        Heater h = new Heater(HeaterType.BLOCK);
        final Temperature t = new Temperature(-20);
        assertEquals(2 * 60, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_BlockExtraCold_LongDuration() {
        Heater h = new Heater(HeaterType.BLOCK);
        final Temperature t = new Temperature(-50);
        assertEquals(2 * 60, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_RadiatorSummerConditions_DurationZero() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature(30);
        assertEquals(0, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_RadiatorBarelyColdConditions_DurationZero() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature("5.1");
        assertEquals(0, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_RadiatorAlmostFreezingConditions_DurationZero() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature("0.1");
        assertEquals(0, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_RadiatorZeroConditions_ShortDuration() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature(0);
        assertEquals(60, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_RadiatorColdish_ShortDuration() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature("-4.9");
        assertEquals(60, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_RadiatorColdish2_ShortDuration() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature("-5");
        assertEquals(60, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_RadiatorCold_LongishDuration() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature("-5.1");
        assertEquals(2 * 60, HeatOptimizer.optimalDuration(h, t, 0));
    }
    
    @Test
    public void optimalDuration_RadiatorCold2_LongishDuration() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature("-9.9");
        assertEquals(2 * 60, HeatOptimizer.optimalDuration(h, t, 0));
    }
    
    @Test
    public void optimalDuration_RadiatorCold3_LongishDuration() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature(-10);
        assertEquals(2 * 60, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_RadiatorVeryCold_LongDuration() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature("-10.1");
        assertEquals(3 * 60, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_RadiatorVeryCold2_LongDuration() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature(-20);
        assertEquals(3 * 60, HeatOptimizer.optimalDuration(h, t, 0));
    }
    
    @Test
    public void optimalDuration_RadiatorExtraCold_LongDuration() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature(-50);
        assertEquals(3 * 60, HeatOptimizer.optimalDuration(h, t, 0));
    }

    @Test
    public void optimalDuration_RadiatorExtraColdWithLimit1_LongDuration() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature(-50);
        assertEquals(1, HeatOptimizer.optimalDuration(h, t, 1));
    }   

    @Test
    public void optimalDuration_RadiatorExtraColdWithLargeLimit_LongDuration() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature(-50);
        assertEquals(3 * 60, HeatOptimizer.optimalDuration(h, t, 600));
    }

    @Test
    public void optimalDuration_RadiatorExtraColdWithSameLimit_LongDuration() {
        Heater h = new Heater(HeaterType.RADIATOR);
        final Temperature t = new Temperature(-50);
        assertEquals(3 * 60, HeatOptimizer.optimalDuration(h, t, 3 * 60));
    }

}
