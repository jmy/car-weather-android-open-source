/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.solipaste.carweather.domain.Heater.HeaterType;

public class HeaterTest {

    @Test
    public void heater_Block() throws Exception {
        Heater h = new Heater(HeaterType.BLOCK);
        assertTrue(h.getType() == HeaterType.BLOCK);
    }

    @Test
    public void heater_Radiator() throws Exception {
        Heater h = new Heater(HeaterType.RADIATOR);
        assertTrue(h.getType() == HeaterType.RADIATOR);
    }

    @Test
    public void SetType_FromBlockToRadiator() throws Exception {
        Heater h = new Heater(HeaterType.RADIATOR);
        h.setType(HeaterType.BLOCK);
        assertTrue(h.getType() == HeaterType.BLOCK);
    }

    @Test
    public void SetType_FromRadiatorToBlock() throws Exception {
        Heater h = new Heater(HeaterType.BLOCK);
        h.setType(HeaterType.RADIATOR);
        assertTrue(h.getType() == HeaterType.RADIATOR);
    }

}
