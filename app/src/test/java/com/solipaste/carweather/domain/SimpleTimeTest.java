/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class SimpleTimeTest {

    private SimpleTime t2;
    private SimpleTime t1;

    @Before
    public void setUp() throws Exception {
        t1 = new SimpleTime(1, 1);
        t2 = new SimpleTime(22, 22);
    }

    @Test
    public void testGetDate() {
        Date d = t1.getDate();
        assertTrue("hours", d.getHours() == 1);
        assertTrue("minutes", d.getMinutes() == 1);
    }

    @Test
    public void test2400() {
        SimpleTime t24 = new SimpleTime(24, 00);
        assertTrue("hours", t24.getHour() == 0);
        assertTrue("minutes", t24.getMinute() == 0);
    }

    @Test
    public void test0000() {
        SimpleTime t24 = new SimpleTime(00, 00);
        assertTrue("hours", t24.getHour() == 0);
        assertTrue("minutes", t24.getMinute() == 0);
    }

    @Test
    public void test2359() {
        SimpleTime t24 = new SimpleTime(23, 59);
        assertTrue("hours", t24.getHour() == 23);
        assertTrue("minutes", t24.getMinute() == 59);
    }

    @Test
    public void test0001() {
        SimpleTime t24 = new SimpleTime(00, 01);
        assertTrue("hours", t24.getHour() == 00);
        assertTrue("minutes", t24.getMinute() == 01);
    }

    @Test
    public void testFromString() {
        SimpleTime t24 = new SimpleTime("00:01");
        assertTrue("hours", t24.getHour() == 00);
        assertTrue("minutes", t24.getMinute() == 01);
    }

    @Test
    public void testFromMin0() {
        SimpleTime t24 = new SimpleTime(0);
        assertEquals("hours", 0, t24.getHour());
        assertEquals("minutes", 0, t24.getMinute());
    }

    @Test
    public void testFromMin1() {
        SimpleTime t24 = new SimpleTime(1);
        assertEquals("hours", 0, t24.getHour());
        assertEquals("minutes", 1, t24.getMinute());
    }

    @Test
    public void testFromMin60() {
        SimpleTime t24 = new SimpleTime(60);
        assertEquals("hours", 1, t24.getHour());
        assertEquals("minutes", 0, t24.getMinute());
    }

    @Test
    public void testFromMin61() {
        SimpleTime t24 = new SimpleTime(61);
        assertEquals("hours", 1, t24.getHour());
        assertEquals("minutes", 1, t24.getMinute());
    }

    @Test
    public void testFromMin120() {
        SimpleTime t24 = new SimpleTime(120);
        assertEquals("hours", 2, t24.getHour());
        assertEquals("minutes", 0, t24.getMinute());
    }
    
    @Test
    public void testFromMinAlmostFullDay() {
        SimpleTime t24 = new SimpleTime(24 * 60 - 1);
        assertEquals("hours", 23, t24.getHour());
        assertEquals("minutes", 59, t24.getMinute());
    }

    @Test
    public void testFromMinInvalid() {
        SimpleTime t24 = new SimpleTime(24 * 60);
        assertEquals("hours", 0, t24.getHour());
        assertEquals("minutes", 0, t24.getMinute());
    }
    
    @Test
    public void testFromString0000() {
        SimpleTime t24 = new SimpleTime("00:00");
        assertTrue("hours", t24.getHour() == 00);
        assertTrue("minutes", t24.getMinute() == 00);
    }

    @Test
    public void testFromString2400() {
        SimpleTime t24 = new SimpleTime("24:00");
        assertTrue("hours", t24.getHour() == 00);
        assertTrue("minutes", t24.getMinute() == 00);
    }

    @Test
    public void testFromStringThrows() {
        try {
            SimpleTime t24 = new SimpleTime("24.00");
            assertFalse(true);
        } catch (Exception e) {
            assertTrue(true);
        }

    }

    @Test
    public void testGetDateTomorrow() {
        Date d = t2.getDate();
        assertTrue("hours", d.getHours() == 22);
        assertTrue("minutes", d.getMinutes() == 22);
    }

    @Test
    public void testIsToday_False() {
        Date now = new Date();
        if (now.getHours() > t1.getHour())
            assertFalse("tomorrow", t1.isToday());
        else
            assertTrue("today", t1.isToday());
    }

    @Test
    public void testIsToday_True() {
        Date now = new Date();
        if (now.getHours() > t2.getHour()
                || (now.getHours() == t2.getHour() && now.getMinutes() > t2
                        .getMinute()))
            assertFalse("tomorrow", t2.isToday());
        else
            assertTrue("today", t2.isToday());
    }

    @Test
    public void testAddMinutes_NoOverflow() {
        SimpleTime t3 = t1.addMinutes(10);
        assertEquals(1, t1.getMinute());
        assertEquals(1, t1.getHour());
        assertEquals(11, t3.getMinute());
        assertEquals(1, t3.getHour());
    }

    @Test
    public void testAddMinutes_OverflowMinutes() {
        SimpleTime t3 = t2.addMinutes(40);
        assertEquals(22, t2.getMinute());
        assertEquals(22, t2.getHour());
        assertEquals(2, t3.getMinute());
        assertEquals(23, t3.getHour());
    }

    @Test
    public void testAddMinutes_OverflowHours() {
        SimpleTime t3 = t2.addMinutes(100);
        assertEquals(22, t2.getMinute());
        assertEquals(22, t2.getHour());
        assertEquals(2, t3.getMinute());
        assertEquals(0, t3.getHour());
    }

    @Test
    public void testAddMinutes_Negative_NoOverflow() {
        SimpleTime t3 = t1.addMinutes(-1);
        assertEquals(22, t2.getMinute());
        assertEquals(22, t2.getHour());
        assertEquals(0, t3.getMinute());
        assertEquals(1, t3.getHour());
    }

    @Test
    public void testAddMinutes_Negative_OverflowMinutes() {
        SimpleTime t3 = t1.addMinutes(-21);
        assertEquals(1, t1.getMinute());
        assertEquals(1, t1.getHour());
        assertEquals(40, t3.getMinute());
        assertEquals(0, t3.getHour());
    }

    @Test
    public void testAddMinutes_Negative_OverflowHours() {
        SimpleTime t3 = t1.addMinutes(-81);
        assertEquals(1, t1.getMinute());
        assertEquals(1, t1.getHour());
        assertEquals(40, t3.getMinute());
        assertEquals(23, t3.getHour());
    }

    @Test
    public void testSubMinutes() {
        SimpleTime t3 = t1.addMinutes(-82);
        SimpleTime t4 = t1.subMinutes(82);
        assertEquals(t3, t4);
    }

    @Test
    public void testToString() {
    }

    @Test
    public void testMinutesFromNow_Today_10Minutes() {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = (Calendar) c1.clone();

        c1.add(Calendar.MINUTE, 10);
        SimpleTime t = new SimpleTime(c1);
        assertEquals(10, t.minutesFrom(c2));
    }

    @Test
    public void testMinutesFromNow_Today_0Minutes() {
        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.SECOND, 0);
        c1.set(Calendar.MILLISECOND, 0);
        SimpleTime t = new SimpleTime(c1);
        assertEquals(0, t.minutesFromNow());
    }

    @Test
    public void testMinutesFrom_Tomorrow_23hours() {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = (Calendar) c1.clone();

        c1.add(Calendar.HOUR, -1); // past times interpreted as future time
        SimpleTime t = new SimpleTime(c1);
        assertEquals(60 * 23, t.minutesFrom(c2));
    }

    @Test
    public void testMinutesFrom_Tomorrow_23hoursAnd59Minutes() {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = (Calendar) c1.clone();

        c1.add(Calendar.MINUTE, -1); // past times interpreted as future time
        SimpleTime t = new SimpleTime(c1);
        assertEquals(60 * 23 + 59, t.minutesFrom(c2));
    }

    @Test
    public void testMinutesFrom_Tomorrow_Minus20s() {
        Calendar nowMinus20s = Calendar.getInstance();
        nowMinus20s.set(Calendar.SECOND, 0);
        nowMinus20s.set(Calendar.MILLISECOND, 0);
        Calendar now = (Calendar) nowMinus20s.clone();

        nowMinus20s.add(Calendar.SECOND, -20);
        SimpleTime t = new SimpleTime(nowMinus20s);
        assertEquals(60 * 23 + 59, t.minutesFrom(now));
    }

    @Test
    public void testMinutesFromNow_Tomorrow_Minus31s() {
        Calendar nowMinus31s = Calendar.getInstance();
        nowMinus31s.set(Calendar.SECOND, 0);
        nowMinus31s.set(Calendar.MILLISECOND, 0);
        Calendar now = (Calendar) nowMinus31s.clone();

        nowMinus31s.add(Calendar.SECOND, -31);
        Date time = nowMinus31s.getTime();
        SimpleTime t = new SimpleTime(time);
        assertEquals(60 * 23 + 59, t.minutesFrom(now));
    }

    @Test
    public void testMinutesFromNow_Now() {
        Date now = Calendar.getInstance().getTime();
        SimpleTime t = new SimpleTime(now);
        assertEquals(0, t.minutesFromNow());
    }

    @Test
    public void testMinutesFromNow_NowPlus20s() {
        Calendar nowPlus20s = Calendar.getInstance();
        nowPlus20s.set(Calendar.SECOND, 0);
        nowPlus20s.set(Calendar.MILLISECOND, 0);
        nowPlus20s.add(Calendar.SECOND, 20);
        Date time = nowPlus20s.getTime();
        SimpleTime t = new SimpleTime(time);
        assertEquals(0, t.minutesFromNow());
    }

    @Test
    public void testMinutesFromNow_NowPlus31s() {
        Calendar nowPlus31s = Calendar.getInstance();
        nowPlus31s.set(Calendar.SECOND, 0);
        nowPlus31s.set(Calendar.MILLISECOND, 0);
        nowPlus31s.add(Calendar.SECOND, 31);
        Date time = nowPlus31s.getTime();
        SimpleTime t = new SimpleTime(time);
        assertEquals(0, t.minutesFromNow());
    }

}
