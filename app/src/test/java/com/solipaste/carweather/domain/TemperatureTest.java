/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

public class TemperatureTest {

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void isInRangeInclusive_Int_True() {
        Temperature t = new Temperature("1.08");
        assertTrue(t.isInRangeInclusive(1,2));
    }

    @Test
    public void isInRangeInclusive_IntWithNegative_True() {
        Temperature t = new Temperature("-0.08");
        assertTrue(t.isInRangeInclusive(-1,2));
    }
    
    @Test
    public void isInRangeInclusive_IntNotInRange_False() {
        Temperature t = new Temperature("-1.08");
        assertTrue(!t.isInRangeInclusive(-1,2));
    }
    
    @Test
    public void isInRangeInclusive_IntWithInvalidRange_False() {
        Temperature t = new Temperature("-0.08");
        assertTrue(!t.isInRangeInclusive(2,-2));
    }
    
    @Test
    public void isInRangeInclusive_TempRange_True() {
        Temperature t = new Temperature("-1.08");
        Temperature min = new Temperature("-1.08");
        Temperature max = new Temperature("1.08");
        assertTrue(t.isInRangeInclusive(min, max));
    }

    @Test
    public void toScaledString_TooLongPrecision_RoundedStringUp() {
        Temperature t = new Temperature("1.5");
        assertTrue("Should equal", t.toScaledString().equals("+2"));
    }

    @Test
    public void toScaledString_TooLongPrecision_RoundedStringDown() {
        Temperature t = new Temperature("1.49");
        assertTrue("Should equal", t.toScaledString().equals("+1"));
    }
    
    
    @Test
    public void toScaledString_TooShortPrecision_Scaled() {
        Temperature t = new Temperature("1");
        assertTrue("Should equal", t.toScaledString().equals("+1"));
    }
    
    @Test
    public void toScaledString_Zero_Scaled() {
        Temperature t = new Temperature("0.0");
        assertTrue("Should equal", t.toScaledString().equals("0"));
    }
    
    @Test
    public void equals_SameValueWithDifferentPrecision_True() {
        Temperature t1 = new Temperature("1");
        Temperature t2 = new Temperature("1.0");
        assertTrue(t1.equals(t2));
    }

    @Test
    public void equals_DifferentValue_False() {
        Temperature t1 = new Temperature("1");
        Temperature t2 = new Temperature("1.01");
        assertTrue(!t1.equals(t2));
    }

    @Test
    public void TemperatureConstructor_String() {
        Temperature t = new Temperature("1.8");
        assertTrue(t.toString().equals("1.8"));
    }

    @Test
    public void TemperatureConstructor_Int() {
        Temperature t = new Temperature(1);
        assertTrue(t.toString().equals("1"));
    }

    @Test
    public void TemperatureConstructor_Long() {
        Temperature t = new Temperature(1L);
        assertTrue(t.toString().equals("1"));
    }

    @Test
    public void TemperatureConstructor_BigInt() {
        Temperature t = new Temperature(new BigInteger("1"));
        assertTrue(t.toString().equals("1"));
    }

    @Test
    public void isValid_valid_true() {
        Temperature t = new Temperature("0");
        assertTrue(t.isValid());
    }

    @Test
    public void isValid_max_true() {
        Temperature t = new Temperature("100");
        assertTrue(t.isValid());
    }
    
    @Test
    public void isValid_min_true() {
        Temperature t = new Temperature("-100");
        assertTrue(t.isValid());
    }
    
    @Test
    public void isValid_low_false() {
        Temperature t = new Temperature("-100.1");
        assertTrue(!t.isValid());
    }
    
    @Test
    public void isValid_high_false() {
        Temperature t = new Temperature("100.1");
        assertTrue(!t.isValid());
    }
}
