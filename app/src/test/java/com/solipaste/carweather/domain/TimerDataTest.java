/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimerDataTest {

    @Test
    public void getStopTimeFromNow_60min_120timer_2h00min() throws Exception {
        TimerData t = new TimerData();
        t.setTimerPeriod(120);
        t.setReadyTime(new SimpleTime().addMinutes(60));
        assertTrue(t.setDuration(60));
        assertEquals(new SimpleTime("2:00"), t.getStopTimeFromNow());
    }

    @Test
    public void getStopTimeFromNow_60min_60timer_1h00min() throws Exception {
        TimerData t = new TimerData();
        t.setTimerPeriod(60);
        t.setReadyTime(new SimpleTime().addMinutes(60));
        assertTrue(t.setDuration(60));
        assertEquals(new SimpleTime("1:00"), t.getStopTimeFromNow());
    }

    @Test
    public void getStopTimeFromNow_30min_120timer_4h30min() throws Exception {
        TimerData t = new TimerData();
        t.setReadyTime(new SimpleTime().addMinutes(180));
        assertTrue(t.setDuration(30));
        assertEquals(new SimpleTime("4:30"), t.getStopTimeFromNow());
    }

    @Test
    public void getStopTimeFromNow_60min_unlimited_1h00min() throws Exception {
        TimerData t = new TimerData();
        t.setTimerPeriod(TimerData.TIMER_PERIOD_UNLIMITED);
        t.setReadyTime(new SimpleTime().addMinutes(60));
        assertTrue(t.setDuration(60));
        assertEquals(new SimpleTime("1:00"), t.getStopTimeFromNow());
    }
    
    @Test
    public void getStopTimeFromNow_30min_unlimited_0h30min() throws Exception {
        TimerData t = new TimerData();
        t.setTimerPeriod(TimerData.TIMER_PERIOD_UNLIMITED);
        t.setReadyTime(new SimpleTime().addMinutes(30));
        assertTrue(t.setDuration(30));
        assertEquals(new SimpleTime("0:30"), t.getStopTimeFromNow());
    }
    
    @Test
    public void getStopTimeFromNow_50min_unlimited_0h30min() throws Exception {
        TimerData t = new TimerData();
        t.setTimerPeriod(TimerData.TIMER_PERIOD_UNLIMITED);
        t.setReadyTime(new SimpleTime().addMinutes(30));
        assertTrue(t.setDuration(50));
        assertEquals(new SimpleTime("0:30"), t.getStopTimeFromNow());
    }
    
}
