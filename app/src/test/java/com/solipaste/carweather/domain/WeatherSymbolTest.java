/*
 *    Copyright 2015 Solipaste Oy
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.solipaste.carweather.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class WeatherSymbolTest {

    @Test
    public void fromNetworkSymbol_snow() {
        final WeatherSymbol SNOW = WeatherSymbol.SNOW;

        WeatherSymbol symbol = WeatherSymbol.fromNetworkSymbol(8);
        assertEquals(SNOW, symbol);
        symbol = WeatherSymbol.fromNetworkSymbol(13);
        assertEquals(SNOW, symbol);
        symbol = WeatherSymbol.fromNetworkSymbol(14);
        assertEquals(SNOW, symbol);
        symbol = WeatherSymbol.fromNetworkSymbol(21);
        assertEquals(SNOW, symbol);
    }

    @Test
    public void fromNetworkSymbol_sleet() {
        final WeatherSymbol SLEET = WeatherSymbol.SLEET;

        WeatherSymbol symbol = WeatherSymbol.fromNetworkSymbol(7);
        assertEquals(SLEET, symbol);
        symbol = WeatherSymbol.fromNetworkSymbol(12);
        assertEquals(SLEET, symbol);
        symbol = WeatherSymbol.fromNetworkSymbol(20);
        assertEquals(SLEET, symbol);
        symbol = WeatherSymbol.fromNetworkSymbol(23);
        assertEquals(SLEET, symbol);
    }

    @Test
    public void fromNetworkSymbol_clearorrain() {
        final WeatherSymbol CLEAR_OR_RAIN= WeatherSymbol.CLEAR;

        WeatherSymbol symbol = WeatherSymbol.fromNetworkSymbol(1);
        assertEquals(CLEAR_OR_RAIN, symbol);
        symbol = WeatherSymbol.fromNetworkSymbol(5);
        assertEquals(CLEAR_OR_RAIN, symbol);
        symbol = WeatherSymbol.fromNetworkSymbol(50);
        assertEquals(CLEAR_OR_RAIN, symbol);
    }

}
