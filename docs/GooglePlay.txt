Google Play Descriptions

English
======= 

Car Weather makes pre-heathing car during winter fun and more reliable.  Never let winter weather suprise you anymore!<br/>
Use Car Weather to take care of your car and environment by avoiding cold starts at winter. Save money on your electricity bill by following the local outdoor temperature based engine pre-heating duration instruction given by this application.  Let Car Weather remind you in advance when temperature drops below zero, and use of block heater is recommended.

Car Weather works with all types of engine and indoor car heating systems.  You get most out of the application by using it with a timer switch.  You only need to set the departure time, and Car Weather then shows you the following information:

- local temperature forecast for the time of your departure
- for how long you should pre-heat your car
- at what time to start and stop pre-heating
- how long (in hours and minutes) it is to the heating-off time

This application uses car engine pre-heating recommendations from Motiva Ltd, a Finnish expert company promoting efficient and sustainable use of energy and materials.  You can select the type of engine heather, block heather or radiator heater, to further improve the accuracy of calculated pre-heating recommendations.

This version of Car Weather shows weather forecast for Finnish locations.

Promo-text
=====

Car Weather makes pre-heathing car during winter fun and more reliable!


Version
======

New in version 1.4.0:

The pre-heating durations are now based on updated recommendations for electrical car engine pre-heaters published by Motiva Ltd in October 2013.  Select block engine heater in the application settings for shorter or radiator engine heater for longer pre-heating duration.

--
New in version 1.3.0:

- Show snow warning also when sleet is forecasted
- Fixed snow warning sometimes appearing on dry weather
- Some other minor optimizations and improvements<br/>
Follow and participate on Car Weather development now on Twitter: 
https://twitter.com/solipaste
--
New in version 1.2.0:

- Snow warning is shown when snow or sleet is forecasted at the time of departure
- Departure time is rounded automatically to nearest 5 minutes
- Pre-heating start time is shown in red if you select departure time that is too early for optimal pre-heating
- Various bug fixes<br/>
Follow and participate on Car Weather development now on Twitter: 
https://twitter.com/solipaste
--
New in version 1.1.0:

- Introduced new notification type: departure time notification
- Updated the location list 
- Improved notification realiability (requires new "Prevent phone from sleeping" permission)
- Localization improvements and bug fixes

Follow and participate on Car Weather development now on Twitter: 
https://twitter.com/solipaste

Suomi 
===== 

Car Weather tekee auton esilämmityksestä helppoa ja luotettavaa.  Älä anna pakkasaamujen yllättää sinua!<br/>
Käytä Car Weatheriä välttämään kylmäkäynnistyksiä talvella ja pitämään huolta sekä autostasi ja ympäristöstä.  Voit säästää jopa kymmeniä euroja sähkölaskussa jo yhden talven aikana käyttämällä moottori- ja sisätilalämmitintä oikeaoppisesti sovelluksen antamien ohjeiden mukaan.  Anna Car Weatherin muistuttaa sinua etukäteen autosi lämmitystolppaan kytkemisestä, mikäli lämpötila sääennusteen mukaan on pakkaselle kun seuraavan kerran olet lähdössä liikkeelle.

Car Weather toimii kaikenmallisten moottori- ja sisätilalämmittimien kanssa.  Parhaan hyödyn sovelluksesta saat käyttämällä sitä yhdessä ajastinkytkimen kanssa. Asetat vain suunnittelemasi lähtöajan ja Car Weather kertoo sinulle

- paikallisen lämpötilaennusteen lähtöhetkelle,
- optimaalisen esilämmitysajan pituuden,
- milloin esilämmitys pitää aloittaa ja lopettaa sekä
- kuinka monen tunnin ja minuutin kuluttua lämmitys pitää lopettaa.

Sovellus käyttää Motivan antamia suosituksia henkilöauton moottorin esilämmitykselle Pohjoismaissa. Halutessasi voit valita moottorilämmittimen tyypiksi joko lohkolämmittimen tai säteilylämmittimen.

Tämä versio Car Weatheristä näyttää sääennusteen paikkakunnille Suomessa.

Promo-text
==========

Car Weather tekee auton esilämmityksestä helppoa ja
luotettavaa!


Versio
=====

Uutta versiossa 1.4.0:

Esilämmitysajat päivitetty vastaamaan Motivan syksyllä julkaisemia auton uusia esilämmityssuosituksia.  Valitse sovelluksen asetuksista moottorilämmittimeksi lohkolämmitin saadaksesi lyhyempi lämmitysaika tai säteilylämmitin saadaksesi pitempi lämmitysaika.

Uutta versiossa 1.3.0:

- Lumisadevaroitus näytetään myös räntäsateella 
- Korjattiin lumisadevaroitus toimimaaan aina oikein sääennusteen mukaan 
- Muita pieniä parannuksia sovelluksen ulkoasuun<br/>
Seuraa ja osallistu Car Weatherin kehitykseen nyt Twitterissä:
https://twitter.com/solipaste
---
Uutta versiossa 1.2.0:

- Lumivaroitus auttaa sinua varaamaan tarpeeksi aikaa lähtemiseen ja ajamiseen lumi- ja räntäsateella
- Osoittimella valittu lähtöaika pyöristyy aina lähimpään viiteen minuuttiin - tarkemman ajan voit edelleen valita lähtöaikaa napauttamalla
- Lämmityksen aloitusaika näytetään punaisella, jos autosi ei ehdi lämmetä valitsemaasi lähtöaikaan mennessä 
- Vikakorjauksia<br/>
Seuraa ja osallistu Car Weatherin kehitykseen nyt Twitterissä:
https://twitter.com/solipaste
---
Uutta versiossa 1.1.0:

- Uusi lähtöaikamuistutus ilmoittaa kun esilämmitys on valmis
- Päivitettiin paikkakuntalista
- Parannettiin muistutusten luotettavuutta (vaatii uuden Estä puhelinta menemästä virransäästötilaan -käyttöluvan)
- Kieli- ja vikakorjauksia

Seuraa ja osallistu Car Weatherin kehitykseen nyt Twitterissä:
https://twitter.com/solipaste

