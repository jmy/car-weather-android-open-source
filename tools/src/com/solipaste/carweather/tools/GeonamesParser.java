package com.solipaste.carweather.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import au.com.bytecode.opencsv.CSVReader;

public class GeonamesParser {

    /**
     * @param args
     */
    public static void main(String[] args) {
        GeonamesParser names = new GeonamesParser();
        names.read("cities1000.txt");
        names.write("locations.csv");
    }

    private final SortedMap<String, String[]> locs;

    public GeonamesParser() {
        locs = new TreeMap<String, String[]>();
    }


    public void read(String fileName) {

        BufferedReader r = null;
        CSVReader reader = null;

        try {
            r = new BufferedReader(new InputStreamReader(new FileInputStream(
                    fileName), "UTF-8"));

            // user non-existing quote mark
            reader = new CSVReader(r, '\t', '#');
            String[] nextLine;
            int c = 0;
            int c2 = 0;
            while ((nextLine = reader.readNext()) != null) {
                // 647731 Loimaa Loimaa Loimijoki,QZJ 60.84972 23.0561 P PPLA3
                // FI FI 15 02 430 0 82 Europe/Helsinki 2011-07-31

                if (nextLine[8].equals("FI")) {

                    // if (nextLine[7].equals("PPLA1")
                    // || nextLine[7].equals("PPLA2")
                    // || nextLine[7].equals("PPLA3")) {
                    String[] dataItems = new String[7];
                    int i = 0;
                    dataItems[i++] = nextLine[0];
                    dataItems[i++] = nextLine[1];
                    dataItems[i++] = nextLine[7];
                    dataItems[i++] = nextLine[8];
                    dataItems[i++] = nextLine[10];
                    dataItems[i++] = nextLine[11];
                    dataItems[i++] = nextLine[12];
                    locs.put(nextLine[1].toLowerCase(), dataItems);
                    c2++;
                    // }
                    c++;
                }
            }
            System.out.println("Total: " + c + " included: " + c2);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    public void write(String name) {

        BufferedWriter fileOutput = null;
        StringBuilder outLine = new StringBuilder();

        try {
            File outFile = new File(name);
            fileOutput = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(outFile), "UTF-8"));

            for (Map.Entry<String, String[]> entry : locs.entrySet()) {
                // 647731 Loimaa Loimaa Loimijoki,QZJ 60.84972 23.0561 P PPLA3
                // FI FI 15 02 430 0 82 Europe/Helsinki 2011-07-31

                String[] items = entry.getValue();
                int i = 0;
                outLine.append(items[i++]); // 647731
                outLine.append(",").append(items[i++]); // Loimaa
                outLine.append(",").append(items[i++]); // PPLA
                outLine.append(",").append(items[i++]); // FI
                outLine.append(",").append(items[i++]); // 15
                outLine.append(",").append(items[i++]); // 02
                if (items[i].length() > 0)
                    outLine.append(",").append(items[i++]); // 02
                outLine.append("\n");
                fileOutput.write(outLine.toString());
                outLine.setLength(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutput != null)
                try {
                    fileOutput.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}
